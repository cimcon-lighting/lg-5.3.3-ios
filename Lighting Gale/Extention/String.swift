//
//  String.swift
//  SONY
//
//  Created by admin on 23/02/17.
//  Copyright © 2017 e-Procurement Technologies Pvt Ltd. All rights reserved.
//

import Foundation

//import CryptoSwift

extension String {
    
    //MARK: - Encrypt AES Method
    /// This method will encrypt and return Cipher string
    
   /* func desEncryption(key: String = "5TGB&YHN7UJM(IK<") {
        let key = Array(key.utf8)
        let bytes = Array(self.utf8)
        let cryptor = Cryptor(operation: .encrypt, algorithm: .des, options: [.ECBMode, .PKCS7Padding], key: key, iv:[UInt8]())
        if let encrypted = cryptor.update(byteArray: bytes)?.final() {
            return Data(encrypted).base64EncodedString()
        }
        return nil
    }*/
    
    
    /*func aesEncrypt(key: String = "5TGB&YHN7UJM(IK<", iv: String = "!QAZ2WSX#EDC4RFV") -> String {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                let enc = try AES(key: key, iv: iv, padding: .pkcs7).encrypt(data.bytes)
                let encData = Data(bytes: enc, count: Int(enc.count))
                let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
                return base64String
            }
            catch let error {
                print(error.localizedDescription)
                return ""
            }
        }
        return ""
    }*/
    
    
    //MARK: - Decrypt AES Method
    /// This method will decrypt the Cipher string
    
    /*func aesDecrypt(key: String = "5TGB&YHN7UJM(IK<", iv: String = "!QAZ2WSX#EDC4RFV") -> String {
        if let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions.init(rawValue: 0)) {
            do {
                let dec = try AES(key: key, iv: iv, padding: .pkcs7).decrypt(data.bytes)
                let decData = Data(bytes: dec, count: Int(dec.count))
                let result = String(data: decData, encoding: .utf8)
                return result ?? ""
            }
            catch let error {
                print(error.localizedDescription)
                return ""
            }
        }
        
        return ""
    }*/
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z_%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    //validate Password
    var isValidPassword: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil){
                
                if(self.count>=6 && self.count<=20) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    /*func combineTwoAttributString (str1 : String , str2 : String) -> NSMutableAttributedString {
     let firstStrng = [NSAttributedStringKey.foregroundColor: UIColor.init(red: 137/255, green: 137/255, blue: 137/255, alpha: 1),
     NSAttributedStringKey.font               : UIFont().fontRobotoMediumTitlewithSize(15.0)]
     
     let SecondString = [NSAttributedStringKey.foregroundColor  : UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1),
     NSAttributedStringKey.font             : UIFont().fontRobotoMediumTitlewithSize(15.0)]
     
     let partOne = NSMutableAttributedString(string: str1, attributes: firstStrng)
     
     let partTwo = NSMutableAttributedString(string: str2, attributes: SecondString)
     
     let combineString = NSMutableAttributedString()
     
     combineString.append(partOne)
     combineString.append(partTwo)
     
     return combineString
     }*/
    
    private static let decimalFormatter:NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        return formatter
    }()
    
    private var decimalSeparator:String{
        return String.decimalFormatter.decimalSeparator ?? "."
    }
}
