//
//  ViewController.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
import SkyFloatingLabelTextField
import MSAL

class SecurityViewController: LGParent {
    var clientID                : String!
    @IBOutlet var lblSecurity   : UILabel!
    @IBOutlet var btnNext       : UIButton!
    @IBOutlet var txtSecurity   : UITextField!
    
    var kClientID       = ""
    var kRedirectUri    = ""
    var kAuthority      = ""
    var kGraphEndpoint  = ""
        
    typealias AccountCompletion = (MSALAccount?) -> Void
    
    let kScopes: [String] = ["user.read"]
    
    var accessToken = String()
    var applicationContext  : MSALPublicClientApplication?
    var webViewParamaters   : MSALWebviewParameters?
    
    var currentAccount      : MSALAccount?
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        setupLocalization()
        txtSecurity.text = (UserdefaultManager().getPreferenceForkey(SECURITY) as? String)
    }
    
    
    @objc func appCameToForeGround(notification: Notification) {
        self.loadCurrentAccount()
    }
    
    func platformViewDidLoadSetup() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appCameToForeGround(notification:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    func getSMALdata() {
        var dictHeader : [String : String] = [:]
        dictHeader["ClientID"]      = clientID
        dictHeader["source"]        = "IOS"
        
        ProfileServices().getSAMLData(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess{
                let dictData = responseData[DATA] as! [String : AnyObject]
                if dictData["isSamlSupport"] as! String == "true" {
                        
                        self.kClientID       = dictData["kClientID"]        as! String
                        self.kRedirectUri    = dictData["kRedirectUri"]     as! String
                        self.kAuthority      = dictData["kAuthority"]       as! String
                        self.kGraphEndpoint  = dictData["kGraphEndpoint"]   as! String
                        
                    DispatchQueue.main.async {
                        self.logout()
                        self.loadCurrentAccount { (account) in
                            guard let currentAccount = account else {
                                self.acquireTokenInteractively()
                                return
                            }
                            self.acquireTokenSilently(currentAccount)
                        }
                    }
                } else {
                    self.performSegue(withIdentifier: "loginVC", sender: self.txtSecurity.text)
                }
                do {
                    try self.initMSAL()
                } catch let error {
                    print("Unable to create Application Context \(error)")
                }
                
                self.loadCurrentAccount()
                self.platformViewDidLoadSetup()
            }
        }
    }
    
    func acquireTokenSilently(_ account : MSALAccount!) {
        
        guard let applicationContext = self.applicationContext else { return }
        
        let parameters = MSALSilentTokenParameters(scopes: kScopes, account: account)
        
        applicationContext.acquireTokenSilent(with: parameters) { (result, error) in
            
            if let error = error {
                
                let nsError = error as NSError
                
                // interactionRequired means we need to ask the user to sign-in. This usually happens
                // when the user's Refresh Token is expired or if the user has changed their password
                // among other possible reasons.
                
                if (nsError.domain == MSALErrorDomain) {
                    
                    if (nsError.code == MSALError.interactionRequired.rawValue) {
                        
                        DispatchQueue.main.async {
                            self.acquireTokenInteractively()
                        }
                        return
                    }
                }
                print("Could not acquire token silently: \(error)")
                //self.updateLogging(text: "Could not acquire token silently: \(error)")
                return
            }
            
            guard let result = result else {
                print("Could not acquire token: No result returned")
                //                self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken
            print("Refreshed Access token is \(self.accessToken)")
            //            self.updateLogging(text: "Refreshed Access token is \(self.accessToken)")
            //            self.updateSignOutButton(enabled: true)
            self.getContentWithToken()
        }
    }
    
    func acquireTokenInteractively() {
        
        guard let applicationContext = self.applicationContext else { return }
        guard let webViewParameters = self.webViewParamaters else { return }
        
        let parameters = MSALInteractiveTokenParameters(scopes: kScopes, webviewParameters: webViewParameters)
        parameters.promptType = .selectAccount
        
        applicationContext.acquireToken(with: parameters) { (result, error) in
            
            if let error = error {
                print("Could not acquire token: \(error)")
                //                self.updateLogging(text: "Could not acquire token: \(error)")
                return
            }
            
            guard let result = result else {
                print("Could not acquire token: No result returned")
                //                self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken
            print("Access token is \(self.accessToken)")
            //            self.updateLogging(text: "Access token is \(self.accessToken)")
            self.updateCurrentAccount(account: result.account)
            self.getContentWithToken()
        }
    }
    
    func getGraphEndpoint() -> String {
        return kGraphEndpoint.hasSuffix("/") ? (kGraphEndpoint + "v1.0/me/") : (kGraphEndpoint + "/v1.0/me/");
    }
    
    func getContentWithToken() {
        
        // Specify the Graph API endpoint
        let graphURI = getGraphEndpoint()
        let url = URL(string: graphURI)
        var request = URLRequest(url: url!)
        
        // Set the Authorization header for the request. We use Bearer tokens, so we specify Bearer + the token we got from the result
        request.setValue("Bearer \(self.accessToken)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let error = error {
                print("Couldn't get graph result: \(error)")
                //                self.updateLogging(text: "Couldn't get graph result: \(error)")
                return
            }
            
            guard let result = try? JSONSerialization.jsonObject(with: data!, options: []) else {
                print("Couldn't deserialize result JSON")
                //                self.updateLogging(text: "Couldn't deserialize result JSON")
                return
            }
            print("Result from Graph: \((result as! [String : Any])["userPrincipalName"]!)")
            //            self.updateLogging(text: "Result from Graph: \(result))")
            if let email = ((result as! [String : Any])["userPrincipalName"]) {
                DispatchQueue.main.async {
                    self.getSAMLToken(strEmail: email as! String)
                }
            }
            
        }.resume()
    }
    
    func loadCurrentAccount(completion: AccountCompletion? = nil) {
        
        guard let applicationContext = self.applicationContext else { return }
        
        let msalParameters = MSALParameters()
        msalParameters.completionBlockQueue = DispatchQueue.main
        
        // Note that this sample showcases an app that signs in a single account at a time
        // If you're building a more complex app that signs in multiple accounts at the same time, you'll need to use a different account retrieval API that specifies account identifier
        // For example, see "accountsFromDeviceForParameters:completionBlock:" - https://azuread.github.io/microsoft-authentication-library-for-objc/Classes/MSALPublicClientApplication.html#/c:objc(cs)MSALPublicClientApplication(im)accountsFromDeviceForParameters:completionBlock:
        applicationContext.getCurrentAccount(with: msalParameters, completionBlock: { (currentAccount, previousAccount, error) in
            
            if let error = error {
                print("Couldn't query current account with error: \(error)")
                return
            }
            
            if let currentAccount = currentAccount {
                print("Found a signed in account \(String(describing: currentAccount.username)). Updating data for that account...")
                
                self.updateCurrentAccount(account: currentAccount)
                
                if let completion = completion {
                    completion(self.currentAccount)
                }
                
                return
            }
            print("Account signed out. Updating UX")
            
            self.accessToken = ""
            self.updateCurrentAccount(account: nil)
            
            if let completion = completion {
                completion(nil)
            }
        })
    }
    
    func updateCurrentAccount(account: MSALAccount?) {
        self.currentAccount = account
        //        self.updateAccountLabel()
        //        self.updateSignOutButton(enabled: account != nil)
    }
    
    func initMSAL() throws {
        guard let authorityURL = URL(string: kAuthority) else {
            print("Unable to create authority URL")
            return
        }
        
        let authority = try MSALAADAuthority(url: authorityURL)
        
        let msalConfiguration = MSALPublicClientApplicationConfig(clientId: kClientID,
                                                                  redirectUri: kRedirectUri,
                                                                  authority: authority)
        self.applicationContext = try MSALPublicClientApplication(configuration: msalConfiguration)
        self.initWebViewParams()
    }
    
    func initWebViewParams() {
        self.webViewParamaters = MSALWebviewParameters(authPresentationViewController: self)
    }
    
    func logout() {
        guard let applicationContext = self.applicationContext else { return }
        
        guard let account = self.currentAccount else { return }
        
        do {
            let signoutParameters = MSALSignoutParameters(webviewParameters: self.webViewParamaters!)
            signoutParameters.signoutFromBrowser = false
            
            applicationContext.signout(with: account, signoutParameters: signoutParameters, completionBlock: {(success, error) in
                
                if let error = error {
                    print("Couldn't sign out account with error: \(error)")
                    //                    self.updateLogging(text: )
                    return
                }
                print("Sign out completed successfully")
                //                self.updateLogging(text: "Sign out completed successfully")
                self.accessToken = ""
                self.updateCurrentAccount(account: nil)
            })
            
        }
    }
    
    func getSAMLToken(strEmail : String) {
        var dictHeader : [String : String] = [:]
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        
        dictHeader[UNAME_SMALL]     = strEmail
        dictHeader[GRANT_TYPE]      = DEFAULT_PWD
        dictHeader["Origins"]       = "Mobile"
        dictHeader["Version"]       = version
        dictHeader["Source"]        = "IOS"
        dictHeader["clientid"]      = clientID
        
        ProfileServices().getSAMLToken(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess{
                UserDefaults().removeObject(forKey: "DYNAMIC")
                UserDefault.removeSuite(named: "LAMPNAME")
                UserDefaults().removeObject(forKey:"DEFAULTLAMP")
                UserDefaults().removeObject(forKey:"DEFAULTCLIENT")
                UserDefaults().removeObject(forKey:"DEFAULTCLIENT")
                UserDefaults().removeObject(forKey:"DEFAULTCLIENTID")
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_login"
                ])
                self.navigateToDashboard()                
            }
        }
    }
    
    func navigateToDashboard() {
        UserdefaultManager().setPreference(self.clientID as AnyObject, strKey: "ClientID")
               
        self.navigationController?.isNavigationBarHidden = true
        self.performSegue(withIdentifier: "loginIdentifier", sender: self)
    }
    
    func setupLocalization() {
        lblSecurity.text            = "Security Code".localized()
        btnNext.setTitle("Next".localized(), for: .normal)
        txtSecurity.placeholder     = "Enter Security Code".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("securityCode", screenClass: "securityCode")
        /*Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
         AnalyticsParameterItemID: "id IOS",
         AnalyticsParameterItemName: "TEST IOS",
         AnalyticsParameterContentType: "cont IOS"
         ])*/
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        txtSecurity.text = ""
        if segue.identifier == "loginIdentifier"{
            
        } else {
            let loginVC : ViewController    = segue.destination as! ViewController
            loginVC.clientID                = clientID
            loginVC.securityCode            = (sender as! String)
        }
    }
    
    
    //MARK:- forgotPWD
    @IBAction func btnSubmit_Click(sender : UIButton) {
        
        txtSecurity.resignFirstResponder()
        if txtSecurity.text != "" {
            var dictData : [String : AnyObject] = [:]
            dictData["source"]  = "IOS" as AnyObject
            dictData["code"]    = txtSecurity.text as AnyObject
            
            ProfileServices().security(parameters: dictData, headerParams: [:], showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    self.clientID           = (responseData["ClientID"] as! String)
                    appDelegate.baseUrlDev  = (responseData["APIURL"] as! String)
                    appDelegate.isFrmPaid   = (responseData["apptype"] as! String)
                    appDelegate.strMsgFrmAPI  = (responseData["apptypemessage"] as? String)
                    appDelegate.strNearMe   = (responseData["nearmedistance"] as? String)
                    appDelegate.strNearMsg  = (responseData["nearmedistancemessage"] as? String)
                    appDelegate.strClientNm = (responseData["ClientName"] as? String)
                    
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemName: appDelegate.strClientNm+"_"+"securityCode"
                    ])
                    
                    self.getSMALdata()
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Please enter security code".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
}

extension SecurityViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
