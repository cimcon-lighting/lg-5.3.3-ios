//
//  HistoricalDataViewController.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics
import RangeSeekSlider

class CellHistory : UITableViewCell {
    @IBOutlet var vwStatus : UIView!
    @IBOutlet var vwComm   : UIView!
    
    @IBOutlet var lblTime  : UILabel!
    @IBOutlet var lblLS    : UILabel!
    @IBOutlet var lblComm  : UILabel!
}


class HistoricalDataViewController: LGParent {
    
    @IBOutlet var vwFilter      : UIView!
    
    @IBOutlet var vwDetails     : UIView!
    
    @IBOutlet var tblView       : UITableView!
    @IBOutlet var tblView1      : UITableView!
    
    @IBOutlet var btnFirst      : UIButton!
    
    @IBOutlet var vwTop         : UIView!
    @IBOutlet var vwSelectedTab : UIView!
    
    var isFrmCommFault          : Bool!
    var isFrmDtPicker           : Bool!
    
    let datePicker              = UIDatePicker()
    
    var isFilterOpen            : Bool! = false
    
    var arrTblData              : [AnyObject]!
    var arrSelectdTag           : [Int]!
    var arrSearchData           : [String]!
    
    var totalCount              : Int! = 1
    var pageCount               : Int! = 1
    var selectedTAG             : Int! = 1
    
    var strMode                 : String!
    var strSLCID                : String!
    var strGateway              : String!
    
    var dictDashboardData       : [String : AnyObject]!
    
    @IBOutlet var lblMsg        : UILabel!
    @IBOutlet var lblTimeRng    : UILabel!
    @IBOutlet var lblFrmDt      : UILabel!
    @IBOutlet var lblToDt       : UILabel!
    
    @IBOutlet var txtSLC        : UITextField!
    @IBOutlet var txtFrmDate    : UITextField!
    @IBOutlet var txtToDate     : UITextField!
    
    var refreshControl          : UIRefreshControl!
    
    @IBOutlet var rangSlider    : RangeSeekSlider!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 15.0, *) {
            self.tblView.sectionHeaderTopPadding = 0
        }
        
        rangSlider.delegate  = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        let bgImage = UIImageView();
        bgImage.image = UIImage(named: "Green-Gradient");
        bgImage.contentMode = .scaleToFill
        
        lblMsg.isHidden = true
        
        arrTblData = []
        
        tblView.tableFooterView = UIView()
        
        addLeftBarButton(imgLeftbarButton: "back")
        
        
        let oneDayAgo = Date(timeIntervalSinceNow: TimeInterval(-3600*24))
        let formatter = DateFormatter()
        formatter.dateFormat = serviceHandler.objUserModel.strDtFrmat!+" HH:mm"
        
        let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let darkGray  = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 111/255, green: 113/255, blue: 121/255, alpha: 1)]
        
        let strFrmDate = formatter.string(from: oneDayAgo)
        let arrFrmDate = strFrmDate.components(separatedBy: " ")
        let mutableAttrString1 = NSMutableAttributedString(string: arrFrmDate[0]+" ", attributes: darkGray)
        let mutableAttrString2 = NSMutableAttributedString(string: arrFrmDate[1], attributes: lightGray)
        
        let finalFrm = NSMutableAttributedString()
        finalFrm.append(mutableAttrString1)
        finalFrm.append(mutableAttrString2)
        
        lblFrmDt.attributedText = finalFrm
        
        
        let currentDt = Date()
        
        let strToDate = formatter.string(from: currentDt)
        let arrToDate = strToDate.components(separatedBy: " ")
        let mutableTo1 = NSMutableAttributedString(string: arrToDate[0]+" ", attributes: darkGray)
        let mutableTo2 = NSMutableAttributedString(string: arrToDate[1], attributes: lightGray)
        
        let finalTo = NSMutableAttributedString()
        finalTo.append(mutableTo1)
        finalTo.append(mutableTo2)
        
        lblToDt.attributedText = finalTo
        
    }
    
    /*override func leftButtonClick() {
     self.navigationController?.isNavigationBarHidden = true
     _ = self.navigationController?.popViewController(animated: true)
     }*/
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title   = "Historical Data".localized()
        lblTimeRng.text             = "Time Range".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        selectMode()
        refresh(sender: self)
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("historicalData", screenClass: "HistoricalData")
    }
    
    
    @objc func refresh(sender:AnyObject) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    
    func selectMode() {
        switch strMode {
        case "0":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_dayBurner"
            ])
            self.title = "Day Burner".localized()
        //str = PHOTOCELL
        case "1":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_lampOutages"
            ])
            self.title = "Lamp Outages".localized()
            //str = MODE_MANUAL
            
        default:
            break
        }
    }
    
    
    func getDataServer(isLoadMore : Bool) {
        if arrTblData.count != totalCount {
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[PAGE_NO]         = String(pageCount)
            dictHeader["pagesize"]      = "10"
            dictHeader["SlcId"]         = strSLCID
            /*if let gateID = strGateway {
                dictHeader["gId"]       = gateID
            }*/
            
            let arrFrm  = lblFrmDt.text?.components(separatedBy: " ")
            let arrTo   = lblToDt.text?.components(separatedBy: " ")
            
            dictHeader["FromDate"]      = arrFrm![0]
            dictHeader["ToDate"]        = arrTo![0]
            dictHeader["FromTime"]      = arrFrm![1]+":00"
            dictHeader["ToTime"]        = arrTo![1]+":00"
            
            ProfileServices().getHistoricalData(parameters: [:], headerParams: dictHeader, showLoader: isLoadMore) { (responseData, isSuccess) in
                if isSuccess {
                    let arrTmp : [AnyObject] = responseData[DATA]![LIST]   as! [AnyObject]
                    self.arrTblData.append(contentsOf: arrTmp)
                    self.totalCount = Int(responseData[DATA]![TOTAL_RECORDS]   as! String)
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }/* else {
             self.lblMsg.isHidden = true
             }*/
            //self.tblView.reloadData()
        }
    }
    
    /*@IBAction func rightButtonPlusClick() {
     self.view.addSubview(vwFilter)
     vwFilter.frame = self.view.bounds
     vwFilter.alpha = 0.0
     UIView.animate(withDuration: 0.15) {
     self.vwFilter.alpha = 1.0
     }
     
     }*/
}

//MARK:- TableView Delegate
extension HistoricalDataViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tblView {
            let cell : CellStatusHeader = tableView.dequeueReusableCell(withIdentifier: "CellStatusHeader") as! CellStatusHeader
            cell.lblLastDt?.text    = "DATE & TIME".localized()
            cell.lblSLCNm?.text     = "LAMP STATUS".localized()
            cell.lblHeader?.text    = "COMM.".localized()
            /*cell.btnCheckAll.addTarget(self, action: #selector(btnCheckAll_Click), for: .touchUpInside)
             if isFrmSelectedAll {
             cell.btnCheckAll.isSelected = true
             } else {
             cell.btnCheckAll.isSelected = false
             }*/
            return cell.contentView
        }
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellHistory = tableView.dequeueReusableCell(withIdentifier: "CellHistory") as! CellHistory
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        
        let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let darkGray  = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 111/255, green: 113/255, blue: 121/255, alpha: 1)]
        
        let strDate = (dictData[DATE_TIME] as! String)
        let arrStrDate = strDate.components(separatedBy: " ")
        let mutableAttrString1 = NSMutableAttributedString(string: arrStrDate[0]+" ", attributes: darkGray)
        let mutableAttrString2 = NSMutableAttributedString(string: arrStrDate[1], attributes: lightGray)
        
        let finalMutable = NSMutableAttributedString()
        finalMutable.append(mutableAttrString1)
        finalMutable.append(mutableAttrString2)
        
        cell.lblTime.attributedText = finalMutable
        
        if let ls : String = (dictData["@s1"] as? String) {
            cell.vwStatus.isHidden      = false
            cell.lblLS.isHidden         = true
            if ls.lowercased() == "0" {
                cell.vwStatus.backgroundColor = UIColor.red
            } /*else if  ls.lowercased() == "" {
                cell.vwStatus.isHidden  = true
                cell.lblLS.isHidden     = true
            }*/ else {
                cell.vwStatus.backgroundColor = UIColor.init(red: 39/255, green: 170/255, blue: 12/255, alpha: 1)
            }
        } else {
            cell.vwStatus.isHidden      = true
            cell.lblLS.isHidden         = false
        }
        
        if let comm : String = (dictData["@s8"] as? String) {
            cell.vwComm.isHidden        = false
            cell.lblComm.isHidden       = true
            if comm.lowercased() == "1" {
                cell.vwComm.backgroundColor = UIColor.init(red: 39/255, green: 170/255, blue: 12/255, alpha: 1)
            } /*else if comm == "" {
                cell.lblLS.isHidden     = true
                cell.lblComm.isHidden   = true
                cell.vwComm.isHidden    = true
                cell.lblComm.isHidden   = true
            }*/ else {
                cell.vwComm.backgroundColor = UIColor.red
                cell.vwStatus.isHidden  = true
                cell.vwStatus.isHidden  = true
                if let _ : String = (dictData["@s1"] as? String) {
                    cell.lblLS.isHidden     = true
                } else {
                    cell.lblLS.isHidden     = false
                }
            }
        } else {
            cell.vwComm.isHidden        = true
            cell.lblComm.isHidden       = false
        }
        
        
        if indexPath.row == arrTblData.count-1 {
            getDataServer(isLoadMore: true)
        }
        
        return cell
    }
    
}

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //getDetails(strSLC:String(dictData["slcNumber"] as! Int) , nvrComm: "")
}

func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if tableView == tableView {
        let cell : CellStatusHeader = tableView.dequeueReusableCell(withIdentifier: "CellStatusHeader") as! CellStatusHeader
        return cell.contentView
    }
    return UIView.init()
}

func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 24
}

extension HistoricalDataViewController : RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        let oneDayAgo : Date = Date(timeIntervalSinceNow: TimeInterval(-3600*24))
        let finatFrmdt  = oneDayAgo.addingTimeInterval(TimeInterval(3600*minValue))
        let formatter   = DateFormatter()
        formatter.dateFormat = serviceHandler.objUserModel.strDtFrmat!+" HH:mm"
        lblFrmDt.text   = formatter.string(from: finatFrmdt)
                
        let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let darkGray  = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 111/255, green: 113/255, blue: 121/255, alpha: 1)]
        
        let strFrmDate = formatter.string(from: finatFrmdt)
        let arrFrmDate = strFrmDate.components(separatedBy: " ")
        let mutableAttrString1 = NSMutableAttributedString(string: arrFrmDate[0]+" ", attributes: darkGray)
        let mutableAttrString2 = NSMutableAttributedString(string: arrFrmDate[1], attributes: lightGray)
        
        let finalFrm = NSMutableAttributedString()
        finalFrm.append(mutableAttrString1)
        finalFrm.append(mutableAttrString2)
        
        lblFrmDt.attributedText   = finalFrm
        
        let currentDt : Date = Date()
        let finatTodt   = currentDt.addingTimeInterval(TimeInterval(-3600*(24-maxValue)))
        
        let strToDate = formatter.string(from: finatTodt)
        let arrToDate = strToDate.components(separatedBy: " ")
        let mutableTo1 = NSMutableAttributedString(string: arrToDate[0]+" ", attributes: darkGray)
        let mutableTo2 = NSMutableAttributedString(string: arrToDate[1], attributes: lightGray)
        
        let finalTo = NSMutableAttributedString()
        finalTo.append(mutableTo1)
        finalTo.append(mutableTo2)
        
        lblToDt.attributedText = finalTo
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
}
