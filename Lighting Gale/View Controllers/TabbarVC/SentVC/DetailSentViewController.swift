//
//  DetailsViewController.swift
//  Lighting Gale
//
//  Created by Apple on 09/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics

class CellDetailSent : UITableViewCell {
    @IBOutlet var lblSLC        :      UILabel!
    @IBOutlet var lblACKTime    :      UILabel!
    @IBOutlet var lblResTime    :      UILabel!
    @IBOutlet var lblStatus     :      UILabel!
    
    @IBOutlet var lblHeadSLC        :      UILabel!
    @IBOutlet var lblHeadACKTime    :      UILabel!
    @IBOutlet var lblHeadResTime    :      UILabel!
    @IBOutlet var lblHeadStatus     :      UILabel!
    
}

class DetailSentViewController: LGParent {
    
    @IBOutlet var tblView       : UITableView!
    
    @IBOutlet var vwTop         : UIView!
    
    var arrTblData              : [AnyObject]!
    var dictTradkDetails        : [String : AnyObject]!
    
    var totalCount              : Int! = 1
    var pageCount               : Int! = 1
    
    @IBOutlet var lblMsg        : UILabel!
    @IBOutlet var lblTrcakID    : UILabel!
    @IBOutlet var lblGName      : UILabel!
    
    @IBOutlet var consVWHeight  : NSLayoutConstraint!
    
    var refreshControl          : UIRefreshControl!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        lblMsg.isHidden = true
        
        tblView.rowHeight = UITableView.automaticDimension
        
        self.addLeftBarButton(imgLeftbarButton: "back")
        
        arrTblData = []
        tblView.tableFooterView = UIView()
        
        //        if self.serviceHandler.objUserModel.strClienttype == "1" {
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            let arrClientType = arr as! [[String : AnyObject]]
            let filterdArray = arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                consVWHeight.constant = 70
            } else{
                consVWHeight.constant = 40
            }
        }
        
        if (dictTradkDetails["clientCommType"] as! Int) != 1 {
            lblGName.isHidden = true
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Analytics.setScreenName("sentCommandDetails", screenClass: "sentCommandDetails")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblTrcakID.text     = "\("Track ID".localized())                 : "+String(dictTradkDetails[TRACK_ID] as! String)
        lblGName.text       = "\("Gateway Name".localized())   : "+String(dictTradkDetails["gatewayName"] as! String)
        
        refresh(sender: self)
    }
    
    @objc func refresh(sender:AnyObject) {
        totalCount = 1
        pageCount = 1
        arrTblData.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmTrackId(isLoading: true)
        })
    }
    
    func getDataFrmTrackId(isLoading : Bool) {
        /*
         "slc": "804-804",
         "status": "Sent successfully",
         "slcAckDateTime": "5/3/2019 10:14:26 AM",
         "slcResponseDateTime": "5/3/2019 10:14:10 AM",
         "isSetCommand": "0",
         "totalRecords": ""
         
         */
        
        if arrTblData.count != totalCount {
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            dictHeader[PAGE_NO]         = String(pageCount)
            dictHeader["TrackID"]       = (dictTradkDetails[TRACK_ID] as! String)
            dictHeader[PAGE_SIZE]       = "10"
            
            let dictData : [String : AnyObject] = [:]
            
            ProfileServices().getDetailsFrmTrackID(parameters: dictData, headerParams: dictHeader, showLoader: isLoading) { (responseData, isSuccess) in
                if isSuccess {
                    print(responseData)
                    let dictTmp : [String : AnyObject] = responseData[DATA] as! [String : AnyObject]
                    let arrTmp : [AnyObject] = dictTmp["trackDetail"]       as! [AnyObject]
                    self.arrTblData.append(contentsOf: arrTmp)
                    
                    self.totalCount = Int(dictTmp[TOTAL_RECORDS]   as! String)
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }
        }
    }
}
extension DetailSentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellDetailSent = tableView.dequeueReusableCell(withIdentifier: "CellDetailSent") as! CellDetailSent
        
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        
        cell.lblHeadSLC.text        = "SLC".localized()
        cell.lblHeadStatus.text     = "Status".localized()
        cell.lblHeadACKTime.text    = "ACK Time".localized()
        cell.lblHeadResTime.text    = "Response Time".localized()
        
        cell.lblSLC.text       = dictData["slc"] as? String
        cell.lblStatus.text    = dictData["status"] as? String
        cell.lblACKTime.text   = dictData["slcAckDateTime"] as? String == "" ? "N/A" : dictData["slcAckDateTime"] as? String
        cell.lblResTime.text   = dictData["slcResponseDateTime"] as? String == "" ? "N/A" : dictData["slcResponseDateTime"] as? String
        
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        if indexPath.row == arrTblData.count-1 {
            getDataFrmTrackId(isLoading: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //showModal()
        /*DispatchQueue.main.async {
         self.performSegue(withIdentifier: "DetailIdentifier", sender: self)
         }*/
    }
    /*private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
     return UITableView.automaticDimension
     }*/
}
