//
//  SearchViewController.swift
//  Lighting Gale
//
//  Created by Apple on 11/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

protocol SearchVCDelegate: class {
    func changeSearchParams(_ placeHolder : String, tagTextField: Int)
    func changeSearchParamsStatus(_ placeHolder : String, tagTextField: Int, strGroupID: String)
}

class CellSearch : UITableViewCell {
    @IBOutlet var lblNm : UILabel!
}

class SearchViewController: LGParent, UISearchBarDelegate {
    
    var strPlasHolder               : String?
    
    var strGatewayID                : String?
    
    var isFrmAssignedSLC            : Bool! = false
    
    @IBOutlet var lblMsg            : UILabel?
    
    weak var searchDelegate         : SearchVCDelegate?
    
    @IBOutlet var tblView           : UITableView!
    
    @IBOutlet var searchBar         : SearchBar!
    
    var arrTblData                  : [String]!
    var arrMainData                 : [AnyObject]!
    var filteredData                : [String]!
    
    var currentTag                  : Int!
    
    var isFrmSLCName                : Bool! = false
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMsg?.isHidden = true
        
        tblView.rowHeight           = UITableView.automaticDimension
        
        searchBar.placeholder       = strPlasHolder
        searchBar.delegate          = self        
        searchBar.tintColor         = UIColor.darkGray
        //        searchBar.cancelTitle       = "Search"
        
        if strPlasHolder == "SLC#" || strPlasHolder == "Group".localized() || strPlasHolder == "Track ID".localized() || strPlasHolder == "SLC# " || strPlasHolder == "SLC Name".localized() {
            filteredData = []
            if strPlasHolder == "SLC#" || strPlasHolder == "SLC Name".localized() {
                getSLCs(strSearch: "")
            } else if strPlasHolder == "Group".localized() {
                getGroup(strSearch: "")
            } else if strPlasHolder == "Track ID".localized() {
                getTrackID(strSearch: "")
            } else if strPlasHolder == "SLC# " {
                getTotalSLC(strSearch: "")
            }
        } else {
            filteredData                = arrTblData
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBG_Click(sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- getSLCs
    func getTotalSLC(strSearch : String) {
        filteredData.removeAll()
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader["SLCNO"]          = strSearch
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader["NodeType"]      = (UserDefault.value(forKey: "DEFAULTCLIENTID") as! String)
        ProfileServices().getTotalSLCSearch(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responseData[DATA] as! [AnyObject]
                self.arrMainData = arrTmp
                for i in 0..<arrTmp.count {
                    if self.isFrmSLCName {
                        self.filteredData.append(arrTmp[i]["text"] as! String)
                    } else {
                        self.filteredData.append(arrTmp[i]["value"] as! String)
                    }
                }
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            } else {
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            }
        }
    }
    
    
    func getSLCs(strSearch : String) {
        filteredData.removeAll()
        tblView.reloadData()
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader[SLC_NO]          = strSearch
        dictHeader["Gatewayid"]     = strGatewayID == "" ? "" : strGatewayID
        if isFrmAssignedSLC {
            dictHeader["lamptypeid"]    = "0"
        } else {
            dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        }
        dictHeader["NodeType"]      = (UserDefault.value(forKey: "DEFAULTCLIENTID") as! String)
        
        ProfileServices().getSLCList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responseData[DATA] as! [AnyObject]
                self.arrMainData = arrTmp
                for i in 0..<arrTmp.count {
                    if self.isFrmSLCName {
                        self.filteredData.append(arrTmp[i]["text"] as! String)
                    } else {
                        self.filteredData.append(arrTmp[i]["value"] as! String)
                    }
                }
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            } else {
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            }
        }
    }
    
    
    //MARK:- getGroup
    func  getGroup(strSearch : String) {
        filteredData.removeAll()
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader["Group"]          = strSearch
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader["NodeType"]      = (UserDefault.value(forKey: "DEFAULTCLIENTID") as! String)
        
        ProfileServices().getGroupSLC(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responseData[DATA] as! [AnyObject]
                self.arrMainData = arrTmp
                for i in 0..<arrTmp.count {
                    self.filteredData.append(arrTmp[i]["groupName"] as! String)
                }
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            } else {
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            }
        }
    }
    
    //MARK:- getTrackID
    func getTrackID(strSearch : String) {
        filteredData.removeAll()
        tblView.reloadData()
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader["TrackID"]          = strSearch
        
        ProfileServices().commandTrackID(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responseData[DATA] as! [AnyObject]
                //self.arrMainData = arrTmp
                self.filteredData = arrTmp as? [String]
                /*for i in 0..<arrTmp.count {
                 self.filteredData.append(arrTmp[i]["groupName"] as! String)
                 }*/
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            } else {
                if self.filteredData.count == 0 {
                    self.filteredData.removeAll()
                    self.lblMsg?.text = "No data Found".localized()
                    self.lblMsg?.isHidden = false
                } else {
                    self.lblMsg?.isHidden = true
                }
                self.tblView.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print(searchBar.text!)
        if strPlasHolder == "SLC#" || strPlasHolder == "Group".localized() || strPlasHolder == "SLC# " || strPlasHolder == "SLC Name".localized() {
            if strPlasHolder == "SLC#" || strPlasHolder == "SLC Name".localized() {
                getSLCs(strSearch: searchBar.text!)
            } else if strPlasHolder == "SLC# " {
                getTotalSLC(strSearch: searchBar.text!)
            } else {
                getGroup(strSearch: searchBar.text!)
            }
        } else {
            filteredData = searchBar.text!.isEmpty ? arrTblData : arrTblData.filter({(dataString: String) -> Bool in
                return dataString.range(of: searchBar.text!, options: .caseInsensitive) != nil
            })
            if self.filteredData.count == 0 {
                self.filteredData.removeAll()
                self.lblMsg?.text = "No data Found".localized()
                self.lblMsg?.isHidden = false
            } else {
                self.lblMsg?.isHidden = true
            }
            tblView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if self.strPlasHolder == "SLC#" || self.strPlasHolder == "Group".localized() || self.strPlasHolder == "Track ID".localized()  || self.strPlasHolder == "SLC# " || self.strPlasHolder == "SLC Name".localized() {
                if self.strPlasHolder == "SLC#" || self.strPlasHolder == "SLC Name".localized() {
                    self.getSLCs(strSearch: searchBar.text!)
                } else if self.strPlasHolder == "SLC# " {
                    self.getTotalSLC(strSearch: searchBar.text!)
                } else if self.strPlasHolder == "Group".localized()  {
                    self.getGroup(strSearch: searchBar.text!)
            } else {
                self.getTrackID(strSearch: searchBar.text!)
            }
        } else {
            self.filteredData = searchText.isEmpty ? self.arrTblData : self.arrTblData.filter({(dataString: String) -> Bool in
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
            if self.filteredData.count == 0 {
                self.filteredData.removeAll()
                self.lblMsg?.text = "No data Found".localized()
                self.lblMsg?.isHidden = false
            } else {
                self.lblMsg?.isHidden = true
            }
            self.tblView.reloadData()
        }
        }
    }
    
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellSearch = tableView.dequeueReusableCell(withIdentifier: "CellSearch") as! CellSearch
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        cell.lblNm.text = filteredData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchDelegate?.changeSearchParams(filteredData[indexPath.row],tagTextField: currentTag)
        if strPlasHolder == "Group".localized() {
            self.searchDelegate?.changeSearchParamsStatus(filteredData[indexPath.row],tagTextField: currentTag, strGroupID : arrMainData[indexPath.row]["groupID"] as! String)
        } else if strPlasHolder == "Gateway".localized() {
            
            let selectedString = filteredData[indexPath.row]
            let filteredArray = arrMainData.filter{ ($0["gatewayName"] as! String) == (selectedString ) }
            
            self.searchDelegate?.changeSearchParamsStatus(filteredData[indexPath.row],tagTextField: currentTag, strGroupID : (filteredArray.first as! [String : AnyObject])["id"] as! String)
        } else if strPlasHolder == "Status".localized() {
            
            let selectedString = filteredData[indexPath.row]
            let filteredArray = arrMainData.filter{ ($0["key"] as! String) == (selectedString ) }
            
            self.searchDelegate?.changeSearchParamsStatus(filteredData[indexPath.row],tagTextField: currentTag, strGroupID : String(describing: (filteredArray.first as! [String : AnyObject])["value"] as! Int))
            
            //self.searchDelegate?.changeSearchParamsStatus(filteredData[indexPath.row],tagTextField: currentTag, strGroupID : String(describing: arrMainData[indexPath.row]["value"] as! Int))
        } else if strPlasHolder == "Command".localized() {
            
            let selectedString = filteredData[indexPath.row]
            let filteredArray = arrMainData.filter{ ($0["commandName"] as! String) == (selectedString ) }
            
            self.searchDelegate?.changeSearchParamsStatus(filteredData[indexPath.row],tagTextField: currentTag, strGroupID : String(describing: (filteredArray.first as! [String : AnyObject])["commandID"] as! Int))
            
        } else if strPlasHolder == "SLC Name".localized() {
            let selectedString = filteredData[indexPath.row]
            let filteredArray = arrMainData.filter{ ($0["text"] as! String) == (selectedString) }
            self.searchDelegate?.changeSearchParamsStatus(selectedString,tagTextField: currentTag, strGroupID : (filteredArray.first as! [String : AnyObject])["value"] as! String)
        } else {
            self.searchDelegate?.changeSearchParamsStatus(filteredData[indexPath.row],tagTextField: currentTag, strGroupID : "")
        }
        self.dismiss(animated: true, completion: nil)
    }
    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


