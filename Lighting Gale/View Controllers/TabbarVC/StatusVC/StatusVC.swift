//
//  StatusVC.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Floaty
import LocalAuthentication
import SwiftKeychainWrapper
import EasyNotificationBadge
import FirebaseAnalytics

class CellStatus : UITableViewCell {
    @IBOutlet var btnCheck : UIButton!
    
    @IBOutlet var lblSLCNm : UILabel!
    @IBOutlet var lblSLCNo : UILabel!
    @IBOutlet var lblDt    : UILabel!
    @IBOutlet var lblBhrs  : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var lblCurrent : UILabel!
    @IBOutlet var lblKiloW : UILabel!
    @IBOutlet var lblVolt  : UILabel!
    @IBOutlet var lblHeadDim  : UILabel!
    
    @IBOutlet var constDimHeight : NSLayoutConstraint?
    
    @IBOutlet var vwD      : UIView!
    @IBOutlet var vwLS     : UIView!
    
    @IBOutlet var imgArrow : UIImageView?
    
    @IBOutlet var vwInformation       : UIView!
}
class CellFilter : UITableViewCell {
    @IBOutlet var imgView   : UIImageView!
    @IBOutlet var btnFilter : UIButton!
}
class CellStatusHeader : UITableViewCell {
    @IBOutlet var btnCheckAll : UIButton!
    @IBOutlet var lblHeader   : UILabel?
    @IBOutlet var stackView   : UIStackView?
    @IBOutlet var lblHeaderHide : UILabel?
    @IBOutlet var lblNodeType : UILabel?
    @IBOutlet var lblLastDt   : UILabel?
    @IBOutlet var lblSLCNm    : UILabel?
    
}

class StatusVC: LGParent {
    @IBOutlet var vwFilter      : UIView!
    @IBOutlet var vwInformation : UIView!
    @IBOutlet var vwDetails     : UIView!
    
    @IBOutlet var scrollview    : UIScrollView!
    
    @IBOutlet var tblView       : UITableView!
    @IBOutlet var tblView1      : UITableView!
    @IBOutlet var tblView2      : UITableView!
    
    @IBOutlet var btnFirst      : UIButton!
    @IBOutlet var btnSelected   : UIButton!
    /*@IBOutlet var btnThird      : UIButton!
     @IBOutlet var btnFourth     : UIButton!
     @IBOutlet var btnFifth      : UIButton!
     @IBOutlet var btnSix        : UIButton!
     @IBOutlet var btnSeven      : UIButton!*/
    @IBOutlet var btnLmpType    : UIButton!
    @IBOutlet var btnNodeType    : UIButton!
    @IBOutlet var btnSearch     : UIButton!
    @IBOutlet var btnNearme     : UIButton!
    @IBOutlet var btnClear      : UIButton!
    
    @IBOutlet var vwTop         : UIView!
    @IBOutlet var vwSlider      : UIView!
    @IBOutlet var vwSelectedTab : UIView!
    @IBOutlet var vwBadge1      : UIView!
    @IBOutlet var vwBadge2      : UIView!
    @IBOutlet var vwBadge3      : UIView!
    @IBOutlet var vwBadge4      : UIView!
    @IBOutlet var vwBadge5      : UIView!
    @IBOutlet var vwBadge6      : UIView!
    @IBOutlet var vwBadge7      : UIView!
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    @IBOutlet var consLamptypeHorizontal : NSLayoutConstraint?
    
    var isFilterOpen            : Bool! = false
    var isFrmPTag               : Bool! = false
    
    var arrTblData              : [AnyObject]!
    var arrInformation          : [[String : String]]!
    var arrSelectdTag           : [Int]!
    var arrSearchData           : [String]!
    var arrGateway              : [AnyObject]!
    var arrFilter,arrImg,arrImgCmd : [String]!
    
    var totalCount              : Int! = 1
    var pageCount               : Int! = 1
    var selectedTAG             : Int! = 1
    
    var strStatusParam          : String!
    var strPowerParam           : String!
    var strGID                  : String!
    var strGatwayID             : String!
    var strLastMode             : String?
    
    @IBOutlet var lblMsg        : UILabel!
    @IBOutlet var lblHeader     : UILabel!
    @IBOutlet var lblLtype      : UILabel!
    @IBOutlet var lblNtype      : UILabel!
    @IBOutlet var lblPrompt     : UILabel!
    @IBOutlet var lblSentCommand : UILabel!
    
    @IBOutlet var txtSLC        : UITextField!
    @IBOutlet var txtGateway    : UITextField!
    @IBOutlet var txtNearMe     : UITextField!
    @IBOutlet var txtGroup      : UITextField!
    
    @IBOutlet var slider        : UISlider!
    
    @IBOutlet var lblSlider     : UILabel!
    
    @IBOutlet var btnOkSlider   : UIButton!
    
    var isDisctance             : Bool! = false
    var isFrmSelectedAll        : Bool! = false
    
    var refreshControl          : UIRefreshControl!
    var rightButton1            : UIBarButtonItem!
    
    var context                 = LAContext()
    
    var floaty                  : Floaty!
    
    var arrClientType           : [[String : AnyObject]] = []
    
    enum AuthenticationState {
        case loggedin, loggedout
    }
    
    var state = AuthenticationState.loggedout {
        
        // Update the UI on a change.
        didSet {
            //loginButton.isHighlighted = state == .loggedin  // The button text changes on highlight.
            //stateView.backgroundColor = state == .loggedin ? .green : .red
            
            // FaceID runs right away on evaluation, so you might want to warn the user.
            //  In this app, show a special Face ID prompt if the user is logged out, but
            //  only if the device supports that kind of authentication.
            //faceIDLabel.isHidden = (state == .loggedin) || (context.biometryType != .faceID)
        }
    }
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblNtype.isHidden = true
        btnNodeType.isHidden = true
        consLamptypeHorizontal?.constant = 10
        
        
        if #available(iOS 15.0, *) {
            self.tblView.sectionHeaderTopPadding = 0
        }
        
        scrollview.contentInsetAdjustmentBehavior = .never
        scrollview.contentSize.height = 1.0
        
        btnSelected = btnFirst
        
        context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        lblHeader.text  = "COMMUNICATION FAULT".localized()
        
        txtGateway.isHidden = true
        
        constVWHeight.constant = 0
        
        lblPrompt.text = "The values reflect the most recent polls conducted.".localized()
        /* if self.serviceHandler.objUserModel.strClienttype == "1" {
         txtGateway.isHidden = false
         lblPrompt.text = "The values reflect the most recent polls conducted by the individual gateways.".localized()
         } */
        
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            arrClientType = arr as! [[String : AnyObject]]
            
            if let clientNm = UserDefault.value(forKey: "DEFAULTCLIENT") {
                btnNodeType.setTitle("\(clientNm as? String ?? "")  ", for: .normal)
            }
            
            let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                txtGateway.isHidden = false
                if self.arrClientType.count == 3 {
                    lblPrompt.text = "The values reflect the most recent polls conducted by the individual gateways.".localized()
                }
            }
        }
        
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 100
        
        totalCount = 1
        
        lblMsg.isHidden = true
        
        arrImg = ["status_7-1","status_2","status_3","status_5","status_4","status_6"]
        
        arrInformation = [["Key" : "img",
                           "Value" : ": Communication Fault"],
                          ["Key" : "img",
                           "Value" : ": Driver Fault"],
                          ["Key" : "img",
                           "Value" : ": Photocell"],
                          ["Key" : "img",
                           "Value" : ": Lamp On"],
                          ["Key" : "img",
                           "Value" : ": Lamp Dim"],
                          ["Key" : "img",
                           "Value" : ": Lamp Off"],
                          ["Key" : "LS",
                           "Value" : ": Lamp Status"],
                          ["Key" : "VUO",
                           "Value" : ": Voltage Under Over"],
                          ["Key" : "LC",
                           "Value" : ": Lamp"],
                          ["Key" : "C",
                           "Value" : ": Communication"],
                          ["Key" : "D",
                           "Value" : ": Driver"],
                          ["Key" : "Volt",
                           "Value" : ": Voltage"],
                          ["Key" : "Cur",
                           "Value" : ": Current"],
                          ["Key" : "KW",
                           "Value" : ": KiloWatt"],
                          ["Key" : "Cum_Kwh",
                           "Value" : ": Cumulative KiloWatt Hrs"],
                          ["Key" : "BHrs",
                           "Value" : ": Burn Hrs"],
                          ["Key" : "Dim",
                           "Value" : ": Dimming"],
                          ["Key" : "PF",
                           "Value" : ": Power Factor"],
                          ["Key" : "Mode",
                           "Value" : ": Mode"]]
        
        
        arrSelectdTag = []
        
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "Search"), style: UIBarButtonItem.Style.done, target:self, action:#selector(rightButtonSearchClick))
        rightButton1 = UIBarButtonItem(image: UIImage.init(named: "near-me"), style: UIBarButtonItem.Style.plain, target:self, action:#selector(lftButtonInfoClick))
        self.navigationItem.rightBarButtonItems = [rightButton, rightButton1]
        
        let lftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "Command"), style: UIBarButtonItem.Style.done, target:self, action:#selector(lftBtnClick))
        let lftButton1: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "Sent_Command"), style: UIBarButtonItem.Style.done, target:self, action:#selector(lftSentCommand))
        self.navigationItem.leftBarButtonItems = [lftButton1, lftButton]
        
        
        DispatchQueue.main.async {
            self.vwSelectedTab.frame = CGRect.init(x:5, y: self.vwSelectedTab.frame.origin.y, width: 50, height: 3)
        }
        
        arrTblData = []
        
        tblView.tableFooterView = UIView()
        
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if floaty != nil {
            floaty.removeFromSuperview()
        }
        //isDisctance = false
        localize()
        
        arrGateway = []
        btnStatus_Click(sender: btnSelected)
        let strNm : String = UserDefault.value(forKey: "LAMPNAME") as! String
        
        btnLmpType.setTitle(strNm+"     ", for: .normal)
        
        tblView1.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != tblView && scrollView != tblView1 && scrollView != tblView2{
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                scrollView.contentOffset.y = 0
            }
        }
    }
    
    func localize() {
        //self.title          = "Status".localized()
        
        /*if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
         arrClientType = arr as! [[String : AnyObject]]
         if let clientNm = UserDefault.value(forKey: "DEFAULTCLIENT") {
         btnNodeType.setTitle("\(clientNm as? String ?? "")  ", for: .normal)
         }
         }*/
        
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            arrClientType = arr as! [[String : AnyObject]]
            
            if let clientNm = UserDefault.value(forKey: "DEFAULTCLIENT") {
                var finalString = "ALL".localized()
                if clientNm as? String != "TODO" && clientNm as? String != "TODOS" {
                    finalString = (clientNm as! String).localized()
                }
                btnNodeType.setTitle("\(finalString)  ", for: .normal)
            }
        }
        
        btnFirst.setTitle("ALL".localized(), for: .normal)
        lblPrompt.text = "The values reflect the most recent polls conducted.".localized()
        if self.arrClientType.count == 3 {
            let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                lblPrompt.text = "The values reflect the most recent polls conducted by the individual gateways.".localized()
            }
        }
        
        /*if self.serviceHandler.objUserModel.strClienttype == "1" {
         lblPrompt.text = "The values reflect the most recent polls conducted by the individual gateways.".localized()
         } else {
         lblPrompt.text = "The values reflect the most recent polls conducted.".localized()
         }*/
        
        arrImgCmd       = ["common","commoff","commdim","Readdata", "Resetdata","route_plan","getMode","setMode"]
        
        arrFilter       = ["SWITCH ON".localized(),
                           "SWITCH OFF".localized(),
                           "DIM".localized(),
                           "READ DATA".localized(),
                           "RESET TRIP".localized(),
                           "ROUTE".localized(),
                           "GET MODE".localized(),
                           "SET MODE".localized()]
        
        lblSentCommand.text = "Sent Commands".localized()
        
        /*["Switch On".localized(),
         "Switch Off".localized(),
         "Dim".localized(),
         "Read Data".localized(),
         "Reset Trip".localized(),
         "Route".localized(),
         "Get Mode".localized(),
         "Set Mode".localized()]*/
        
        self.navigationItem.title  = "Status".localized()
        
        let selectLocalize = "Select".localized()
        
        txtGroup.placeholder = "Group".localized()
        
        if txtSLC.text == "Select" || txtSLC.text == "Selecionar" || txtSLC.text == "Seleccionar" {
            txtSLC.text         = selectLocalize
        }
        
        if txtGroup.text == "Select" || txtGroup.text == "Selecionar" || txtGroup.text == "Seleccionar" {
            txtGroup.text       = selectLocalize
        }
        
        if txtGateway.text == "Select" || txtGateway.text == "Selecionar" || txtGateway.text == "Seleccionar" {
            txtGateway.text     = selectLocalize
        }
        
        
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            arrClientType = arr as! [[String : AnyObject]]
            if arrClientType.count > 3 {
                lblNtype.isHidden = false
                btnNodeType.isHidden = false
                consLamptypeHorizontal?.constant = -50
            }
        }
        
        
        lblLtype.text       = "LAMP TYPE".localized()+" :"
        lblNtype.text       = "NODE TYPE".localized()+" :"
        
        btnSearch.setTitle("SEARCH".localized(), for: .normal)
        btnClear.setTitle("CLEAR".localized(), for: .normal)
        btnNearme.setTitle("NEAR ME".localized(), for: .normal)
        
        floaty = Floaty.init(frame: CGRect.init(x: self.view.frame.width - 70 , y: self.view.frame.height - ((tabBarController?.tabBar.frame.height)!+65) , width: 50, height: 50))
        
        floaty.plusColor    = UIColor.white
        floaty.buttonColor  = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 83.0/255.0, alpha: 1.0)
        floaty.sticky = true
        
        let item                   : FloatyItem = FloatyItem()
        item.title          = "Dim".localized()
        item.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item.titleLabel.textAlignment = NSTextAlignment.center
        item.titleLabel.backgroundColor = UIColor.init(red: 251.0/255.0, green: 131.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        item.titleLabel.clipsToBounds = true
        item.titleLabel.layer.cornerRadius = 6.0
        item.buttonColor = UIColor.white//UIColor.init(red: 251.0/255.0, green: 131.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        item.icon   = UIImage(named: "dashboard_Dim")!
        
        let item1                   : FloatyItem = FloatyItem()
        item1.title         = "Switch Off".localized()
        item1.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item1.titleLabel.textAlignment = NSTextAlignment.center
        item1.titleLabel.backgroundColor = UIColor.init(red: 99.0/255.0, green: 100.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        item1.titleLabel.clipsToBounds = true
        item1.titleLabel.layer.cornerRadius = 6.0
        item1.buttonColor = UIColor.white//UIColor.init(red: 99.0/255.0, green: 100.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        item1.icon   = UIImage(named: "dashboard_Off")!
        
        let item3                   : FloatyItem = FloatyItem()
        item3.title         = "Switch On".localized()
        item3.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item3.titleLabel.textAlignment = NSTextAlignment.center
        item3.titleLabel.backgroundColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        item3.titleLabel.clipsToBounds = true
        item3.titleLabel.layer.cornerRadius = 6.0
        item3.buttonColor = UIColor.white//UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        item3.icon   = UIImage(named: "dashboard_On")!
        
        let item4                   : FloatyItem = FloatyItem()
        item4.title         = "Read Data".localized()
        item4.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item4.titleLabel.textAlignment = NSTextAlignment.center
        item4.titleLabel.backgroundColor = UIColor.init(red: 246/255.0, green: 74/255.0, blue: 46/255.0, alpha: 1.0)
        item4.titleLabel.clipsToBounds = true
        item4.titleLabel.layer.cornerRadius = 6.0
        item4.buttonColor = UIColor.init(red: 246/255.0, green: 74/255.0, blue: 46/255.0, alpha: 1.0)
        item4.icon   = UIImage(named: "Readdata")!
        
        let item5                   : FloatyItem = FloatyItem()
        item5.title         = "Reset Trip".localized()
        item5.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item5.titleLabel.textAlignment = NSTextAlignment.center
        item5.titleLabel.backgroundColor = UIColor.init(red: 33/255.0, green: 151.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        item5.titleLabel.clipsToBounds = true
        item5.titleLabel.layer.cornerRadius = 6.0
        item5.buttonColor = UIColor.init(red: 33/255.0, green: 151.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        item5.icon   = UIImage(named: "Resetdata")!
        
        let item6                   : FloatyItem = FloatyItem()
        item6.title         = "More".localized()
        item6.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item6.titleLabel.textAlignment = NSTextAlignment.center
        item6.titleLabel.backgroundColor = UIColor.init(red: 174.0/255.0, green: 52.0/255.0, blue: 182.0/255.0, alpha: 1.0)
        item6.titleLabel.clipsToBounds = true
        item6.titleLabel.layer.cornerRadius = 6.0
        item6.buttonColor = UIColor.init(red: 174.0/255.0, green: 52.0/255.0, blue: 182.0/255.0, alpha: 1.0)
        item6.icon   = UIImage(named: "More")!
        
        let item7                   : FloatyItem = FloatyItem()
        item7.title         = "Route".localized()
        item7.titleLabel.font = UIFont.init(name: "Poppins-Regular" , size: 13.0)
        item7.titleLabel.textAlignment = NSTextAlignment.center
        item7.titleLabel.backgroundColor = UIColor.init(red: 34/255.0, green: 196/255.0, blue: 184/255.0, alpha: 1.0)
        item7.titleLabel.clipsToBounds = true
        item7.titleLabel.layer.cornerRadius = 6.0
        item7.buttonColor = UIColor.init(red: 34/255.0, green: 196/255.0, blue: 184/255.0, alpha: 1.0)
        item7.icon   = UIImage(named: "route_plan")!
        
        item6.handler = { item in
            //if appDelegate.isFrmPaid == "paid" {
            self.lftBtnClick()
            /*} else {
             self.showUpgardeAlert()
             //}*/
        }
        floaty.addItem(item: item6)
        
        
        item7.handler = { item in
            if appDelegate.isFrmPaid == "paid" {
                if self.arrSelectdTag.count != 0 {
                    self.routePlanner()
                } else {
                    AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
                    }
                }
            } else {
                self.showUpgardeAlert()
            }
        }
        floaty.addItem(item: item7)
        
        
        item5.handler = { item in
            if appDelegate.isFrmPaid == "paid" {
                if self.arrSelectdTag.count != 0 {
                    self.setResetData()
                } else {
                    AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
                    }
                }
            } else {
                self.showUpgardeAlert()
            }
        }
        floaty.addItem(item: item5)
        
        
        item4.handler = { item in
            if appDelegate.isFrmPaid == "paid" {
                if self.arrSelectdTag.count != 0 {
                    self.setReadData()
                } else {
                    AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
                    }
                }
            } else {
                self.showUpgardeAlert()
            }
        }
        floaty.addItem(item: item4)
        
        
        item.handler = { item in
            if appDelegate.isFrmPaid == "paid" {
                if self.arrSelectdTag.count != 0 {
                    self.checkSelectedSLCs(strType: "OFF", isFrmDim: true)
                } else {
                    AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
                    }
                }
            } else {
                self.showUpgardeAlert()
            }
        }
        floaty.addItem(item: item)
        
        
        item1.handler = { item in
            if appDelegate.isFrmPaid == "paid" {
                if self.arrSelectdTag.count != 0 {
                    self.checkSelectedSLCs(strType: "OFF", isFrmDim: false)
                } else {
                    AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
                    }
                }
            } else {
                self.showUpgardeAlert()
            }
        }
        floaty.addItem(item: item1)
        
        
        
        item3.handler = { item in
            if appDelegate.isFrmPaid == "paid" {
                if self.arrSelectdTag.count != 0 {
                    self.checkSelectedSLCs(strType: "ON", isFrmDim: false)
                } else {
                    AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
                    }
                }
            } else {
                self.showUpgardeAlert()
            }
        }
        floaty.addItem(item: item3)
        
        self.view.addSubview(floaty)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.btnBGClosed_Click(sender:btnFirst)
        floaty.close()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("status", screenClass: "status")
    }
    
    
    @objc func refresh(sender:AnyObject) {
        
        rightButton1.style = .plain
        rightButton1.tintColor = UIColor.white
        
        isFrmSelectedAll = false
        
        arrSelectdTag.removeAll()
        
        isDisctance = false
        
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        
        txtSLC.text       = "Select".localized()
        txtGroup.text     = "Select".localized()
        txtGateway.text   = "Select".localized()
        
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.getDataServer(isLoadMore: true)
        })
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: APPNAME, message: "Please enable location from Settings".localized(), preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Go To Settings".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sentCommand" {
        }  else if  segue.identifier == "SearchIdenifier"{
            let searchVC : SearchViewController = segue.destination as! SearchViewController
            searchVC.searchDelegate = self
            searchVC.strPlasHolder  = (sender as! String)
            searchVC.arrTblData     = arrSearchData
            searchVC.currentTag     = selectedTAG
            searchVC.arrMainData    = arrGateway
        } else if segue.identifier == "detailScreen" {
            let detailVC : DetailViewController = (segue.destination as? DetailViewController)!
            let dict : [String : AnyObject] = sender as! [String : AnyObject]
            detailVC.isFrmComm  = (dict["frmComm"] as! Bool)
            detailVC.arrTblData = (dict["array"] as! [AnyObject])
            detailVC.latitude   = (dict["lat"] as! String)
            detailVC.longitude  = (dict["lng"] as! String)
            detailVC.strSLCno   = (dict["slc"] as! String)
            detailVC.strAddress = ""
            if let strAdd = dict["address"] {
                detailVC.strAddress = (strAdd as! String)
            }
            if let gID = dict["gateway"] {
                detailVC.strGatewayID = (gID as! String)
            }
        } else  if segue.identifier == "mapboxIdenitifier" {
            /*let mapboxVC : MapboxViewController = (segue.destination as? MapboxViewController)!
             mapboxVC.arrSorted  = sender as! [CLLocation]*/
        }
    }
    
    @objc func lftSentCommand() {
        if appDelegate.isFrmPaid == "paid" {
            self.performSegue(withIdentifier: "sentCommand", sender: self)
        } else {
            showUpgardeAlert()
        }
    }
    
    @objc func lftBtnClick() {
        self.view.addSubview(vwFilter)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        vwFilter.frame = self.view.frame
        vwFilter.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwFilter.alpha = 1.0
        }
    }
    
    
    @objc func lftButtonInfoClick(sender : UIBarButtonItem) {
        if appDelegate.isFrmPaid == "paid" {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_nearMe"
            ])
            
            if sender.style == .plain {
                sender.style = .done
                sender.tintColor = UIColor.init(red: 92.0/255.0, green: 196.0/255.0, blue: 227.0/255.0, alpha: 1.0)
                isDisctance = true
            } else {
                sender.style = .plain
                sender.tintColor = UIColor.white
                isDisctance = false
            }
            
            
            if appDelegate.clLocation.coordinate.latitude == 0.0 {
                showAlert()
            } else {
                pageCount   = 1
                totalCount  = 1
                arrTblData.removeAll()
                
                txtSLC.text       = "Select".localized()
                txtGroup.text     = "Select".localized()
                txtGateway.text   = "Select".localized()
                
                tblView.reloadData()
                
                arrSelectdTag.removeAll()
                isFrmSelectedAll    = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.getDataServer(isLoadMore: true)
                })
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    
    //MARK:- getParamsFrmServer
    func getParamsFrmServer() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getLampList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                if let arrFrmServer : [AnyObject] = (responseData[DATA] as? [AnyObject]) {
                    if arrFrmServer.count > 0 {
                        let arrDynamic      : [AnyObject] = arrFrmServer
                        
                        var dictDynamic : [String : String] = [:]
                        
                        var strKey          : String = ""
                        var strValue        : String = ""
                        
                        for dict in arrDynamic {
                            strKey      = dict["type"] as! String
                            strValue    = dict["key"] as! String
                            dictDynamic[strKey] = strValue
                        }
                        UserDefaults().set(dictDynamic, forKey: "DYNAMIC")
                    }
                    let str : String = UserDefault.value(forKey: "LAMPNAME") as! String
                    self.btnLmpType.setTitle(str+"     ", for: .normal)
                    self.refresh(sender: self)
                }
            }
        }
    }
    
    //MARK:- getDataServer
    func getDataServer(isLoadMore : Bool) {
        if arrTblData.count != totalCount {
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            dictHeader[PAGE_NO]         = String(pageCount)
            dictHeader[PAGE_SIZE]       = "10"
            dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
            
            var dictData : [String : AnyObject] = [:]
            
            let strSelectTx = "Select".localized()
            
            dictData[G_ID]          = txtGateway.text   == strSelectTx ? "" as AnyObject : strGatwayID        as AnyObject
            dictData[SLC_ID]        = txtSLC.text       == strSelectTx ? "" as AnyObject : txtSLC.text        as AnyObject
            dictData[SLC_GROUP]     = txtGroup.text     == strSelectTx ? "" as AnyObject : strGID             as AnyObject
            dictData[POWER_PARAM]   = strPowerParam             as AnyObject
            dictData[STATUS_PARAM]  = strStatusParam            as AnyObject
            dictData["NeverCommunication"]  = isFrmPTag == true ?  1 as AnyObject : 0 as AnyObject
            dictData["LastReceivedMode"] = (strLastMode ?? "") as AnyObject
            dictData["NodeType"]      = (UserDefault.value(forKey: "DEFAULTCLIENTID") as! String) as AnyObject
            
            if isDisctance {
                dictData[LATITUDE]      = appDelegate.clLocation.coordinate.latitude  as AnyObject
                dictData[LONGITUDE]     = appDelegate.clLocation.coordinate.longitude as AnyObject
                dictData[DISTANCE]      = Float(appDelegate.strNearMe as String) as AnyObject?//1 as AnyObject// / 1609.344 as AnyObject
            }
            
            ProfileServices().getStatusList(parameters: dictData, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    let arrTmp : [AnyObject] = responseData[LIST]       as! [AnyObject]
                    self.arrTblData.append(contentsOf: arrTmp)
                    
                    self.totalCount = Int(responseData[TOTAL_RECORDS]   as! String)
                    if self.arrTblData.count == 0 {
                        if self.isDisctance {
                            self.lblMsg.text = appDelegate.strNearMsg.localized()//"Sorry, we didn’t find any SLCs within 1 mile around you!"
                        } else {
                            self.lblMsg.text = "No data Found".localized()
                        }
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    
                    var dictFilterData : [String : AnyObject] = [:]
                    dictFilterData = responseData["filterCounts"] as! [String : AnyObject]
                    
                    var badgeAppearance = BadgeAppearance()
                    badgeAppearance.textAlignment = .center //default is center
                    badgeAppearance.textSize = 8 //default is 12
                    
                    var totalSLC : Int = 0
                    if (dictFilterData["commFaults"] as! Int) > 99 {
                        self.vwBadge2.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge2.badge(text: String(dictFilterData["commFaults"] as! Int), appearance : badgeAppearance)
                    }
                    
                    if (dictFilterData["driverFault"] as! Int) > 99 {
                        self.vwBadge3.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge3.badge(text: String(dictFilterData["driverFault"] as! Int), appearance : badgeAppearance)
                    }
                    
                    if (dictFilterData["photoCellFault"] as! Int) > 99 {
                        self.vwBadge7.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge7.badge(text: String(dictFilterData["photoCellFault"] as! Int), appearance : badgeAppearance)
                    }
                    
                    if (dictFilterData["controllerOn"] as! Int) > 99 {
                        self.vwBadge4.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge4.badge(text: String(dictFilterData["controllerOn"] as! Int), appearance : badgeAppearance)
                        
                    }
                    
                    if (dictFilterData["controllerDim"] as! Int) > 99 {
                        self.vwBadge5.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge5.badge(text: String(dictFilterData["controllerDim"] as! Int), appearance : badgeAppearance)
                        
                    }
                    
                    if (dictFilterData["controllerOff"] as! Int) > 99 {
                        self.vwBadge6.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge6.badge(text: String(dictFilterData["controllerOff"] as! Int), appearance : badgeAppearance)
                        
                    }
                    
                    if (dictFilterData["neverCommunicated"] as! Int) > 99 {
                        self.vwBadge7.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge7.badge(text: String(dictFilterData["neverCommunicated"] as! Int), appearance : badgeAppearance)
                    }
                    
                    totalSLC +=  dictFilterData["controllerOn"]     as! Int
                    totalSLC +=  dictFilterData["controllerDim"]    as! Int
                    totalSLC +=  dictFilterData["driverFault"]      as! Int
                    //                    totalSLC +=  dictFilterData["neverCommunicated"] as! Int
                    totalSLC +=  dictFilterData["commFaults"]       as! Int
                    totalSLC +=  dictFilterData["controllerOff"]    as! Int
                    //                    totalSLC +=  dictFilterData["photoCellFault"]   as! Int
                    
                    if totalSLC > 99 {
                        self.vwBadge1.badge(text: "99+", appearance : badgeAppearance)
                    } else {
                        self.vwBadge1.badge(text: String(totalSLC), appearance : badgeAppearance)
                    }
                    
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    if self.arrTblData.count == 0 {
                        if self.isDisctance {
                            self.lblMsg.text = appDelegate.strNearMsg.localized()//"Sorry, we didn’t find any SLCs within 1 mile around you!"
                        } else {
                            self.lblMsg.text = "No data Found".localized()
                        }
                        self.lblMsg.isHidden = false
                    } else {
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }/* else {
              self.lblMsg.isHidden = true
              }*/
            //self.tblView.reloadData()
        }
    }
    
    //MARK: - Button clicks
    
    func sliderVwOpen() {
        if arrSelectdTag.count > 0 {
            self.view.addSubview(vwSlider)
            vwSlider.frame = self.view.bounds
            vwSlider.alpha = 0.0
            UIView.animate(withDuration: 0.15) {
                self.vwSlider.alpha = 1.0
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    
    
    @IBAction func btnNodeType_Click(sender : UIButton) {
        view.endEditing(true)
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_nodeType"
        ])
        
        let actionSheet = UIAlertController(title: "Select".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        let strStoredTitle = UserDefault.value(forKey: "DEFAULTCLIENT")
        
        for dictClientType in arrClientType {
            let strTitle : String = dictClientType["clientType"] as! String
            
            if strStoredTitle as? String ?? "" == strTitle {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                    self.dismiss(animated: true) {
                    }
                }))
            } else {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                    self.dismiss(animated: true) {
                        let selectedTitle = action.title ?? ""
                        self.btnNodeType.setTitle("\(String(describing: selectedTitle))   ", for: .normal)
                        UserDefault.set(selectedTitle, forKey: "DEFAULTCLIENT")
                        
                        let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == (selectedTitle) }
                        let value = filterdArray.first?["value"]
                        
                        UserDefault.set(value, forKey: "DEFAULTCLIENTID")
                        
                        DispatchQueue.main.async {
                            self.refresh(sender: self)
                        }
                    }
                }))
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        
        present(actionSheet, animated: true)
        
    }
    
    @IBAction func btnNearme_click(sneder : UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            lftButtonInfoClick(sender: rightButton1)
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnInfo_Click(sender : UIButton) {
        tblView.reloadData()
        self.view.addSubview(vwInformation)
        vwInformation.frame = self.view.bounds
        vwInformation.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwInformation.alpha = 1.0
        }
    }
    @IBAction func btnBGInfoClosed_Click (sender : UIButton) {
        vwInformation.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwInformation.alpha = 0.0
        }) { (isDone) in
            self.vwInformation.removeFromSuperview()
        }
    }
    
    
    @IBAction func sliderChanged(_ sender : Any){
        print(slider.value)
        print(String(describing: Int(slider.value))+" %")
        lblSlider.text = String(describing: Int(slider.value))+" %"
    }
    
    //@IBAction func rightButtonPlusClick(sender : UIBarButtonItem) {
    @IBAction func rightButtonPlusClick(sender : UIButton) {
        view.endEditing(true)
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_lampType"
        ])
        
        let actionSheet = UIAlertController(title: "Select Lamp Type".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        let arrLmpList : [AnyObject] = UserDefault.value(forKey: "LAMPLIST") as! [AnyObject]
        
        for dictLampData in arrLmpList {
            let strTitle : String = dictLampData["lampType"] as! String
            if dictLampData["isDefault"] as! String == "1" {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                    self.dismiss(animated: true) {
                    }
                }))
            } else {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                    var arrNew : [AnyObject] = []
                    for i in 0..<arrLmpList.count {
                        var di = arrLmpList[i] as! [String : String]
                        if di["lampType"]! == strTitle {
                            di["isDefault"] = "1"
                        } else {
                            di["isDefault"] = "0"
                        }
                        arrNew.append(di as AnyObject)
                    }
                    UserDefault.set(arrNew, forKey: "LAMPLIST")
                    UserDefault.set(dictLampData["lampTypeID"]!, forKey: "DEFAULTLAMP")
                    UserDefault.set(dictLampData["lampType"]!, forKey: "LAMPNAME")
                    //self.dismiss(animated: true) {
                    self.getParamsFrmServer()
                    //}
                }))
            }
        }
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        
        present(actionSheet, animated: true)
        
    }
    
    
    
    @IBAction func btnBGClosed_Click (sender : UIButton) {
        vwFilter.alpha = 1.0
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIView.animate(withDuration: 0.15, animations: {
            self.vwFilter.alpha = 0.0
        }) { (isDone) in
            self.vwFilter.removeFromSuperview()
        }
    }
    
    @IBAction func btnBGSliderClosed_Click (sender : UIButton) {
        vwSlider.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwSlider.alpha = 0.0
        }) { (isDone) in
            self.slider.value = 0
            self.lblSlider.text = String(describing: Int(self.slider.value))+" %"
            self.vwSlider.removeFromSuperview()
        }
    }
    
    
    @IBAction func rightButtonSearchClick() {
        if !isFilterOpen {
            isFilterOpen = true
            //            if self.serviceHandler.objUserModel.strClienttype == "1" {
            let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                constVWHeight .constant = 153
            } else {
                constVWHeight .constant = 100
            }
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    /*func showModal() {
     let modalViewController = DetailViewController()
     modalViewController.modalPresentationStyle = .overCurrentContext
     present(modalViewController, animated: true, completion: nil)
     }*/
    
    //MARK:- Button clicks
    
    @IBAction func btnSlider_OK_Click(sender : UIButton) {
        if slider.value > 71 {
            ShowSentAlertWithYs(strMsg: "You have selected more than 70% dimming value. The visibility might decrease significantly. Are you sure you want to continue ?".localized())
        } else {
            btnBGSliderClosed_Click(sender: sender)
            setDim()
        }
    }
    
    @IBAction func btnStatus_Click(sender : UIButton) {
        btnSelected = sender
        UIView.animate(withDuration: 0.1) {
            let xPosition : CGFloat = (CGFloat((sender.tag-1)*60))
            self.vwSelectedTab.frame = CGRect.init(x: xPosition+5, y: self.vwSelectedTab.frame.origin.y, width: self.vwSelectedTab.frame.width, height: self.vwSelectedTab.frame.height)
            self.view.layoutIfNeeded()
            /*self.vwSelectedTab.frame = CGRect.init(x:sender.frame.origin.x+5, y: self.vwSelectedTab.frame.origin.y, width: self.vwSelectedTab.frame.width, height: self.vwSelectedTab.frame.height)
             self.view.layoutIfNeeded()*/
        }
        isFrmPTag = false
        isFrmSelectedAll = false
        arrSelectdTag.removeAll()
        
        let strNm : String = UserDefault.value(forKey: "LAMPNAME") as! String
        
        btnLmpType.setTitle(strNm+"     ", for: .normal)
        
        if sender.tag == 1 {
            lblHeader.text = "ALL".localized()
            strStatusParam  = ""
            strPowerParam   = ""
        } else if sender.tag == 7 {
            lblHeader.text = "COMMUNICATION FAULT".localized()
            strStatusParam  = COMM_FAULT
            strPowerParam   = ""
        } else if sender.tag == 2 {
            lblHeader.text = "DRIVER FAULT".localized()
            strStatusParam  = DRIVER_FAULT
            strPowerParam   = ""
        } else if sender.tag == 3 {
            lblHeader.text = "LAMP ON".localized()
            strStatusParam  = LAMP_ON_STATUS
            strPowerParam   = LAMP_ON_POWER
        } else if sender.tag == 4 {
            lblHeader.text = "LAMP DIM".localized()
            strStatusParam  = LAMP_DIM_STATUS
            strPowerParam   = LAMP_DIM_POWER
        } else if sender.tag == 5 {
            lblHeader.text = "LAMP OFF".localized()
            strStatusParam  = LAMP_OFF
            strPowerParam   = ""
        } else if sender.tag == 6 {
            isFrmPTag = true
            lblHeader.text = "IN PLANNING".localized()
            strStatusParam  = ""
            strPowerParam   = ""
        }
        
        arrTblData.removeAll()
        totalCount  = 1
        pageCount   = 1
        tblView.reloadData()
        getDataServer(isLoadMore: true)
    }
    
    @IBAction func btnCheck_Click(sender : UIButton) {
        let cell : CellStatus = tblView.cellForRow(at: IndexPath.init(row:  sender.tag, section: 0)) as! CellStatus
        //if arrSelectdTag.count > 0 {
        if arrSelectdTag.contains(sender.tag) {
            let index = arrSelectdTag.index(of : sender.tag)
            arrSelectdTag.remove(at: index!)
            cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
        } else {
            arrSelectdTag.append(sender.tag)
            cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
        }
        /*} else {
         arrSelectdTag.append(sender.tag)
         cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
         }*/
    }
    
    func hasSpecialCharacters(str : String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "@", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: str, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, str.count)) {
                return true
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
        
        return false
    }
    
    func getLocationFrmArray() -> [CLLocation] {
        var arrLocation : [CLLocation] = []
        for i in 0..<arrTblData.count {
            if arrSelectdTag.contains(i) {
                if (arrTblData![i]["longitude"] as? String) != ""  {
                    let cll : CLLocation = CLLocation.init(latitude:Double(arrTblData![i]["latitude"]! as! String)!, longitude: Double(arrTblData![i]["longitude"]! as! String)!)
                    arrLocation.append(cll)
                }
            }
        }
        return arrLocation
    }
    
    
    func getStringFrmArray() -> String {
        var arrIds : [String] = []
        for i in 0..<arrTblData.count {
            if arrSelectdTag.contains(i) {
                arrIds.append(String(describing: arrTblData![i]["slcNo"] as AnyObject))
            }
        }
        let stringRepresentation = arrIds.joined(separator: ",")
        return stringRepresentation
    }
    
    func showAlert(strTitle : String, modeID : String) {
        let alertController = UIAlertController(title: APPNAME, message: strTitle, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            if self.arrSelectdTag.count > 10 {
                let alertController: UIAlertController = UIAlertController(title: APPNAME, message: "Please confirm you want to send this command to multiple SLCs".localized(), preferredStyle: .alert)
                let goButton: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .default) { action -> Void in}
                let okButton: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                    
                    self.context = LAContext()
                    self.context.localizedCancelTitle = "Enter Username/Password".localized()
                    
                    // First check if we have the needed hardware support.
                    var error: NSError?
                    if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                        
                        let reason = "Log in to your account".localized()
                        self.context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                            
                            if success {
                                // Move to the main thread because a state update triggers UI changes.
                                DispatchQueue.main.async {
                                    self.state = .loggedin
                                    //AlertView().showAlert(strMsg: "AUTHENICATION", btntext: "OK", completion: { (str) in
                                    self.setMode(strModeID: modeID)
                                    //})
                                }
                                
                            } else {
                                DispatchQueue.main.async {
                                    let alertController: UIAlertController = UIAlertController(title: "Password".localized(), message: "To continue please enter your password again".localized(), preferredStyle: .alert)
                                    //cancel button
                                    let cancelAction: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                                        //cancel code
                                    }
                                    alertController.addAction(cancelAction)
                                    
                                    //Create an optional action
                                    let nextAction: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                                        let text = (alertController.textFields?.first!)!.text
                                        if text == "" {
                                            AlertView().showAlert(strMsg: "Please enter your password".localized(), btntext: "OK".localized(), completion: { (str) in
                                                self.showAlert(strTitle: strTitle, modeID: modeID)
                                            })
                                        } else {
                                            var dictHeader : [String : String] = [:]
                                            dictHeader[UNAME_SMALL]     = self.serviceHandler.objUserModel.strUName
                                            dictHeader[PWD]             = text
                                            dictHeader[GRANT_TYPE]      = DEFAULT_PWD
                                            dictHeader["ClientID"]      = (UserdefaultManager().getPreferenceForkey("ClientID") as? String)
                                            
                                            let dictParams : [String : AnyObject] = [:]
                                            
                                            ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                                                if isSuccess {
                                                    self.setMode(strModeID: modeID)
                                                }
                                            }
                                        }
                                    }
                                    
                                    alertController.addTextField(configurationHandler: { (textField) in
                                        textField.isSecureTextEntry = true
                                        textField.placeholder = "Enter your password".localized()
                                    })
                                    
                                    alertController.addAction(nextAction)
                                    
                                    self.present(alertController, animated: true, completion: nil)
                                }
                            }
                        }
                    } else {
                        print(error?.localizedDescription ?? "Can't evaluate policy")
                        DispatchQueue.main.async {
                            let alertController: UIAlertController = UIAlertController(title: "Password".localized(), message: "To continue please enter your password again".localized(), preferredStyle: .alert)
                            //cancel button
                            let cancelAction: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                                //cancel code
                            }
                            alertController.addAction(cancelAction)
                            
                            //Create an optional action
                            let nextAction: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                                let text = (alertController.textFields?.first!)!.text
                                if text == "" {
                                    AlertView().showAlert(strMsg: "Please enter your password".localized(), btntext: "OK".localized(), completion: { (str) in
                                        self.showAlert(strTitle: strTitle, modeID: modeID)
                                    })
                                } else {
                                    var dictHeader : [String : String] = [:]
                                    dictHeader[UNAME_SMALL]     = self.serviceHandler.objUserModel.strUName
                                    dictHeader[PWD]             = text
                                    dictHeader[GRANT_TYPE]      = DEFAULT_PWD
                                    dictHeader["ClientID"]      = (UserdefaultManager().getPreferenceForkey("ClientID") as? String)
                                    
                                    let dictParams : [String : AnyObject] = [:]
                                    
                                    ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                                        if isSuccess {
                                            self.setMode(strModeID: modeID)
                                        }
                                    }
                                }
                            }
                            
                            alertController.addTextField(configurationHandler: { (textField) in
                                textField.isSecureTextEntry = true
                                textField.placeholder = "Enter your password".localized()
                            })
                            
                            alertController.addAction(nextAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
                alertController.addAction(goButton)
                alertController.addAction(okButton)
                self.present(alertController, animated: true, completion: nil)
            } else {
                self.setMode(strModeID: modeID)
                /* let alertController: UIAlertController = UIAlertController(title: APPNAME, message: "Please confirm you want to send this command to multiple SLCs".localized(), preferredStyle: .alert)
                 let goButton: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .default) { action -> Void in}
                 let okButton: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                 }
                 alertController.addAction(goButton)
                 alertController.addAction(okButton)
                 self.present(alertController, animated: true, completion: nil)*/
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- ActionMapSheet
    func actionMapSheet(arrData : [AnyObject]) {
        view.endEditing(true)
        
        let actionSheet = UIAlertController(title: "Select Mode".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        for dict in arrData {
            actionSheet.addAction(UIAlertAction(title: (dict["modeName"] as! String), style: .default, handler: { action in
                self.dismiss(animated: true) {
                }
                for di in arrData {
                    if action.title == (di["modeName"] as! String) {
                        var msgForAlert : String = ""
                        if action.title == "Manual" {
                            msgForAlert = "Setting the mode to Manual will cause the light to use the ON/OFF state it was last in when set in Manual mode. Please verify the state of the light with a Read Data command and refresh the Status page or Map to ensure light is in the desired ON/OFF state.".localized()
                            let msgForAlert1 = "Select \"OK\" to continue or \"Cancel\" to stop the command".localized()
                            self.showAlert(strTitle: "\(msgForAlert) \n\n \(msgForAlert1)", modeID: (di["modeID"] as! String))
                        } else {
                            /* else if action.title == "Astro Clock" {
                             msgForAlert = "Astro Clock cannot be set for one or more SLC(s). Set Latitude/Longitude and DST Rule in SLC before setting the SLCs in Astro Clock. \n\n  You have already sent the commands but the commands are not yet acknowledged. Check the status of your commands using More tab. \n\n You have not yet sent the commands."
                             self.showAlert(strTitle: msgForAlert, modeID: (di["modeID"] as! String))
                             } else if action.title == "Mix-Mode" {
                             msgForAlert = "Mixed mode cannot be set for one or more SLC(s). Mixed Mode Schedule need to be sent before setting the SLCs in Mixed Mode. \n\n You have not yet set the Mixed mode schedule. Ensure Mixed mode Schedule template is created and use Mixed Mode Set option to send the command. \n\n  You have already sent Mixed Mode Schedule but the command is not yet acknowledged. Check the status of your commands using More tab."
                             self.showAlert(strTitle: msgForAlert, modeID: (di["modeID"] as! String))
                             } else{*/
                            if self.arrSelectdTag.count > 10 {
                                let alertController: UIAlertController = UIAlertController(title: APPNAME, message: "Please confirm you want to send this command to multiple SLCs".localized(), preferredStyle: .alert)
                                let goButton: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .default) { action -> Void in}
                                let okButton: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                                    self.context = LAContext()
                                    self.context.localizedCancelTitle = "Enter Username/Password".localized()
                                    
                                    // First check if we have the needed hardware support.
                                    var error: NSError?
                                    if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                                        
                                        let reason = "Log in to your account".localized()
                                        self.context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                                            
                                            if success {
                                                // Move to the main thread because a state update triggers UI changes.
                                                DispatchQueue.main.async {
                                                    self.state = .loggedin
                                                    //AlertView().showAlert(strMsg: "AUTHENICATION", btntext: "OK", completion: { (str) in
                                                    self.setMode(strModeID: (di["modeID"] as! String))
                                                    //})
                                                }
                                            } else {
                                                DispatchQueue.main.async {
                                                    let alertController: UIAlertController = UIAlertController(title: "Password".localized(), message: "To continue please enter your password again".localized(), preferredStyle: .alert)
                                                    //cancel button
                                                    let cancelAction: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                                                        //cancel code
                                                    }
                                                    alertController.addAction(cancelAction)
                                                    
                                                    //Create an optional action
                                                    let nextAction: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                                                        let text = (alertController.textFields?.first!)!.text
                                                        if text == "" {
                                                            AlertView().showAlert(strMsg: "Please enter your password".localized(), btntext: "OK".localized(), completion: { (str) in
                                                                //self.showAlert(strTitle: strTitle, modeID: modeID)
                                                            })
                                                        } else {
                                                            var dictHeader : [String : String] = [:]
                                                            dictHeader[UNAME_SMALL]     = self.serviceHandler.objUserModel.strUName
                                                            dictHeader[PWD]             = text
                                                            dictHeader[GRANT_TYPE]      = DEFAULT_PWD
                                                            dictHeader["ClientID"]      = (UserdefaultManager().getPreferenceForkey("ClientID") as? String)
                                                            
                                                            let dictParams : [String : AnyObject] = [:]
                                                            
                                                            ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                                                                if isSuccess {
                                                                    self.setMode(strModeID: (di["modeID"] as! String))
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                    alertController.addTextField(configurationHandler: { (textField) in
                                                        textField.isSecureTextEntry = true
                                                        textField.placeholder = "Enter your password".localized()
                                                    })
                                                    
                                                    alertController.addAction(nextAction)
                                                    
                                                    self.present(alertController, animated: true, completion: nil)
                                                }
                                            }
                                        }
                                    } else {
                                        print(error?.localizedDescription ?? "Can't evaluate policy")
                                        DispatchQueue.main.async {
                                            let alertController: UIAlertController = UIAlertController(title: "Password".localized(), message: "To continue please enter your password again".localized(), preferredStyle: .alert)
                                            //cancel button
                                            let cancelAction: UIAlertAction = UIKit.UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                                                //cancel code
                                            }
                                            alertController.addAction(cancelAction)
                                            
                                            //Create an optional action
                                            let nextAction: UIAlertAction = UIKit.UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                                                let text = (alertController.textFields?.first!)!.text
                                                if text == "" {
                                                    AlertView().showAlert(strMsg: "Please enter your password".localized(), btntext: "OK".localized(), completion: { (str) in
                                                        //self.showAlert(strTitle: strTitle, modeID: modeID)
                                                    })
                                                } else {
                                                    var dictHeader : [String : String] = [:]
                                                    dictHeader[UNAME_SMALL]     = self.serviceHandler.objUserModel.strUName
                                                    dictHeader[PWD]             = text
                                                    dictHeader[GRANT_TYPE]      = DEFAULT_PWD
                                                    dictHeader["ClientID"]      = (UserdefaultManager().getPreferenceForkey("ClientID") as? String)
                                                    
                                                    let dictParams : [String : AnyObject] = [:]
                                                    
                                                    ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                                                        if isSuccess {
                                                            self.setMode(strModeID: (di["modeID"] as! String))
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            alertController.addTextField(configurationHandler: { (textField) in
                                                textField.isSecureTextEntry = true
                                                textField.placeholder = "Enter your password".localized()
                                            })
                                            
                                            alertController.addAction(nextAction)
                                            
                                            self.present(alertController, animated: true, completion: nil)
                                        }
                                    }
                                }
                                alertController.addAction(goButton)
                                alertController.addAction(okButton)
                                self.present(alertController, animated: true, completion: nil)
                            } else {
                                self.setMode(strModeID: (di["modeID"] as! String))
                            }
                        }
                    }
                }
            }))
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        present(actionSheet, animated: true)
    }
    
    
    //MARK:- getDataforGetMode
    func getDataforGetMode() {
        
        let dictDta : [String : AnyObject] = [:]
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getSetMode(parameters: dictDta, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let dictData : [AnyObject] = responseData[DATA] as! [AnyObject]
                self.actionMapSheet(arrData: dictData)
            }
        }
    }
    
    
    //MARK:- setMode
    func setMode(strModeID : String) {
        
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_setMode"
            ])
            
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            
            var dictTmp : [String : String] = [:]
            dictTmp["ModeTypeValue"]  = strModeID
            dictTmp[SLC_LIST_ID]    = getStringFrmArray()
            let arrTmp : [AnyObject] = [dictTmp as AnyObject]
            
            var dictParams : [String : AnyObject] = [:]
            dictParams["SetModeSLC"] = arrTmp as AnyObject
            
            ProfileServices().setMode(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    
                    let str1 = "The command has been queued.".localized()
                    let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                    let str3 = "Track ID".localized()+": "
                    
                    let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                    if arrTmp.count > 1 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                    } else {
                        if arrTmp.count != 0 {
                            self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                        } else {
                            AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                                self.refresh(sender: self)
                            })
                        }
                    }
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    
    
    //MARK:- GETMODE
    func getMode() {
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_getMode"
            ])
            
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            
            var dictTmp : [String : String] = [:]
            //        dictTmp[CMD]            = "Get Mode"
            dictTmp[SLC_LIST_ID]    = getStringFrmArray()
            let arrTmp : [AnyObject] = [dictTmp as AnyObject]
            
            var dictParams : [String : AnyObject] = [:]
            dictParams["GetMode"] = arrTmp as AnyObject
            
            ProfileServices().getMode(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    
                    let str1 = "The command has been queued.".localized()
                    let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                    let str3 = "Track ID".localized()+": "
                    
                    let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                    if arrTmp.count > 1 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                    } else {
                        if arrTmp.count != 0 {
                            self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                        } else {
                            AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                                self.refresh(sender: self)
                            })
                        }
                    }
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    
    
    func checkSelectedSLCs(strType : String, isFrmDim : Bool) {
        if arrSelectdTag.count > 10 {
            let alertController: UIAlertController = UIAlertController(title: APPNAME, message: "Please confirm you want to send this command to multiple SLCs".localized(), preferredStyle: .alert)
            let goButton: UIAlertAction = UIAlertAction(title: "Cancel".localized(), style: .default) { action -> Void in}
            let okButton: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                
                self.context = LAContext()
                self.context.localizedCancelTitle = "Enter Username/Password".localized()
                
                // First check if we have the needed hardware support.
                var error: NSError?
                if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                    
                    let reason = "Log in to your account"
                    self.context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                        
                        if success {
                            // Move to the main thread because a state update triggers UI changes.
                            DispatchQueue.main.async {
                                self.state = .loggedin
                                //AlertView().showAlert(strMsg: "AUTHENICATION", btntext: "OK", completion: { (str) in
                                if isFrmDim {
                                    self.sliderVwOpen()
                                } else {
                                    self.setOnOff(str_CMD: strType)
                                }
                                //})
                            }
                            
                        } else {
                            print(error?.localizedDescription ?? "FAILED AUTHENICATION")
                            DispatchQueue.main.async {
                                let alertController: UIAlertController = UIAlertController(title: "Password", message: "To continue please enter your password again".localized(), preferredStyle: .alert)
                                //cancel button
                                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                                    //cancel code
                                }
                                alertController.addAction(cancelAction)
                                
                                //Create an optional action
                                let nextAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                                    let text = (alertController.textFields?.first!)!.text
                                    if text == "" {
                                        AlertView().showAlert(strMsg: "Please enter your password".localized(), btntext: "OK".localized(), completion: { (str) in
                                            self.checkSelectedSLCs(strType: strType, isFrmDim: isFrmDim)
                                        })
                                    } else {
                                        var dictHeader : [String : String] = [:]
                                        dictHeader[UNAME_SMALL]     = self.serviceHandler.objUserModel.strUName
                                        dictHeader[PWD]             = text
                                        dictHeader[GRANT_TYPE]      = DEFAULT_PWD
                                        dictHeader["ClientID"]      = (UserdefaultManager().getPreferenceForkey("ClientID") as? String)
                                        
                                        let dictParams : [String : AnyObject] = [:]
                                        
                                        ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                                            if isSuccess {
                                                if isFrmDim {
                                                    self.sliderVwOpen()
                                                } else {
                                                    self.setOnOff(str_CMD: strType)
                                                }
                                            } else {
                                                self.checkSelectedSLCs(strType: strType, isFrmDim: isFrmDim)
                                            }
                                        }
                                    }
                                }
                                
                                alertController.addTextField(configurationHandler: { (textField) in
                                    textField.isSecureTextEntry = true
                                    textField.placeholder = "Enter your password".localized()
                                })
                                
                                alertController.addAction(nextAction)
                                
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                } else {
                    print(error?.localizedDescription ?? "Can't evaluate policy")
                    DispatchQueue.main.async {
                        let alertController: UIAlertController = UIAlertController(title: "Password".localized(), message: "To continue please enter your password again".localized(), preferredStyle: .alert)
                        //cancel button
                        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
                            //cancel code
                        }
                        alertController.addAction(cancelAction)
                        
                        //Create an optional action
                        let nextAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in
                            let text = (alertController.textFields?.first!)!.text
                            if text == "" {
                                AlertView().showAlert(strMsg: "Please enter your password".localized(), btntext: "OK".localized(), completion: { (str) in
                                    self.checkSelectedSLCs(strType: strType, isFrmDim: isFrmDim)
                                })
                            } else {
                                var dictHeader : [String : String] = [:]
                                dictHeader[UNAME_SMALL]     = self.serviceHandler.objUserModel.strUName
                                dictHeader[PWD]             = text
                                dictHeader[GRANT_TYPE]      = DEFAULT_PWD
                                dictHeader["ClientID"]      = (UserdefaultManager().getPreferenceForkey("ClientID") as? String)
                                
                                let dictParams : [String : AnyObject] = [:]
                                
                                ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                                    if isSuccess {
                                        if isFrmDim {
                                            self.sliderVwOpen()
                                        } else {
                                            self.setOnOff(str_CMD: strType)
                                        }
                                    } else {
                                        self.checkSelectedSLCs(strType: strType, isFrmDim: isFrmDim)
                                    }
                                }
                            }
                        }
                        
                        alertController.addTextField(configurationHandler: { (textField) in
                            textField.isSecureTextEntry = true
                            textField.placeholder = "Enter your password".localized()
                        })
                        
                        alertController.addAction(nextAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            
            alertController.addAction(goButton)
            alertController.addAction(okButton)
            self.present(alertController, animated: true, completion: nil)
        } else {
            if isFrmDim {
                sliderVwOpen()
            } else {
                self.setOnOff(str_CMD: strType)
            }
        }
    }
    
    func ShowSentAlertWithYs(strMsg : String) {
        let alertController: UIAlertController = UIAlertController(title: APPNAME, message: strMsg, preferredStyle: .alert)
        let okButton: UIAlertAction = UIAlertAction(title: "YES".localized(), style: .default) { action -> Void in
            self.btnBGSliderClosed_Click(sender: self.btnFirst)
            self.setDim()
        }
        let goButton: UIAlertAction = UIAlertAction(title: "NO".localized(), style: .default) { action -> Void in}
        alertController.addAction(goButton)
        alertController.addAction(okButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func ShowSentAlert(strMsg : String) {
        let alertController: UIAlertController = UIAlertController(title: APPNAME, message: strMsg, preferredStyle: .alert)
        let okButton: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in}
        let goButton: UIAlertAction = UIAlertAction(title: "GO".localized(), style: .default) { action -> Void in
            self.lftSentCommand()
        }
        alertController.addAction(okButton)
        alertController.addAction(goButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- On Off
    func setOnOff(str_CMD : String) {
        if arrSelectdTag.count > 0 {
            var strEvents : String = ""
            if str_CMD == "ON" {
                strEvents = "switchOn"
            } else {
                strEvents = "switchOff"
            }
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_\(strEvents)"
            ])
            
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            
            var dictTmp : [String : String] = [:]
            dictTmp[CMD]            = str_CMD
            dictTmp[SLC_LIST_ID]    = getStringFrmArray()
            let arrTmp : [AnyObject] = [dictTmp as AnyObject]
            
            var dictParams : [String : AnyObject] = [:]
            dictParams[SWITCH_ON_OFF] = arrTmp as AnyObject
            
            ProfileServices().switchOnOff(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    
                    let str1 = "The command has been queued.".localized()
                    let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                    let str3 = "Track ID".localized()+": "
                    
                    let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                    if arrTmp.count > 1 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                    } else {
                        if arrTmp.count != 0 {
                            self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                        } else {
                            AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                                self.refresh(sender: self)
                            })
                        }
                    }
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    
    //MARK:- DIM
    func setDim() {
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_dim"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        dictTmp[DIM_VALUE]      = (lblSlider.text)?.replacingOccurrences(of: " %", with: "")
        dictTmp[SLC_LIST_ID]    = getStringFrmArray()
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams["DIMSLC"] = arrTmp as AnyObject
        
        ProfileServices().switchDim(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            self.refresh(sender: self)
                        })
                    }
                }
            }
        }
        
        
    }
    
    
    //MARK: - Reset Data
    func setResetData() {
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_resetTrip"
            ])
            
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            
            var dictTmp : [String : String] = [:]
            dictTmp[CMD]            = "Reset SLC Trip"
            dictTmp[SLC_LIST_ID]    = getStringFrmArray()
            let arrTmp : [AnyObject] = [dictTmp as AnyObject]
            
            var dictParams : [String : AnyObject] = [:]
            dictParams[RESET_SLC_TRIP] = arrTmp as AnyObject
            
            ProfileServices().setResetData(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    
                    let str1 = "The command has been queued.".localized()
                    let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                    let str3 = "Track ID".localized()+": "
                    
                    let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                    if arrTmp.count > 1 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                    } else {
                        if arrTmp.count != 0 {
                            self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                        } else {
                            AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                                self.refresh(sender: self)
                            })
                        }
                    }
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    
    
    //MARK:- Read Data
    func setReadData() {
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_readData"
            ])
            
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            
            var dictTmp : [String : String] = [:]
            //dictTmp[CMD]            = "ON"
            dictTmp[SLC_LIST_ID]    = getStringFrmArray()
            let arrTmp : [AnyObject] = [dictTmp as AnyObject]
            
            var dictParams : [String : AnyObject] = [:]
            dictParams[READ_DATA]   = arrTmp as AnyObject
            
            ProfileServices().getReadData(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    
                    let str1 = "The command has been queued.".localized()
                    let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                    let str3 = "Track ID".localized()+": "
                    
                    let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                    if arrTmp.count > 1 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                    } else {
                        if arrTmp.count != 0 {
                            self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                        } else {
                            AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                                self.refresh(sender: self)
                            })
                        }
                    }
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    
    //MARK:- getWayList
    func getWayList() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getCommandGatwayList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                self.arrGateway = (responseData[DATA] as! [AnyObject])
                self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "2")
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "Gateway")
            }
        }
    }
    
    
    //MARK:- getNearme
    func getNearme() {
        
    }
    
    func getvalueFrmDict(dict : [String : AnyObject], isFrm : String) -> [String] {
        var arrTmp : [String] = []
        if isFrm == "1" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["value"] as! String)
            }
        } else  if isFrm == "2" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["gatewayName"] as! String)
            }
        } else if isFrm == "3" {
            
        } else if isFrm == "4" {
            
        }
        return arrTmp
    }
    
    
    //MARK:- Button clicks
    @IBAction func btnCheckAll_Click(sender : UIButton) {
        print(sender.isSelected)
        arrSelectdTag.removeAll()
        if !sender.isSelected {
            sender.isSelected = true
            isFrmSelectedAll = true
            for i in 0..<arrTblData!.count {
                arrSelectdTag.append(i)
            }
        } else {
            isFrmSelectedAll = false
            sender.isSelected = false
        }
        tblView.reloadData()
    }
    
    @IBAction func searchBtnClick (sender : UIButton) {
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_searchStatus"
        ])
        
        UIView.animate(withDuration: 0.1) {
            self.vwSelectedTab.frame = CGRect.init(x: self.btnFirst.frame.origin.x , y: self.vwSelectedTab.frame.origin.y, width: self.btnFirst.frame.width, height: self.vwSelectedTab.frame.height)
            self.view.layoutIfNeeded()
        }
        lblHeader.text      = "ALL".localized()
        strStatusParam      = ""
        strPowerParam       = ""
        pageCount           = 1
        totalCount          = 1
        arrSelectdTag.removeAll()
        isFrmPTag           = false
        isFrmSelectedAll    = false
        arrTblData.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    @IBAction func clearBtn_Click(sender : UIButton) {
        
        rightButton1.style = .plain
        rightButton1.tintColor = UIColor.white
        
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        
        arrSelectdTag.removeAll()
        arrSelectdTag.removeAll()
        isFrmSelectedAll    = false
        
        isDisctance = false
        
        txtSLC.text       = "Select".localized()
        txtGroup.text     = "Select".localized()
        //txtNearMe.text    = "Select"
        txtGateway.text   = "Select".localized()
        
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
}


extension StatusVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView {
            return arrTblData.count
        } else if tableView == tblView1 {
            return arrFilter.count
        } else {
            return arrInformation.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /*if tableView == tblView {
         if strPowerParam == LAMP_DIM_POWER {
         return 50.0
         } else {
         return 30.0
         }
         } else {*/
        return UITableView.automaticDimension
        //}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView {
            let cell : CellStatus = tableView.dequeueReusableCell(withIdentifier: "CellStatus") as! CellStatus
            
            let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
            
            let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            let darkGray  = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 111/255, green: 113/255, blue: 121/255, alpha: 1)]
            
            let strDate = (dictData[LAST_COMMUNICATION_ON] as! String) == "" ? "N/A" : (dictData[DATE_TIME] as! String)
            if strDate == "N/A" {
                cell.lblDt.text = strDate
            } else {
                let arrStrDate = strDate.components(separatedBy: " ")
                let mutableAttrString1 = NSMutableAttributedString(string: arrStrDate[0]+" ", attributes: darkGray)
                let mutableAttrString2 = NSMutableAttributedString(string: arrStrDate[1], attributes: lightGray)
                
                let finalMutable = NSMutableAttributedString()
                finalMutable.append(mutableAttrString1)
                finalMutable.append(mutableAttrString2)
                
                cell.lblDt.attributedText = finalMutable
            }
            
            cell.lblSLCNm.text      = dictData["name"]      as? String
            if let slcNo = dictData["slcNo"] {
                cell.lblSLCNo.text = String(describing : slcNo)
            }
            
            cell.constDimHeight?.constant = 0
            
            if strPowerParam == LAMP_DIM_POWER {
                cell.constDimHeight?.constant = 15
                cell.lblHeadDim.text = "Dim".localized()
                if let dimming = dictData["@p6"] {
                    cell.lblBhrs.text = ": "+String(describing : dimming)+" %"
                } else {
                    cell.lblBhrs.text = ": 0 %"
                }
            }
            
            if lblHeader.text == "IN PLANNING".localized() {
                cell.imgArrow?.isHidden = true
            } else {
                cell.imgArrow?.isHidden = false
            }
            
            if indexPath.row % 2 != 0 {
                cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
            } else {
                cell.backgroundColor = UIColor.white
            }
            
            cell.btnCheck.addTarget(self, action: #selector(btnCheck_Click), for: .touchUpInside)
            cell.btnCheck.tag = indexPath.row
            
            if arrSelectdTag.contains(indexPath.row) {
                cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
            } else {
                cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
            }
            
            if indexPath.row == arrTblData.count-1 {
                getDataServer(isLoadMore: true)
            }
            
            return cell
            
        } else if tableView == tblView1 {
            let cell : CellFilter = tableView.dequeueReusableCell(withIdentifier: "CellFilter") as! CellFilter
            cell.btnFilter.setTitle(arrFilter[indexPath.row], for: .normal)
            cell.imgView.image = UIImage.init(named: arrImgCmd[indexPath.row])
            if indexPath.row == 0 {
                //                cell.btnFilter.backgroundColor = UIColor.init(red: 39.0/255.0, green: 170.0/255.0, blue: 12.0/255.0, alpha: 1.0)
                cell.btnFilter.backgroundColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 84.0/255.0, alpha: 1.0)
            } else if indexPath.row == 1 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 99.0/255.0, green: 100.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            } else if indexPath.row == 2 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 252.0/255.0, green: 127.0/255.0, blue: 49.0/255.0, alpha: 1.0)
            } else if indexPath.row == 3 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 246/255.0, green: 74/255.0, blue: 46/255.0, alpha: 1.0)
            } else if indexPath.row == 4 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 174.0/255.0, blue: 283.0/255.0, alpha: 1.0)
            } else if indexPath.row == 5 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 202/255.0, blue: 200/255.0, alpha: 1.0)
            } else if indexPath.row == 6 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 114.0/255.0, blue: 188.0/255.0, alpha: 1.0)
            } else {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 65.0/255.0, blue: 111.0/255.0, alpha: 1.0)
            }
            return cell
        } else {
            let cell : CellDashboard = tableView.dequeueReusableCell(withIdentifier: "CellDashboard") as! CellDashboard
            cell.btnNm.setTitle(arrInformation![indexPath.row]["Value"], for: .normal)
            cell.btnKey.setTitle(arrInformation![indexPath.row]["Key"], for: .normal)
            if indexPath.row > 5 {
                cell.imgView?.isHidden = true
                cell.btnKey.isHidden = false
            } else {
                cell.btnKey.isHidden = true
                cell.imgView?.isHidden = false
                cell.imgView?.image = UIImage.init(named:arrImg[indexPath.row])
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if appDelegate.isFrmPaid == "paid" {
            /*if lblHeader.text == "IN PLANNING".localized() {
             } else {*/
            if tableView == self.tblView {
                let cell : CellStatus = tableView.cellForRow(at: indexPath) as! CellStatus
                if cell.lblDt.text != "N/A" {
                    let dictData : [String : AnyObject] = self.arrTblData[indexPath.row] as! [String : AnyObject]
                    
                    var finalDictData : [String : AnyObject] = [:]
                    var arrSymbol : [AnyObject] = []
                    var dictTmp : [String : String] = [:]
                    
                    let dictDynaic : [String : String] = UserDefaults().value(forKey: "DYNAMIC") as! [String : String]
                    
                    var isFrmComm : Bool = false
                    
                    dictTmp["KEY"]      = "SLC Name".localized()
                    dictTmp["VALUE"]    = (dictData["name"] as! String)
                    dictTmp["FOR"]      = "D"
                    arrSymbol.append(dictTmp as AnyObject)
                    
                    if let communication = dictData["@s8"] {
                        if communication as! String == "0" {
                            dictTmp["KEY"]      = dictDynaic["s8"]//results.last
                            dictTmp["VALUE"]    = "0"//String(describing: value)
                            dictTmp["FOR"]      = "A"//key.hasPrefix("a") == true ? "A" : "D"
                            arrSymbol.append(dictTmp as AnyObject)
                            isFrmComm = true
                        } else {
                            let sortedDic = dictData.sorted { (aDic, bDic) -> Bool in
                                return aDic.key < bDic.key
                            }
                            for (key, val) in sortedDic  {
                                if self.hasSpecialCharacters(str: key) {
                                    let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
                                    dictTmp["KEY"]      = dictDynaic[results.last!]
                                    if let value = val as? AnyObject {
                                        dictTmp["VALUE"] = String(describing: value)
                                    }
                                    dictTmp["FOR"]      = results.last!.hasPrefix("s") == true ? "A" : "D"
                                    arrSymbol.append(dictTmp as AnyObject)
                                }
                            }
                        }
                    } else {
                        let sortedDic = dictData.sorted { (aDic, bDic) -> Bool in
                            return aDic.key < bDic.key
                        }
                        for (key, val) in sortedDic  {
                            if self.hasSpecialCharacters(str: key) {
                                let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
                                dictTmp["KEY"]      = dictDynaic[results.last!]
                                if let value = val as? AnyObject {
                                    dictTmp["VALUE"] = String(describing: value)
                                }
                                dictTmp["FOR"]      = results.last!.hasPrefix("s") == true ? "A" : "D"
                                arrSymbol.append(dictTmp as AnyObject)
                            }
                        }
                    }
                    finalDictData["frmComm"] = isFrmComm as AnyObject
                    finalDictData["array"]  = arrSymbol as AnyObject
                    finalDictData["lat"]    = dictData["latitude"]
                    finalDictData["lng"]    = dictData["longitude"]
                    finalDictData["address"] = dictData["deviceAddress"]
                    finalDictData["slc"]    = "SLC# "+String(describing:(dictData["slcNo"] as AnyObject)) as AnyObject
                    if let gID = dictData["gatewayId"] {
                        finalDictData["gateway"] = String(gID as! Int) as AnyObject
                    }
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
                    }
                }
            }else if tableView == self.tblView1 {
                btnBGClosed_Click(sender: btnOkSlider)
                if indexPath.row == 0 {
                    self.checkSelectedSLCs(strType: "ON", isFrmDim: false)
                } else if indexPath.row == 1 {
                    self.checkSelectedSLCs(strType: "OFF", isFrmDim: false)
                } else if indexPath.row == 2 {
                    self.checkSelectedSLCs(strType: "OFF", isFrmDim: true)
                } else if indexPath.row == 3 {
                    self.setReadData()
                } else if indexPath.row == 4 {
                    self.setResetData()
                } else if indexPath.row == 5 {
                    self.routePlanner()
                } else if indexPath.row == 6 {
                    self.getMode()
                } else if indexPath.row == 7 {
                    self.getDataforGetMode()
                }
            }
            //}
        } else {
            showUpgardeAlert()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tblView {
            let cell : CellStatusHeader = tableView.dequeueReusableCell(withIdentifier: "CellStatusHeader") as! CellStatusHeader
            cell.lblSLCNm?.text = "SLC NAME".localized()
            cell.lblLastDt?.text = "LAST UPDATED".localized()
            cell.btnCheckAll.addTarget(self, action: #selector(btnCheckAll_Click), for: .touchUpInside)
            if isFrmSelectedAll {
                cell.btnCheckAll.isSelected = true
            } else {
                cell.btnCheckAll.isSelected = false
            }
            return cell.contentView
        }
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
    //MARK:- routePlanner
    func routePlanner () {
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_route"
            ])
            
            
            if arrSelectdTag.count > 10 {
                AlertView().showAlert(strMsg: "Please select up to 10 SLCs to use the Multi Stop Route Planner".localized(), btntext: "Close".localized()) { (str) in }
            } else {
                var arr : [[String : AnyObject]] = []
                var dict : [String : AnyObject] = [:]
                for locat in getLocationFrmArray() {
                    let nearLocation = appDelegate.clLocation.distance(from: locat)
                    dict["dist"] = nearLocation as AnyObject
                    dict["loca"] = locat as AnyObject
                    arr.append(dict)
                }
                
                let sortedArray=arr.sorted { (obj1 , obj2) -> Bool in
                    return (obj1["dist"] as! Double) < (obj2["dist"] as! Double)
                }
                
                var str : String = ""
                var newFinalArr : [CLLocation] = []
                for i in 0..<sortedArray.count {
                    let toLocation : CLLocation = sortedArray[i]["loca"] as! CLLocation
                    if i != 0 {
                        str += "+to:\(String(toLocation.coordinate.latitude)),\(String(toLocation.coordinate.longitude))"
                    } else {
                        str += "\(String(toLocation.coordinate.latitude)),\(String(toLocation.coordinate.longitude))"
                    }
                    newFinalArr.append(toLocation)
                }
                
                print(newFinalArr)
                
                //performSegue(withIdentifier: "mapboxIdenitifier", sender: newFinalArr)
                
                let strFnal="http://maps.google.com/?daddr="+str
                guard let url = URL(string: strFnal) else { return }
                UIApplication.shared.open(url)
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
}

extension StatusVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtSLC.resignFirstResponder()
        txtGroup.resignFirstResponder()
        //txtNearMe.resignFirstResponder()
        txtGateway.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTAG = textField.tag
        if textField == txtSLC {
            self.performSegue(withIdentifier: "SearchIdenifier", sender: "SLC#")
            //getSLCs()
        } else if textField == txtGroup {
            self.performSegue(withIdentifier: "SearchIdenifier", sender: "Group".localized())
            //getGroup()
        } else if textField == txtGateway {
            getWayList()
        } else if textField == txtNearMe {
            getNearme()
        }
        return true
    }
}

extension StatusVC : SearchVCDelegate {
    func changeSearchParams(_ placeHolder: String, tagTextField: Int) {
        
    }
    
    func changeSearchParamsStatus(_ placeHolder: String, tagTextField: Int, strGroupID : String) {
        if tagTextField == 1 {
            txtSLC.text = placeHolder
        } else if tagTextField == 2 {
            strGatwayID = strGroupID
            txtGateway.text = placeHolder
        } else if tagTextField == 3 {
            strGID = strGroupID
            txtGroup.text = placeHolder
        } else if tagTextField == 4 {
            //txtTrackID.text = placeHolder
        }
    }
}
