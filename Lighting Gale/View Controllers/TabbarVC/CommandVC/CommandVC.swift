//
//  CommandVC.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import Floaty

class HeaderCell : UITableViewCell {
    
}
class CellCommand: UITableViewCell {
    @IBOutlet var btnDot        : UIButton!
}

class CommandVC: LGParent {
    
    @IBOutlet var txtCmdNm          : UITextField!
    @IBOutlet var txtStatus         : UITextField!
    @IBOutlet var txtTimeFrom       : UITextField!
    @IBOutlet var txtTimeTo         : UITextField!
    @IBOutlet var txtGateWayNm      : UITextField!
    @IBOutlet var txtTrackID        : UITextField!
    
    @IBOutlet var tblView       : UITableView!
    
    @IBOutlet var vwTop         : UIView!
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    
    var isFilterOpen            : Bool! = false
    
    var arrTblData              : [String]!
    var arrSelectdTag           : [Int]!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrSelectdTag = []
        
        constVWHeight .constant = 0
        
        let floaty = Floaty()
        
        floaty.buttonColor  = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 83.0/255.0, alpha: 1.0)
        
        let item = FloatyItem()
        item.title = "Dim"
        item.titleLabel.backgroundColor = UIColor.init(red: 251.0/255.0, green: 131.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        item.titleLabel.clipsToBounds = true
        item.titleLabel.layer.cornerRadius = 3.0
        item.buttonColor = UIColor.init(red: 251.0/255.0, green: 131.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        item.icon   = UIImage(named: "Light_1")!
        floaty.addItem(item: item)
        
        let item1 = FloatyItem()
        item1.title = "Switch Off"
        
        item1.titleLabel.backgroundColor = UIColor.init(red: 133.0/255.0, green: 133.0/255.0, blue: 133.0/255.0, alpha: 1.0)
        item1.titleLabel.clipsToBounds = true
        item1.titleLabel.layer.cornerRadius = 3.0
        item1.buttonColor = UIColor.init(red: 133.0/255.0, green: 133.0/255.0, blue: 133.0/255.0, alpha: 1.0)
        item1.icon   = UIImage(named: "Light_2")!
        floaty.addItem(item: item1)
        
        let item3 = FloatyItem()
        item3.title = "Switch On"
        item3.titleLabel.backgroundColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        item3.titleLabel.clipsToBounds = true
        item3.titleLabel.layer.cornerRadius = 3.0
        item3.buttonColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 84.0/255.0, alpha: 1.0)
        item3.icon   = UIImage(named: "Light_1")!
        floaty.addItem(item: item3)
        
        let item4 = FloatyItem()
        item4.title = "Read Data"
        item4.titleLabel.backgroundColor = UIColor.init(red: 251.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        item4.titleLabel.clipsToBounds = true
        item4.titleLabel.layer.cornerRadius = 3.0
        item4.buttonColor = UIColor.init(red: 251/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        item4.icon   = UIImage(named: "Readdata")!
        floaty.addItem(item: item4)
        
        let item5 = FloatyItem()
        item5.title = "Reset"
        item5.titleLabel.backgroundColor = UIColor.init(red: 33/255.0, green: 151.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        item5.titleLabel.clipsToBounds = true
        item5.titleLabel.layer.cornerRadius = 3.0
        item5.buttonColor = UIColor.init(red: 33/255.0, green: 151.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        item5.icon   = UIImage(named: "Resetdata")!
        floaty.addItem(item: item5)
        
        self.view.addSubview(floaty)
        
        self.addRightBarButton(imgRightbarButton: "Filter")
        arrTblData = ["","","","","","","","","","","","","","",""]
    }
    
    @IBAction func btnDot_Click(sender : UIButton) {
        let cell : CellCommand = tblView.cellForRow(at: IndexPath.init(row:  sender.tag, section: 0)) as! CellCommand
        if arrSelectdTag.count > 0 {
            if arrSelectdTag.contains(sender.tag) {
                let index = arrSelectdTag.firstIndex(where: {$0 == sender.tag})
                arrSelectdTag.remove(at: index!)
                cell.btnDot.tintColor = UIColor.init(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1.0)
            } else {
                arrSelectdTag.append(sender.tag)
                cell.btnDot.tintColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 83.0/255.0, alpha: 1.0)
            }
        } else {
            arrSelectdTag.append(sender.tag)
            cell.btnDot.tintColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 83.0/255.0, alpha: 1.0)
        }
        print(arrSelectdTag)
    }
    
    override func rightButtonClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 192
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension CommandVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellCommand = tableView.dequeueReusableCell(withIdentifier: "CellCommand") as! CellCommand
        cell.btnDot.addTarget(self, action: #selector(btnDot_Click), for: UIControl.Event.touchUpInside)
        cell.btnDot.tag = indexPath.row
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        if arrSelectdTag.contains(indexPath.row) {
            cell.btnDot.tintColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 83.0/255.0, alpha: 1.0)
        } else {
            cell.btnDot.tintColor = UIColor.init(red: 196.0/255.0, green: 196.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*DispatchQueue.main.async {
         self.performSegue(withIdentifier: "DetailIdentifier", sender: self)
         }*/
    }
    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header : HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")! as! HeaderCell
        return header.contentView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension CommandVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtCmdNm.resignFirstResponder()
        txtStatus.resignFirstResponder()
        txtTrackID.resignFirstResponder()
        txtGateWayNm.resignFirstResponder()
        /*if textField == txtTimeTo || textField == txtTimeFrom {
         if textField == txtTimeFrom{
         isFrmDtPicker = true
         } else {
         isFrmDtPicker = false
         }
         showDatePicker()
         } else {
         self.performSegue(withIdentifier: "SearchIdenifier", sender: textField.placeholder)
         }*/
    }
}
