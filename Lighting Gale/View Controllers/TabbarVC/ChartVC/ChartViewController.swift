//
//  ChartViewController.swift
//  Lighting Gale
//
//  Created by Apple on 25/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Charts
import FirebaseAnalytics

class ChartCell : UITableViewCell {
    @IBOutlet var imgView   : UIImageView!
    @IBOutlet var lblNm     : UILabel!
    @IBOutlet var lblValue  : UILabel!
}

class ChartViewController: LGParent {
    
    @IBOutlet var pieChartView  : PieChartView!
    
    @IBOutlet var lblTitle      : UILabel!
    
    var isFrmOption             : String!
    
    var arrHeading              : [String] = []
    var arrValue                : [Double] = []
    var arrImg                  : [UIImage] = []
    var arrColor                : [UIColor] = []
    
    @IBOutlet var tblView       : UITableView!
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.viewDidLoad()
        
        if isFrmOption == "SLCFAULT" {
            lblTitle.text = "SLCs Service Request".localized()
            arrImg = [
                UIImage.init(named: "Lamp-fault"),
                UIImage.init(named: "status_2"),
                UIImage.init(named: "Voltage_over"),
                UIImage.init(named: "Day_Burner"),
                UIImage.init(named: "astro_clock")
                ] as! [UIImage]
            
            arrHeading = ["Lamp Fault".localized(),
                          "Driver Fault".localized(),
                          "Voltage Under Over".localized(),
                          "Day Burner".localized(),
                          "Lamp Outages".localized()]
        } else if isFrmOption == "SLCLIGHT" {
            lblTitle.text = "Lamp Status".localized()
            arrImg = [UIImage.init(named: "dashboard_On"),
                      UIImage.init(named: "status_off"),
                      UIImage.init(named: "dashboard_Dim") ] as! [UIImage]
            arrHeading = ["ON".localized(),
                          "OFF".localized(),
                          "DIM".localized()]
        } else if isFrmOption == "SLCOMMUNICATION" {
            lblTitle.text = "SLC COMM. STATUS".localized()
            arrImg = [UIImage.init(named: "slc_yes"),
                      UIImage.init(named: "cfail"),
                      UIImage.init(named: "P") ] as! [UIImage]
            arrHeading = ["YES".localized(),
                          "NO".localized(),
                          "NEVER".localized()]
        } else {
            lblTitle.text = "SLC Mode".localized()
            arrImg = [UIImage.init(named: "01"),
                      UIImage.init(named: "02"),
                      UIImage.init(named: "03"),
                      UIImage.init(named: "04"),
                      UIImage.init(named: "05"),
                      UIImage.init(named: "06"),
                ] as! [UIImage]
            arrHeading = ["Photocell".localized(),
                          "Schedule".localized(),
                          "Astro Clock with Override".localized(),
                          "Astro Clock".localized(),
                          "Mixed Mode".localized(),
                          "Manual".localized()]
        }
        
        setChart(dataPoints: arrValue, values: arrHeading)
        setup(pieChartView: pieChartView)
        
        tblView.layer.cornerRadius = 13.0
        
        tblView.tableFooterView = UIView()
        
        tblView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFrmOption == "SLCFAULT" {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_graphSLCService"
            ])
        } else if isFrmOption == "SLCSTATUS" {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_graphSLCStatus"
            ])
        } else if isFrmOption == "SLCOMMUNICATION" {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_graphSLCCommunication"
            ])
        } else {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_graphSLCMode"
            ])
        }
    }
    
    func setChart(dataPoints: [Double], values : [String]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            if dataPoints[i] != 0.0 {
                let data = PieChartDataEntry(value: dataPoints[i], label:arrHeading[i])
                dataEntries.append(data)
            }
        }
        
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        
        let noZeroFormatter = NumberFormatter()
        noZeroFormatter.zeroSymbol = ""
        
        pieChartDataSet.yValuePosition = .outsideSlice
        pieChartDataSet.valueLinePart1Length = 1.0
        pieChartDataSet.valueLinePart2Length = 1.2
        pieChartDataSet.valueLineColor = UIColor.white
        pieChartDataSet.entryLabelColor = UIColor.clear
        pieChartDataSet.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        
        if isFrmOption == "SLCFAULT" {
            arrColor = [UIColor.init(red: 170.0/255.0, green: 87.0/255.0, blue: 162.0/255.0, alpha: 1.0),
                        UIColor.init(red: 246/255.0, green: 74/255.0, blue: 46/255.0, alpha: 1.0),
                        UIColor.init(red: 69.0/255.0, green: 131.0/255.0, blue: 221.0/255.0, alpha: 1.0),
                        UIColor.init(red: 247.0/255.0, green: 129.0/255.0, blue: 52.0/255.0, alpha: 1.0),
                        UIColor.init(red: 243.0/255.0, green: 120.0/255.0, blue: 90.0/255.0, alpha: 1.0)]
            var newColor : [UIColor] = []
            for i in 0..<arrValue.count {
                if arrValue[i] != 0.0 {
                    newColor.append(arrColor[i])
                }
            }
            pieChartDataSet.colors = newColor
            
        } else if isFrmOption == "SLCLIGHT" {
            
            arrColor = [UIColor.init(red: 85.0/255.0, green: 177.0/255.0, blue: 75.0/255.0, alpha: 1.0),
                        UIColor.init(red: 199.0/255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1.0),
                        UIColor.init(red: 232.0/255.0, green: 128.0/255.0, blue: 33.0/255.0, alpha: 1.0)]
            
            var newColor : [UIColor] = []
            for i in 0..<arrValue.count {
                if arrValue[i] != 0.0 {
                    newColor.append(arrColor[i])
                }
            }
            pieChartDataSet.colors = newColor
            
        } else if isFrmOption == "SLCOMMUNICATION" {
            
            arrColor = [UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 45.0/255.0, alpha: 1.0),
                        UIColor.lightGray,
                        UIColor.darkGray]
            
            var newColor : [UIColor] = []
            for i in 0..<arrValue.count {
                if arrValue[i] != 0.0 {
                    newColor.append(arrColor[i])
                }
            }
            pieChartDataSet.colors = newColor
            
        } else {
           
            arrColor = [UIColor.init(red: 0.0/255.0, green: 76.0/255.0, blue: 109.0/255.0, alpha: 1.0),
                        UIColor.init(red: 24.0/255.0, green: 170.0/255.0, blue: 45.0/255.0, alpha: 1.0),
                        UIColor.init(red: 252.0/255.0, green: 127.0/255.0, blue: 50.0/255.0, alpha: 1.0),
                        UIColor.init(red: 255.0/255.0, green: 196.0/255.0, blue: 56.0/255.0, alpha: 1.0),
                        UIColor.init(red: 172.0/255.0, green: 87.0/255.0, blue: 159.0/255.0, alpha: 1.0),
                        UIColor.lightGray]
            
            var newColor : [UIColor] = []
            for i in 0..<arrValue.count {
                if arrValue[i] != 0.0 {
                    newColor.append(arrColor[i])
                }
            }
            pieChartDataSet.colors = newColor
        }
        
        pieChartDataSet.sliceSpace = 3.0
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 2
        formatter.multiplier = 1.0
        formatter.percentSymbol = " %"
        formatter.zeroSymbol = ""
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        pieChartView.data = pieChartData
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.drawSlicesUnderHoleEnabled = false
        pieChartView.legend.enabled     = false
        pieChartView.legend.textColor   = UIColor.clear
        pieChartView.animate(xAxisDuration: 1.5, easingOption: ChartEasingOption.easeOutBack)
        
    }
    
    @IBAction func btnBack_Click(sender : UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func setup(pieChartView chartView: PieChartView) {
        chartView.usePercentValuesEnabled       = true
        chartView.drawSlicesUnderHoleEnabled    = false
        chartView.holeRadiusPercent             = 0.0
        chartView.transparentCircleRadiusPercent = 0.0
        chartView.chartDescription?.enabled     = false
        chartView.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        chartView.delegate = self
        chartView.drawCenterTextEnabled         = true
        chartView.clipsToBounds = false
        
        chartView.drawHoleEnabled       = true
        chartView.rotationAngle         = 0
        chartView.rotationEnabled       = true
        chartView.highlightPerTapEnabled = true
        
        let l = chartView.legend
        l.horizontalAlignment = .right
        l.verticalAlignment = .top
        l.orientation = .vertical
        l.drawInside = false
        l.xEntrySpace = 7
        l.yEntrySpace = 0
        l.yOffset = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "statusDashboard" {
            let statusDashVC : StatusDashBoardVC = segue.destination as! StatusDashBoardVC
            var dictData : [String : AnyObject] = [:]
            var strLastMode : String = ""
            if (sender as! String) == "0" {
                dictData["POWER"] = PHOTOCELL           as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                strLastMode = "1"
            } else if (sender as! String) == "1" {
                dictData["POWER"] = SCHEDULE            as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                strLastMode = "2"
            } else if (sender as! String) == "2" {
                dictData["POWER"] = ASTRO_CLOCK_OVERRIDE as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                strLastMode = "4"
            } else if (sender as! String) == "3" {
                dictData["POWER"] = ASTRO_CLOCK as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                strLastMode = "3"
            } else if (sender as! String) == "4" {
                dictData["POWER"] = MIXED_MODE          as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                strLastMode = "7"
            } else if (sender as! String) == "5" {
                dictData["POWER"] = MODE_MANUAL         as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                strLastMode = "0"
            } else if (sender as! String) == "6" {
                dictData["POWER"] = LAMP_ON_POWER       as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = LAMP_ON_STATUS     as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
            } else if (sender as! String) == "7" {
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = LAMP_OFF           as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
            } else if (sender as! String) == "8" {
                dictData["POWER"] = LAMP_DIM_POWER      as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = LAMP_DIM_STATUS    as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
            } else if (sender as! String) == "12" {
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
            } else if (sender as! String) == "13" {
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&0"              as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
            } else if (sender as! String) == "14" {
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 1                   as AnyObject
                dictData["STATUS"] = ""                 as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
            }
            statusDashVC.strMode = (sender as! String)
            statusDashVC.dictDashboardData = dictData
            statusDashVC.strLastMode = strLastMode
        } else if segue.identifier == "FaultyList" {
            let faultySLC : FaultySLC = segue.destination as! FaultySLC
            /*if sender as! String == "0" {
             //COMM FALUT
             faultySLC.strMode = "2"
             } else*/ if sender as! String == "0" {
                //LAMP FAULT
                faultySLC.strMode = "0"
             } else if sender as! String == "1" {
                //DRIVER
                faultySLC.strMode = "1"
             } else if sender as! String == "2" {
                //VOLTAGE
                faultySLC.strMode = "3"
            }
        } else if segue.identifier == "dayBurnerIdenitifier" {
            let dayBurnerVC : DayBurnerDashBoardVC = segue.destination as! DayBurnerDashBoardVC
            dayBurnerVC.strMode = (sender as! String)
        }
    }
}


extension ChartViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFrmOption == "SLCFAULT" {
            return 5
        } else if isFrmOption == "SLCLIGHT" {
            return 3
        } else if isFrmOption == "SLCOMMUNICATION" {
            return 3
        } else {
            return 6
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ChartCell = tableView.dequeueReusableCell(withIdentifier: "ChartCell") as! ChartCell
        cell.lblNm.text         = arrHeading[indexPath.row]
        cell.lblValue.text      = String(describing: Int(arrValue[indexPath.row]))
        cell.imgView.image      = arrImg[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : ChartCell = tableView.cellForRow(at: indexPath) as! ChartCell
        if cell.lblValue.text != "0" {
            if isFrmOption == "SLCFAULT" {
                if indexPath.row == 3 || indexPath.row == 4 {
                    var strFrm : String = ""
                    if indexPath.row == 3 {
                        strFrm = "0"
                    } else{
                        strFrm = "1"
                    }
                    self.performSegue(withIdentifier: "dayBurnerIdenitifier", sender: strFrm)
                } else {
                    self.performSegue(withIdentifier: "FaultyList", sender: String(describing: indexPath.row))
                }
            } else if isFrmOption == "SLCLIGHT" {
                self.performSegue(withIdentifier: "statusDashboard", sender: String(describing: indexPath.row+6))
            } else if isFrmOption == "SLCOMMUNICATION" {
                self.performSegue(withIdentifier: "statusDashboard", sender: String(describing: indexPath.row+12))
            } else {
                self.performSegue(withIdentifier: "statusDashboard", sender: String(describing: indexPath.row))
            }
        }
    }
}


extension ChartViewController : ChartViewDelegate {
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
                
        var index : String = "0"
        let strSelecvtValue = (entry as! PieChartDataEntry).label
        
        if isFrmOption == "SLCLIGHT" {
            if strSelecvtValue == "ON".localized() {
                index = "6"
            } else if strSelecvtValue == "OFF".localized() {
                index = "7"
            } else if strSelecvtValue == "DIM".localized() {
                index = "8"
            }
            self.performSegue(withIdentifier: "statusDashboard", sender: index)
        } else if isFrmOption == "SLCOMMUNICATION" {
            if strSelecvtValue == "YES".localized() {
                index = "12"
            } else if strSelecvtValue == "NO".localized() {
                index = "13"
            } else if strSelecvtValue == "NEVER".localized() {
                index = "14"
            }
            self.performSegue(withIdentifier: "statusDashboard", sender: index)
        }  else if isFrmOption == "SLCFAULT" {
            if strSelecvtValue == "Lamp Fault".localized() {
                 self.performSegue(withIdentifier: "FaultyList", sender: "0")
            } else if strSelecvtValue == "Driver Fault".localized() {
                 self.performSegue(withIdentifier: "FaultyList", sender: "1")
            } else if strSelecvtValue == "Voltage Under Over".localized() {
                 self.performSegue(withIdentifier: "FaultyList", sender: "2")
            } else if strSelecvtValue == "Day Burner".localized() {
                self.performSegue(withIdentifier: "dayBurnerIdenitifier", sender: "0")
            } else if strSelecvtValue == "Lamp Outages".localized() {
                self.performSegue(withIdentifier: "dayBurnerIdenitifier", sender: "1")
            }
        } else {
            if strSelecvtValue == "Astro Clock with Override".localized() {
                index = "2"
            } else if strSelecvtValue == "Manual".localized() {
                index = "5"
            } else if strSelecvtValue == "Mixed Mode".localized() {
                index = "4"
            } else if strSelecvtValue == "Astro Clock".localized() {
                index = "3"
            } else if strSelecvtValue == "Schedule".localized() {
                index = "1"
            } else if strSelecvtValue == "Photocell".localized() {
                index = "0"
            }
            self.performSegue(withIdentifier: "statusDashboard", sender: index)
        }
    }
}
