//
//  MapboxViewController.swift
//  Lighting Gale
//
//  Created by Akash on 25/12/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

/*import Mapbox
import MapboxNavigation
import MapboxDirections
import MapboxCoreNavigation*/


class MapboxViewController: LGParent {//, MGLMapViewDelegate {
    
    /*var mapVw                   : NavigationMapView!
    var directionsRoute         : Route?
    
    @IBOutlet var btnNavigation : UIButton!
    
    var arrSorted               : [CLLocation] = []
    */
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*self.navigationItem.title = "Route"
        
        self.addLeftBarButton(imgLeftbarButton: "back")
        
        let topBar      = UIApplication.shared.statusBarFrame.height +
            (self.navigationController?.navigationBar.frame.height)!
        
        let bottomBar   = self.tabBarController!.tabBar.frame.height
        
        mapVw = NavigationMapView(frame: CGRect.init(x: 0,
                                                     y: topBar,
                                                     width: self.view.bounds.width,
                                                     height: self.view.bounds.height-topBar-bottomBar))
        
        view.addSubview(mapVw)
        
        mapVw.delegate          = self
        mapVw.showsUserLocation = true
        mapVw.setUserTrackingMode(.follow, animated: true, completionHandler: nil)
        
        calculateRoute()
        
        view.bringSubviewToFront(btnNavigation)*/
    }
    
    /*//MARK:- button click
    @IBAction func btnNavigation_Click(sender : UIButton) {
        let navigationViewController = NavigationViewController(for: directionsRoute!)
        navigationViewController.modalPresentationStyle = .fullScreen
        self.present(navigationViewController, animated: true, completion: nil)
    }
    
    //MARK:- calculateRoute
    func calculateRoute() {
        var waypoints : [Waypoint] = []
        var arrCoord : [CLLocationCoordinate2D] = []
        waypoints.append(Waypoint(coordinate: CLLocationCoordinate2D(latitude: appDelegate.clLocation.coordinate.latitude, longitude: appDelegate.clLocation.coordinate.longitude), name: "T1"))
        arrCoord.append(CLLocationCoordinate2D(latitude: appDelegate.clLocation.coordinate.latitude, longitude: appDelegate.clLocation.coordinate.longitude))
        for cl in arrSorted {
            waypoints.append(Waypoint(coordinate: CLLocationCoordinate2D(latitude: cl.coordinate.latitude, longitude: cl.coordinate.longitude), name: "T1"))
            arrCoord.append(CLLocationCoordinate2D(latitude:  cl.coordinate.latitude, longitude: cl.coordinate.longitude))
        }
        
        /*let waypoints = [
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0262, longitude: 72.5623), name: "T1"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0252, longitude: 72.5723), name: "T2"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0272, longitude: 72.5023), name: "T3"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0372, longitude: 72.6023), name: "T4"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0252, longitude: 72.3023), name: "T5"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0202, longitude: 72.4023), name: "T6"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0227, longitude: 72.5023), name: "T7"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0207, longitude: 72.4427), name: "T8"),
         Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0222, longitude: 72.4563), name: "T9"),
         ]*/
        
        /*let arrCoord = [CLLocationCoordinate2D(latitude: 23.0262, longitude: 72.5623),
         CLLocationCoordinate2D(latitude: 23.0252, longitude: 72.5723),
         CLLocationCoordinate2D(latitude: 23.0272, longitude: 72.5023),
         CLLocationCoordinate2D(latitude: 23.0372, longitude: 72.6023),
         CLLocationCoordinate2D(latitude: 23.0252, longitude: 72.3023),
         CLLocationCoordinate2D(latitude: 23.0202, longitude: 72.4023),
         CLLocationCoordinate2D(latitude: 23.0227, longitude: 72.5023),
         CLLocationCoordinate2D(latitude: 23.0207, longitude: 72.4427),
         CLLocationCoordinate2D(latitude: 23.0222, longitude: 72.4563)
         ]*/
        
        for c in 0..<arrCoord.count {
            if c != 0 {
                let annotation = MGLPointAnnotation()
                annotation.coordinate = arrCoord[c]
                annotation.title = "Start navigation"
                mapVw.addAnnotation(annotation)
            }
        }
        
        let options = NavigationRouteOptions(waypoints: waypoints, profileIdentifier: .automobile)
        
        _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
            if error == nil {
                self.btnNavigation.isHidden = false
                self.directionsRoute = routes?.first
                drawRoute(route: self.directionsRoute!)
            } else {
                AlertView().showAlert(strMsg: error?.localizedDescription, btntext: "OK") { (str) in
                    self.btnNavigation.isHidden = true
                }
            }
        }
        
        //MARK:- drawRoute
        func drawRoute(route: Route) {
            guard route.coordinateCount > 0 else { return }
            // Convert the route’s coordinates into a polyline
            var routeCoordinates = route.coordinates!
            let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
            
            // If there's already a route line on the map, reset its shape to the new route
            if let source = mapVw.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
                source.shape = polyline
            } else {
                let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
                
                // Customize the route line color and width
                let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
                lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1))
                lineStyle.lineWidth = NSExpression(forConstantValue: 3)
                
                // Add the source and style layer of the route line to the map
                mapVw.style?.addSource(source)
                mapVw.style?.addLayer(lineStyle)
            }
        }
    }*/
}
