//
//  StatusVC.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics

class MyCell : UICollectionViewCell {
    @IBOutlet var btnTime       : UIButton!
}

class DayBurnerDashBoardVC: LGParent {
    
    @IBOutlet var vwFilter      : UIView!
    
    @IBOutlet var vwDetails     : UIView!
    
    @IBOutlet var tblView       : UITableView!
    @IBOutlet var tblView1      : UITableView!
    
    @IBOutlet var btnFirst      : UIButton!
    
    @IBOutlet var vwTop         : UIView!
    @IBOutlet var vwSelectedTab : UIView!
    
    var isFrmDtPicker           : Bool!
    
    let datePicker              = UIDatePicker()
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    
    var isFilterOpen            : Bool! = false
    
    var arrTblData              : [AnyObject]!
    var arrSelectdTag           : [Int]!
    var arrSearchData           : [String]!
    
    var totalCount              : Int! = 1
    var pageCount               : Int! = 1
    var selectedTAG             : Int! = 1
    
    var strMode                 : String!
    
    var dictDashboardData       : [String : AnyObject]!
    
    @IBOutlet var lblMsg        : UILabel!
    
    @IBOutlet var txtSLC        : UITextField!
    @IBOutlet var txtFrmDate    : UITextField?
    @IBOutlet var txtToDate     : UITextField?
    
    var strSLCID                : String! = ""
    
    @IBOutlet var collectionView: UICollectionView!
    
    var isFrmSelectedAll        : Bool! = false
    
    var arrCollection : [String] = ["12 AM","1","2","3","4","5","6","7","8","9","10","11","12 PM","1","2","3","4","5","6","7","8","9","10","11"]
    
    var refreshControl          : UIRefreshControl!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 15.0, *) {
            self.tblView.sectionHeaderTopPadding = 0
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        let bgImage = UIImageView();
        bgImage.image = UIImage(named: "Green-Gradient");
        bgImage.contentMode = .scaleToFill
        
        collectionView.backgroundView = bgImage
        
        //collectionView.backgroundColor = UIColor(patternImage: bgImage.image)
        
        lblMsg.isHidden = true
        
        arrSelectdTag = []
        
        constVWHeight.constant = 0
        
        arrTblData = []
        
        tblView.tableFooterView = UIView()
        
        addLeftBarButton(imgLeftbarButton: "back")
        addRightBarButton(imgRightbarButton:"Search")
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "Search"), style: UIBarButtonItem.Style.done, target:self, action:#selector(searchClick))
        let rightButton1 = UIBarButtonItem(image: UIImage.init(named: "RoutForNAv"), style: UIBarButtonItem.Style.plain, target:self, action:#selector(mapIconClick))
        self.navigationItem.rightBarButtonItems = [rightButton, rightButton1]
        
        let lftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "back"), style: UIBarButtonItem.Style.done, target:self, action:#selector(leftButtonClick))
        let lftButton1: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "SLC_header"), style: UIBarButtonItem.Style.done, target:self, action:#selector(lftLampType))
        self.navigationItem.leftBarButtonItems = [lftButton, lftButton1]
        
        
        collectionView.reloadData()
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = serviceHandler.objUserModel.strDtFrmat
        //txtToDate.text = formatter.string(from: Date.yesterday)
        //txtFrmDate.text = formatter.string(from: Date.twoDays)
        
        //getDataServer(isLoadMore: true)
        
    }
    
    @IBAction func searchClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 110
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func mapIconClick() {
        routePlanner()
    }
    
    
    override func leftButtonClick() {
        self.navigationController?.isNavigationBarHidden = true
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func lftLampType() {
        view.endEditing(true)
        
        let actionSheet = UIAlertController(title: "Select Lamp Type".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        let arrLmpList : [AnyObject] = UserDefault.value(forKey: "LAMPLIST") as! [AnyObject]
        
        for dictLampData in arrLmpList {
            let strTitle : String = dictLampData["lampType"] as! String
            if dictLampData["isDefault"] as! String == "1" {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                    self.dismiss(animated: true) {
                    }
                }))
            } else {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                    var arrNew : [AnyObject] = []
                    for i in 0..<arrLmpList.count {
                        var di = arrLmpList[i] as! [String : String]
                        if di["lampType"]! == strTitle {
                            di["isDefault"] = "1"
                        } else {
                            di["isDefault"] = "0"
                        }
                        arrNew.append(di as AnyObject)
                    }
                    UserDefault.set(arrNew, forKey: "LAMPLIST")
                    UserDefault.set(dictLampData["lampTypeID"]!, forKey: "DEFAULTLAMP")
                    UserDefault.set(dictLampData["lampType"]!, forKey: "LAMPNAME")
//                    self.dismiss(animated: true) {
                        self.getParamsFrmServer()
//                    }
                }))
            }
        }
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        
        present(actionSheet, animated: true)
        
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refresh(sender: self)
        self.navigationController?.isNavigationBarHidden = false
        selectMode()
        
        txtSLC.placeholder = "SLC Name".localized()
        
        let selectLocalize = "Select".localized()
        
        if txtSLC.text == "Select" || txtSLC.text == "Selecionar" || txtSLC.text == "Seleccionar" {
            txtSLC.text       = selectLocalize
        }
        
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("slclist", screenClass: "slclist")
    }
    
    
    @objc func refresh(sender:AnyObject) {
        isFrmSelectedAll = false
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        arrSelectdTag.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    
    //MARK:- routePlanner
    func routePlanner () {
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_route"
            ])
            
            
            if arrSelectdTag.count > 10 {
                AlertView().showAlert(strMsg: "Please select up to 10 SLCs to use the Multi Stop Route Planner".localized(), btntext: "Close".localized()) { (str) in }
            } else {
                var arr : [[String : AnyObject]] = []
                var dict : [String : AnyObject] = [:]
                for locat in getLocationFrmArray() {
                    let nearLocation = appDelegate.clLocation.distance(from: locat)
                    dict["dist"] = nearLocation as AnyObject
                    dict["loca"] = locat as AnyObject
                    arr.append(dict)
                }
                
                let sortedArray=arr.sorted { (obj1 , obj2) -> Bool in
                    return (obj1["dist"] as! Double) < (obj2["dist"] as! Double)
                }
                
                var str : String = ""
                var newFinalArr : [CLLocation] = []
                for i in 0..<sortedArray.count {
                    let toLocation : CLLocation = sortedArray[i]["loca"] as! CLLocation
                    if i != 0 {
                        str += "+to:\(String(toLocation.coordinate.latitude)),\(String(toLocation.coordinate.longitude))"
                    } else {
                        str += "\(String(toLocation.coordinate.latitude)),\(String(toLocation.coordinate.longitude))"
                    }
                    newFinalArr.append(toLocation)
                }
                
                print(newFinalArr)
                
                //performSegue(withIdentifier: "mapboxIdenitifier", sender: newFinalArr)
                
                let strFnal="http://maps.google.com/?daddr="+str
                guard let url = URL(string: strFnal) else { return }
                UIApplication.shared.open(url)
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    func getLocationFrmArray() -> [CLLocation] {
        var arrLocation : [CLLocation] = []
        for i in 0..<arrTblData.count {
            if arrSelectdTag.contains(i) {
                if (arrTblData![i]["longitude"] as? String) != ""  {
                    let cll : CLLocation = CLLocation.init(latitude:Double(arrTblData![i]["latitude"]! as! String)!, longitude: Double(arrTblData![i]["longitude"]! as! String)!)
                    arrLocation.append(cll)
                }
            }
        }
        return arrLocation
    }
    
    //MARK:- DatePicker
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.maximumDate = NSDate.init() as Date
        datePicker.date = Date()
        
        /*let formatter = DateFormatter()
         formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         
         let myString = formatter.string(from: Date())
         let yourDate = formatter.date(from: myString)
         
         print(self.serviceHandler.objUserModel.strDtFrmat)
         
         let formatter1 = DateFormatter()
         formatter1.dateFormat = self.serviceHandler.objUserModel.strDtFrmat
         let myFString = formatter1.string(from: yourDate!)
         let yourFDate = formatter1.date(from: myFString)
         
         print(myFString)
         print(yourFDate)
         
         datePicker.date = yourFDate!*/
        
        /*let dateStringFormatter = DateFormatter()
         dateStringFormatter.dateFormat = self.serviceHandler.objUserModel.strDtFrmat!
         dateStringFormatter.timeZone = TimeZone(abbreviation: "UTC")
         
         let defaultDateStringFormatter = DateFormatter()
         defaultDateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
         defaultDateStringFormatter.timeZone = TimeZone(abbreviation: "UTC")
         
         let convertedStartDate : Date = defaultDateStringFormatter.date(from: (String(describing:Date.init())))!
         print(dateStringFormatter.date(from: (String(describing:convertedStartDate))))
         
         let newConverted    = dateStringFormatter.date(from: (String(describing:Date.init())))*/
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton      = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton     = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton    = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        //txtFrmDate.inputAccessoryView      = toolbar
        //txtFrmDate.inputView               = datePicker
        
        //txtToDate.inputAccessoryView        = toolbar
        //txtToDate.inputView                 = datePicker
    }
    
    @objc func donedatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = serviceHandler.objUserModel.strDtFrmat
        if isFrmDtPicker {
            //txtFrmDate.text = formatter.string(from: datePicker.date)
        } else {
            //txtToDate.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func selectMode() {
        switch strMode {
        case "0":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_dayBurner"
            ])
            self.title = "Day Burner".localized()
        //str = PHOTOCELL
        case "1":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_lampOutages"
            ])
            self.title = "Lamp Outages".localized()
            //str = MODE_MANUAL
            
        default:
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchIdenifier" {
            let searchVC : SearchViewController = segue.destination as! SearchViewController
            searchVC.searchDelegate = self
            searchVC.strPlasHolder  = (sender as! String)
            searchVC.arrTblData     = arrSearchData
            searchVC.currentTag     = selectedTAG
            searchVC.isFrmSLCName    = true
        } else  if segue.identifier == "detailScreen" {
            let detailVC : DetailViewController = (segue.destination as? DetailViewController)!
            let dict : [String : AnyObject] = sender as! [String : AnyObject]
            detailVC.isFrmComm  = false
            detailVC.arrTblData = (dict["array"] as! [AnyObject])
            detailVC.latitude   = (dict["lat"] as! String)
            detailVC.longitude  = (dict["lng"] as! String)
            detailVC.strSLCno   = (dict["slc"] as! String)
            detailVC.strAddress = ""
            if let strAdd = dict["address"] {
                detailVC.strAddress = (strAdd as! String)
            }
        }
    }
    
    
    //MARK:- GetDataFromServer
    func getParamsFrmServer() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getLampList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                if let arrFrmServer : [AnyObject] = (responseData[DATA] as? [AnyObject]) {
                    if arrFrmServer.count > 0 {
                        let arrDynamic      : [AnyObject] = arrFrmServer
                        
                        var dictDynamic : [String : String] = [:]
                        
                        var strKey          : String = ""
                        var strValue        : String = ""
                        
                        for dict in arrDynamic {
                            strKey      = dict["type"] as! String
                            strValue    = dict["key"] as! String
                            dictDynamic[strKey] = strValue
                        }
                        UserDefaults().set(dictDynamic, forKey: "DYNAMIC")
                    }
                    self.refresh(sender: self)
                }
            }
        }
    }
    
    func getDataServer(isLoadMore : Bool) {
        if arrTblData.count != totalCount {
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader["PageIndex"]     = String(pageCount)
            dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
            dictHeader[RESPORT_TYPE]    = strMode == "0" ? "DayBurn" : "Outage"
//            dictHeader["EndDate"]       = txtToDate.text    == "SELECT" ? ""  : txtToDate.text
//            dictHeader["StartDate"]     = txtFrmDate.text   == "SELECT" ? ""  : txtFrmDate.text
            let strSelectTx = "Select".localized()
            
            dictHeader["SLCName"]       = txtSLC.text       == strSelectTx ? ""  : strSLCID
            
            ProfileServices().getBurnerList(parameters: [:], headerParams: dictHeader, showLoader: isLoadMore) { (responseData, isSuccess) in
                if isSuccess {
                    let arrTmp : [AnyObject] = responseData[DATA]![LIST]   as! [AnyObject]
                    self.arrTblData.append(contentsOf: arrTmp)
                    self.totalCount = responseData[DATA]![TOTAL_RECORDS]   as! Int
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }/* else {
             self.lblMsg.isHidden = true
             }*/
            //self.tblView.reloadData()
        }
    }
    
    /*@IBAction func rightButtonPlusClick() {
     self.view.addSubview(vwFilter)
     vwFilter.frame = self.view.bounds
     vwFilter.alpha = 0.0
     UIView.animate(withDuration: 0.15) {
     self.vwFilter.alpha = 1.0
     }
     
     }*/
    
    @IBAction func btnBGClosed_Click (sender : UIButton) {
        vwFilter.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwFilter.alpha = 0.0
        }) { (isDone) in
            self.vwFilter.removeFromSuperview()
        }
    }
    
    override func rightButtonClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 110
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK:- Button clicks
    @IBAction func btnCheck_Click(sender : UIButton) {
        let cell : CellStatus = tblView.cellForRow(at: IndexPath.init(row:  sender.tag, section: 0)) as! CellStatus
        
        if arrSelectdTag.contains(sender.tag) {
            let index = arrSelectdTag.index(of : sender.tag)
            arrSelectdTag.remove(at: index!)
            cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
        } else {
            arrSelectdTag.append(sender.tag)
            cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
        }
    }
    @IBAction func btnCheckAll_Click(sender : UIButton) {
        arrSelectdTag.removeAll()
        if !sender.isSelected {
            sender.isSelected = true
            isFrmSelectedAll = true
            for i in 0..<arrTblData!.count {
                arrSelectdTag.append(i)
            }
        } else {
            isFrmSelectedAll = false
            sender.isSelected = false
        }
        tblView.reloadData()
    }
    @IBAction func clearBtn_Click(sender : UIButton) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        arrSelectdTag.removeAll()
        isFrmSelectedAll    = false
        txtSLC.text       = "Select".localized()
        let formatter = DateFormatter()
        formatter.dateFormat = serviceHandler.objUserModel.strDtFrmat
        //txtToDate.text = formatter.string(from: Date.yesterday)
        //txtFrmDate.text = formatter.string(from: Date.twoDays)
        
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    @IBAction func searchBtnClick (sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_searchStatusDashboard"
        ])
        isFrmSelectedAll    = false
        pageCount           = 1
        totalCount          = 1
        arrTblData.removeAll()
        arrSelectdTag.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    
    
    @IBAction func btnStatus_Click(sender : UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.vwSelectedTab.frame = CGRect.init(x: sender.frame.origin.x , y: self.vwSelectedTab.frame.origin.y, width: sender.frame.width, height: self.vwSelectedTab.frame.height)
            self.view.layoutIfNeeded()
        }
        
        arrTblData.removeAll()
        totalCount  = 1
        pageCount   = 1
        tblView.reloadData()
        getDataServer(isLoadMore: true)
    }
    
    func hasSpecialCharacters(str : String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "@", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: str, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, str.count)) {
                return true
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
        
        return false
    }
    
    
    //MARK:- getSLCs
    func getSLCs() {
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader[SLC_NO]          = ""
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader["NodeType"]      = ""
        
        ProfileServices().getSLCList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "1")
                print(self.arrSearchData)
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "SLC Name".localized())
            }
        }
    }
    
    func getvalueFrmDict(dict : [String : AnyObject], isFrm : String) -> [String] {
        var arrTmp : [String] = []
        if isFrm == "1" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["value"] as! String)
            }
        }
        return arrTmp
    }
    
    
    //MARK:- Get Details From SLC
    func getDetails(strSLC:String, nvrComm : String) {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader[PAGE_NO]         = String(1)
        dictHeader[PAGE_SIZE]       = "10"
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        
        var dictData : [String : AnyObject] = [:]
        dictData[G_ID]          = ""            as AnyObject
        dictData[SLC_ID]        = strSLC        as AnyObject
        dictData[SLC_GROUP]     = ""            as AnyObject
        dictData[POWER_PARAM]   = ""            as AnyObject
        dictData[STATUS_PARAM]  = ""            as AnyObject
        dictData["NeverCommunication"]  = nvrComm == "DNR" ? 1 as AnyObject : 0 as AnyObject
        dictData["LastReceivedMode"] = "" as AnyObject
        
        ProfileServices().getStatusList(parameters: dictData, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responseData[LIST]       as! [AnyObject]
                //self.arrTblData.append(contentsOf: arrTmp)
                print(arrTmp)
                
                if arrTmp.count > 0 {
                    let dictDynaic : [String : String] = UserDefaults().value(forKey: "DYNAMIC") as! [String : String]
                    
                    let dictData : [String : AnyObject] = arrTmp[0] as! [String : AnyObject]
                    var dictTmp : [String : String] = [:]
                    var arrSymbol : [AnyObject] = []
                    var finalDictData : [String : AnyObject] = [:]
                    let sortedDic = dictData.sorted { (aDic, bDic) -> Bool in
                        return aDic.key < bDic.key
                    }
                    
                    dictTmp["KEY"]      = "SLC Name".localized()
                    dictTmp["VALUE"]    = (dictData["name"] as! String)
                    dictTmp["FOR"]      = "D"
                    arrSymbol.append(dictTmp as AnyObject)
                    
                    for (key, val) in sortedDic  {
                        if self.hasSpecialCharacters(str: key) {
                            let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
                            dictTmp["KEY"]      = dictDynaic[results.last!]
                            if let value = val as? AnyObject {
                                dictTmp["VALUE"] = String(describing: value)
                            }
                            dictTmp["FOR"]      = results.last!.hasPrefix("s") == true ? "A" : "D"
                            arrSymbol.append(dictTmp as AnyObject)
                        }
                    }
                    finalDictData["array"]  = arrSymbol as AnyObject
                    finalDictData["lat"]    = dictData["latitude"]
                    finalDictData["lng"]    = dictData["longitude"]
                    finalDictData["address"] = dictData["deviceAddress"]
                    finalDictData["slc"]    = "SLC# "+String(describing:(dictData["slcNo"] as AnyObject)) as AnyObject
                    
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
                    }
                } else {
                    AlertView().showAlert(strMsg: "No data Found".localized(), btntext: "OK".localized()) { (str) in}
                }
            } else {
            }
        }
    }
    
}

//MARK:- TableView Delegate
extension DayBurnerDashBoardVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //if tableView == tblView {
        arrTblData.count
        /*} else {
         return 0
         }*/
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView {
            let cell : CellStatus = tableView.dequeueReusableCell(withIdentifier: "CellStatus") as! CellStatus
            
            let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
            
            let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            let darkGray  = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 111/255, green: 113/255, blue: 121/255, alpha: 1)]
            
            let strDate = (dictData[DATE_TIME] as! String) == "" ? "N/A" : (dictData[DATE_TIME] as! String)
            cell.lblDt.text = strDate
            /*if strDate == "N/A" {
                cell.lblDt.text = strDate
            } else {
                let arrStrDate = strDate.components(separatedBy: " ")
              
                let mutableAttrString1 = NSMutableAttributedString(string: arrStrDate[0]+" ", attributes: darkGray)
                let mutableAttrString2 = NSMutableAttributedString(string: arrStrDate[1], attributes: lightGray)
                
                let finalMutable = NSMutableAttributedString()
                finalMutable.append(mutableAttrString1)
                finalMutable.append(mutableAttrString2)
                
                cell.lblDt.attributedText = finalMutable
            }*/
            
            cell.lblSLCNm.text      = dictData["name"]              as? String == "" ? "N/A" :  dictData["name"]      as? String
            cell.lblAddress.text    = dictData["deviceAddress"]     as? String == "" ? "N/A" :  dictData["deviceAddress"]   as? String
            /*if let slcNo = dictData["slcName"] as? AnyObject {
             cell.lblSLCNo.text = String(describing : slcNo)
             }*/
            
            if indexPath.row % 2 != 0 {
                cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
            } else {
                cell.backgroundColor = UIColor.white
            }
            
            cell.btnCheck.addTarget(self, action: #selector(btnCheck_Click), for: .touchUpInside)
            cell.btnCheck.tag = indexPath.row
            
            if arrSelectdTag.contains(indexPath.row) {
                cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
            } else {
                cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
            }
            
            if indexPath.row == arrTblData.count-1 {
                getDataServer(isLoadMore: true)
            }
            
            return cell
            
        } else {
            let cell : CellFilter = tableView.dequeueReusableCell(withIdentifier: "CellFilter") as! CellFilter
            //cell.btnFilter.setTitle(arrFilter[indexPath.row], for: .normal)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        getDetails(strSLC:dictData["slcNo"] as! String , nvrComm: "")
        /*if self.title ==  "SLC Comm Status - No" {
         let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
         var finalDictData : [String : AnyObject] = [:]
         
         var dictTmp : [String : String] = [:]
         
         var arrSymbol : [AnyObject] = []
         
         dictTmp["KEY"]      = "Communication"//results.last
         dictTmp["VALUE"]    = "0"//String(describing: value)
         dictTmp["FOR"]      = "D"//key.hasPrefix("a") == true ? "A" : "D"
         arrSymbol.append(dictTmp as AnyObject)
         
         finalDictData["array"]  = arrSymbol as AnyObject
         finalDictData["lat"]    = dictData["latitude"]
         finalDictData["lng"]    = dictData["longitude"]
         finalDictData["slc"]    = "SLC "+String(describing: (dictData["slcNo"] as! Int)) as AnyObject
         DispatchQueue.main.async {
         self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
         }
         } else if self.title !=  "SLC Comm Status - Never" {
         let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
         var finalDictData : [String : AnyObject] = [:]
         let sortedDic = dictData.sorted { (aDic, bDic) -> Bool in
         return aDic.key < bDic.key
         }
         var arrSymbol : [AnyObject] = []
         for (key, val) in sortedDic  {
         if self.hasSpecialCharacters(str: key) {
         let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
         var dictTmp : [String : String] = [:]
         dictTmp["KEY"]      = results.last
         if let value = val as? AnyObject {
         dictTmp["VALUE"] = String(describing: value)
         }
         dictTmp["FOR"]      = key.hasPrefix("a") == true ? "A" : "D"
         arrSymbol.append(dictTmp as AnyObject)
         }
         }
         finalDictData["array"]  = arrSymbol as AnyObject
         finalDictData["lat"]    = dictData["latitude"]
         finalDictData["lng"]    = dictData["longitude"]
         finalDictData["slc"]    = "SLC "+String(describing: (dictData["slcNo"] as! Int)) as AnyObject
         DispatchQueue.main.async {
         self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
         }
         }*/
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tblView {
            let cell : CellStatusHeader = tableView.dequeueReusableCell(withIdentifier: "CellStatusHeader") as! CellStatusHeader
            let str : String = UserDefault.value(forKey: "LAMPNAME") as! String
            cell.lblHeader?.text = "LAMP TYPE".localized()+" : "+str
            cell.btnCheckAll.isHidden = false
            cell.btnCheckAll.addTarget(self, action: #selector(btnCheckAll_Click), for: .touchUpInside)
            if isFrmSelectedAll {
                cell.btnCheckAll.isSelected = true
            } else {
                cell.btnCheckAll.isSelected = false
            }
            return cell.contentView
        }
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 46
    }
}

//MARK:- TextFiled Delegate
extension DayBurnerDashBoardVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtSLC.resignFirstResponder()
        if textField == txtToDate || textField == txtFrmDate {
            if textField == txtFrmDate {
                isFrmDtPicker = true
            } else {
                isFrmDtPicker = false
            }
            showDatePicker()
        } else {
            //self.performSegue(withIdentifier: "SearchIdenifier", sender: textField.placeholder)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTAG = textField.tag
        if textField == txtSLC {
            getSLCs()
        }
        return true
    }
}

//MARK:- CollectionView Delegate
extension DayBurnerDashBoardVC : UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCollection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! MyCell
        cell.btnTime.setTitle(arrCollection[indexPath.row], for: .normal)
        return cell
    }
    
    func randomColor() -> UIColor{
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
}

//MARK:- SearchView Delegate
extension DayBurnerDashBoardVC : SearchVCDelegate {
    func changeSearchParams(_ placeHolder: String, tagTextField: Int) {
        
    }
    
    func changeSearchParamsStatus(_ placeHolder: String, tagTextField: Int, strGroupID : String) {
        if tagTextField == 1 {
            txtSLC.text = placeHolder
            strSLCID = strGroupID
        } else if tagTextField == 2 {
            //strGatwayID = strGroupID
            //txtGateway.text = placeHolder
        } else if tagTextField == 3 {
            //strGID = strGroupID
            //txtGroup.text = placeHolder
        } else if tagTextField == 4 {
            //txtTrackID.text = placeHolder
        }
    }
}
