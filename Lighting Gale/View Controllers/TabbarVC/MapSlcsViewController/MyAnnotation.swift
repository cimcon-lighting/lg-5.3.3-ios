//
//  MyAnnotation.swift
//  SLC Admin
//
//  Created by Apple on 22/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import MapKit
import CoreLocation

class MyAnnotation: NSObject, MKAnnotation {
    
    var coordinate  : CLLocationCoordinate2D
    var title       : String?
    var discipline  : String
    var strID       : Double
    var sID         : Double
    var status      : String
    var slcID       : String
    
    init (coordinate: CLLocationCoordinate2D, title: String!, subtitle: String!, strID: Double?, status : String, slcID : String) {
        self.title      = title!
        self.discipline   = subtitle!
        self.coordinate = coordinate
        self.strID      = strID!
        self.sID        = strID!
        self.status     = status
        self.slcID      = slcID
        super.init()
    }
    
    var subtitle: String? {
        return discipline
    }
}
