//
//  MyCustomePin.swift
//  SLC Admin
//
//  Created by Apple on 22/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import MapKit

class MyCustomPinAnnotationView : MKAnnotationView {
    
    var price: Float = 0.0
    
    init(annotation: MKAnnotation?, price: Float) {
        super.init(annotation: annotation, reuseIdentifier: nil)
        self.price = price
        image = UIImage(named: "cluster_blue")
        let label = UILabel(frame: CGRect(x: 9, y: 16, width: 38, height: 24))
        label.textAlignment = .center
        label.textColor = UIColor.white
        if self.price > 999 {
            label.text = "999+"
        } else {
            label.text = "\(UInt(self.price))"
        }
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        addSubview(label)
        canShowCallout = true
        rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
