//
//  AppDelegate.swift
//  SLC Admin
//
//  Created by Apple on 03/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import Fabric

import CoreLocation
import Crashlytics
import Localize_Swift
import FirebaseAnalytics
import Firebase

//akash

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var baseUrlDev      : String!
    
    var isFrmPaid       : String!
    var strMsgFrmAPI    : String!
    var strNearMe       : String!
    var strNearMsg      : String!
    var strClientNm     : String!
    
    var strSelectedMap  : Int = 1
    
    let locationManager = CLLocationManager()
    var clLocation      = CLLocation()
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseConfiguration.shared.setLoggerLevel(.max)
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey("AIzaSyA8NezlaD2DPEjhoTl8JPVIXA54e-4ByyI")
        //5.3.1
        if (UserdefaultManager().getPreferenceForkey(APPLANGUAGE) != nil) {
            Localize.setCurrentLanguage(UserdefaultManager().getPreferenceForkey(APPLANGUAGE) as! String)
        } else {
            Localize.resetCurrentLanguageToDefault()
            let language    = NSLocale.preferredLanguages[0]
            let languageDic = NSLocale.components(fromLocaleIdentifier: language)
            let strLanguage = languageDic["kCFLocaleLanguageCodeKey"]
            UserdefaultManager().setPreference(strLanguage as AnyObject, strKey: APPLANGUAGE)
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 50
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        launchWithViewcontroller()
        
        return true
    }
    
    func launchWithViewcontroller() {
        if UserdefaultManager().getUserModel() != nil {
            ServiceHandler.sharedInstance().objUserModel = (UserdefaultManager().getUserModel() as! UserModel)
        } 
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
       
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
      
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("user latitude = \(clLocation.coordinate.latitude)")
        print("user longitude = \(clLocation.coordinate.longitude)")
        clLocation = locations[0] as CLLocation
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}
