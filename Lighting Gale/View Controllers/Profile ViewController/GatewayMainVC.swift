//
//  GatewayMainVC.swift
//  Lighting Gale
//
//  Created by Apple on 01/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics

class GatewayMainVC: LGParent {
    
    @IBOutlet var lblConnected      : UILabel!
    @IBOutlet var lblDisConnected   : UILabel!
    @IBOutlet var lblPowerloss      : UILabel!
    
    @IBOutlet var lblHeadpowerloss : UILabel!
    @IBOutlet var lblHeadConn   : UILabel!
    @IBOutlet var lblHeadDisco  : UILabel!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftBarButton(imgLeftbarButton: "back")
        
    }
    
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title   = "Gateway Status".localized()
        lblHeadConn.text            = "CONNECTED".localized()
        lblHeadDisco.text           = "DISCONNECTED".localized()
        lblHeadpowerloss.text       = "POWER LOST".localized()
        getCountfrmService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("gatewayStatus", screenClass: "gatewayStatus")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gatewayInventary" {
            let gateway : GatewayInventoryVC = segue.destination as! GatewayInventoryVC
            gateway.strFrom = (sender as! String)
        }
    }
    
    //MARK:- Button click
    @IBAction func btnConected_Click(sender : UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            var str : String = ""
            if sender.tag == 101 {
                str = "connected"
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_gatewayConnected)"
                ])
                if lblConnected.text != "0" {
                    performSegue(withIdentifier: "gatewayInventary", sender: str)
                }
            } else if sender.tag == 103 {
                str = "powerloss"
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_powerloss)"
                ])
                if lblPowerloss.text != "0" {
                    performSegue(withIdentifier: "gatewayInventary", sender: str)
                }
            } else {
                str = "disconnected"
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_gatwayDisconnected)"
                ])
                if lblDisConnected.text != "0" {
                    performSegue(withIdentifier: "gatewayInventary", sender: str)
                }
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    
    //MARK:- getCountfrmService
    func getCountfrmService() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getGatewayCount(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let dictData : [String : AnyObject] = responseData[DATA] as! [String : AnyObject]
                self.lblConnected.text       = (dictData[CONENCTED] as? String) ?? "0"
                self.lblDisConnected.text    = (dictData[DISCONENCTED] as? String) ?? "0"
                self.lblPowerloss.text       = (dictData[POWERLOSS] as? String) ?? "0"
            }
        }
    }
    
}
