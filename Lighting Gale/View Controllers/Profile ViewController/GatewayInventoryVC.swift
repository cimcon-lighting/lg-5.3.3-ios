//
//  GatewayInventoryVC.swift
//  Lighting Gale
//
//  Created by Apple on 01/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class CellGateway : UITableViewCell {
    @IBOutlet var imgGEye           : UIImageView!
    @IBOutlet var imgEye            : UIImageView!
    @IBOutlet var imgStatus         : UIImageView!
    
    @IBOutlet var btnSLC            : UIButton!
    
    @IBOutlet var lblCommType       : UILabel!
    @IBOutlet var lblGatewayName    : UILabel!
    @IBOutlet var lblAddress        : UILabel!
    @IBOutlet var lblSLCCount       : UILabel!
    
    @IBOutlet var lblHeadAddress    : UILabel!
    @IBOutlet var lblHeadComp       : UILabel!
}

class GatewayInventoryVC: LGParent {
    
    var totalCount          : Int! = 1
    var pageCount           : Int! = 1
    var selectedTAG         : Int! = 1
    
    var strFrom             : String!
    
    @IBOutlet var txtID         : UITextField!
    @IBOutlet var txtGateway    : UITextField!
    
    @IBOutlet var lblMsg        : UILabel!
    
    var refreshControl          : UIRefreshControl!
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    
    var isFilterOpen        : Bool! = false
    
    var arrTblData          : [AnyObject] = []
    var arrGateway          : [AnyObject] = []
    var arrSearchData       : [String]!   = []
    
    @IBOutlet var btnSearch     : UIButton!
    @IBOutlet var btnClear      : UIButton!
    
    @IBOutlet var tblView   : UITableView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        constVWHeight .constant = 0
        addLeftBarButton(imgLeftbarButton: "back")
        addRightBarButton(imgRightbarButton: "Search")        
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        lblMsg.isHidden = true
        
        tblView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnSearch.setTitle("SEARCH".localized(), for: .normal)
        btnClear.setTitle("CLEAR".localized(), for: .normal)
        self.navigationItem.title = "Gateway Inventory".localized()
        
        let selectLocalize = "Select".localized()
        
        if txtGateway.text == "Select" || txtGateway.text == "Selecionar" || txtGateway.text == "Seleccionar" {
            txtGateway.text         = selectLocalize
        }
        
        if txtID.text == "Select" || txtID.text == "Selecionar" || txtID.text == "Seleccionar" {
            txtID.text         = selectLocalize
        }

        getDataFrmServer(isLoder: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Analytics.setScreenName("gatewayList", screenClass: "gatewayList")
    }
    
    @objc func refresh(sender:AnyObject) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        
        txtID.text        = "Select".localized()
        txtGateway.text   = "Select".localized()
        
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoder: true)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "SearchIdenifier" {
            let searchVC : SearchViewController = segue.destination as! SearchViewController
            searchVC.searchDelegate = self
            searchVC.strPlasHolder  = (sender as! String)
            searchVC.arrTblData     = arrSearchData
            searchVC.currentTag     = selectedTAG
            searchVC.arrMainData    = arrGateway
        } else if segue.identifier == "assignedIdentifier" {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
             AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_assignedSLC"
            ])
            let searchVC : AssignedSLCVC = segue.destination as! AssignedSLCVC
            searchVC.strGatewayID = (sender as! String)
        } else if segue.identifier == "detailsIdentifier"{
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_gatewayDetails"
                       ])
            let searchVC : GatewayDetailsVC = segue.destination as! GatewayDetailsVC
            searchVC.dictMainData =  sender as! [String : AnyObject]
        }
    }
    
    func getDataFrmServer(isLoder : Bool) {
        if arrTblData.count != totalCount {
            let strSelectTx = "Select".localized()
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            dictHeader["pageNo"]        = String(pageCount)
            dictHeader[PAGE_SIZE]       = "10"
            dictHeader["GatewayType"]   = strFrom
            dictHeader["GatewayName"]   = txtGateway.text   == strSelectTx ? "" as String : txtGateway.text!
            dictHeader["Gatewayid"]     = txtID.text        == strSelectTx ? "" as String : txtID.text!
            
            ProfileServices().getGatewayList(parameters: [:], headerParams: dictHeader, showLoader: isLoder) { (responsedata, isSuccess) in
                if isSuccess {
                    if self.arrTblData.count != self.totalCount {
                        let arrTmp : [AnyObject] = responsedata[DATA]![LIST]       as! [AnyObject]
                        self.arrTblData.append(contentsOf: arrTmp)
                        self.totalCount = responsedata[DATA]![TOTAL_RECORDS] as! Int
                    }
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }
        }
    }
    
    @IBAction func searchBtnClick (sender : UIButton) {

        pageCount           = 1
        totalCount          = 1
        arrTblData.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoder: true)
        })
    }
    @IBAction func clearBtn_Click(sender : UIButton) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        txtID.text       = "Select".localized()
        txtGateway.text  = "Select".localized()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoder: true)
        })
    }
    
    //MARK:- getWayList
    func getWayList(withTag : Int) {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader["GatewayType"]   = strFrom
        
        ProfileServices().getCommandGatwayList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                self.arrGateway = responseData[DATA] as! [AnyObject]
                var arrTmp : [String] = []
                if withTag == 1 {
                    for dict in responseData[DATA] as! [AnyObject] {
                        arrTmp.append(dict["id"] as! String)
                    }
                } else {
                    for dict in responseData[DATA] as! [AnyObject] {
                        arrTmp.append(dict["gatewayName"] as! String)
                    }
                }
                self.arrSearchData = arrTmp
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "")
            }
        }
    }
    
    @IBAction func btnSlc_Click(sender : UIButton) {
        let dictData : [String : AnyObject] = arrTblData[sender.tag] as! [String : AnyObject]
        performSegue(withIdentifier: "assignedIdentifier", sender: (dictData["rtuGroupId"]))
    }
    
    override func rightButtonClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 115
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

//MARK:- TableDeleagte
extension GatewayInventoryVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellGateway = tblView.dequeueReusableCell(withIdentifier: "CellGateway") as! CellGateway
        cell.imgEye.tintColor  = UIColor.darkGray
        cell.imgGEye.tintColor = UIColor.darkGray
        
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        cell.lblHeadComp.text       = "Comm. Type".localized()
        cell.lblHeadAddress.text    = "Address".localized()
        
        if strFrom == "disconnected" {
            cell.imgStatus.image = UIImage.init(named: "goff")
        } else if strFrom == "powerloss" {
            cell.imgStatus.image = UIImage.init(named: "gpowerloss")
        } else {
            cell.imgStatus.image = UIImage.init(named: "gon")
        }
        
        cell.btnSLC.tag = indexPath.row
        cell.btnSLC.addTarget(self, action: #selector(btnSlc_Click), for: .touchUpInside)
        
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        
        cell.lblAddress.text    = (dictData["deviceAddress"] as! String)
        cell.lblSLCCount.text   = "\(dictData["commCount"] as! String)/\(dictData["slcCount"] as! String)"
        cell.lblCommType.text   = ((dictData["details"] as! [String : AnyObject])["communication_Type"] as! String)
        cell.lblGatewayName.text = (dictData["gatewayName"] as! String)
        
        if indexPath.row == arrTblData.count-1 {
            getDataFrmServer(isLoder: true)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        performSegue(withIdentifier: "detailsIdentifier", sender: dictData)
    }
}


extension GatewayInventoryVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTAG = textField.tag
        if textField == txtID {
            getWayList(withTag: selectedTAG)
            return false
        } else if textField == txtGateway {
            getWayList(withTag: selectedTAG)
            return false
        }
        return true
    }
}


extension GatewayInventoryVC : SearchVCDelegate {
    func changeSearchParams(_ placeHolder: String, tagTextField: Int) {
        
    }
    
    func changeSearchParamsStatus(_ placeHolder: String, tagTextField: Int, strGroupID : String) {
        if tagTextField == 1 {
            txtID.text = placeHolder
        } else if tagTextField == 2 {
            txtGateway.text = placeHolder
        }
    }
}
