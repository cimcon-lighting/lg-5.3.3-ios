//
//  ProfileViewController.swift
//  SLC Admin
//
//  Created by Apple on 08/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Localize_Swift
import FirebaseAnalytics
import MessageUI

class cellMore: UITableViewCell {
    @IBOutlet var lblNm     : UILabel!
    @IBOutlet var imgView   : UIImageView!
}

class MoreViewController: LGParent, MFMailComposeViewControllerDelegate {
    
    @IBOutlet var tblView         : UITableView!
    
    var arrClientType             : [[String : AnyObject]] = []
    
    @IBOutlet var consHeight      : NSLayoutConstraint!
    
    var arrImg          : [String]!
    var arrName         : [String]!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            self.arrClientType = arr as! [[String : AnyObject]]
            
            let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count == 0 {
                consHeight.constant = 160
            }
        }
        /*if self.serviceHandler.objUserModel.strClienttype != "1" {
            consHeight.constant = 160
        }*/
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        tblView.layer.cornerRadius = 15.0
        //if self.serviceHandler.objUserModel.strClienttype == "1" {
        let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
        if filterdArray.count > 0 {
            print("Profile".localized())
            arrImg = ["User","Gateway_no_communication","Support","Logoff"]
            arrName = ["Profile".localized(),
                       "Gateway Status".localized(),
                       "Support".localized(),
                       "Logout".localized()]
        } else {
            arrImg = ["User","Support","Logoff"]
            arrName = ["Profile".localized(),
                       "Support".localized(),
                       "Logout".localized()]
        }
        
        tblView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("more", screenClass: "more")
    }
}

extension MoreViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : cellMore = tableView.dequeueReusableCell(withIdentifier: "moreCell") as! cellMore
        cell.lblNm.text     = arrName[indexPath.row]
        cell.imgView.image  = UIImage.init(named: arrImg[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
             AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_profile"
            ])
            performSegue(withIdentifier: "profileIdenitifier", sender: self)
        } /*else if indexPath.row == 1 {
           self.performSegue(withIdentifier: "sentCommand", sender: self)
        }*/ else if indexPath.row == 1 {
           // if self.serviceHandler.objUserModel.strClienttype == "1" {
            let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                 AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_gatewayList"
                ])
                performSegue(withIdentifier: "gatwayIdentifier", sender: self)
            } else {
               let subject          = "Contact Support - LG"
                let body            = ""
                let encodedParams   = "subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
                
                if let url = URL(string: "mailto:support@quantela.com?\(encodedParams!)") {
                  if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                  } else {
                    UIApplication.shared.openURL(url)
                  }
                }
            }
        }  else if indexPath.row == 2 {
//            if self.serviceHandler.objUserModel.strClienttype == "1" {
            let filterdArray = self.arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                let subject          = "Contact Support - LG"
                let body            = ""
                let encodedParams   = "subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
                
                if let url = URL(string: "mailto:support@quantela.com?\(encodedParams!)") {
                  if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                  } else {
                    UIApplication.shared.openURL(url)
                  }
                }
            } else {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                 AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_logout"
                ])
                UserdefaultManager().setPreference("" as AnyObject, strKey: APPLANGUAGE)
                Localize.resetCurrentLanguageToDefault()
                UserdefaultManager().clearSessions()
                _ = self.navigationController?.navigationController!.popToRootViewController(animated: true)
            }
        } else if indexPath.row == 3 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
             AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_logout"
            ])
            UserdefaultManager().setPreference("" as AnyObject, strKey: APPLANGUAGE)
            Localize.resetCurrentLanguageToDefault()
            UserdefaultManager().clearSessions()
            _ = self.navigationController?.navigationController!.popToRootViewController(animated: true)
        }
    }
}
