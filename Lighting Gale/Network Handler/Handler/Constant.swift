//
//  Constant.swift
//  SONY
//
//  Created by admin on 07/03/17.
//  Copyright © 2017 e-Procurement Technologies Pvt Ltd. All rights reserved.
//

import UIKit

import Foundation
import Reachability


import LocalAuthentication

/*func biometricType() -> BiometricType {
    let authContext = LAContext()
    if #available(iOS 11, *) {
        let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        switch(authContext.biometryType) {
        case .none:
            return .none
        case .touchID:
            return .touch
        case .faceID:
            return .face
        }
    } else {
        return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
    }
}

enum BiometricType {
    case none
    case touch
    case face
}*/

// MARK: Reachability class
struct CheckNetwork {
    static func isNetworkAvailable() -> Bool {
        var isAvailable  = false;
        
        let reachability = Reachability()!
        
        switch reachability.connection {
        case .wifi:
            isAvailable = true;
            break;
        case .cellular:
            isAvailable = true;
            break;
        case .none:
            isAvailable = false;
            break;
        }
        return isAvailable;
    }
}

// Screen Height
struct Screen {
    static var height: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    static var width: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    static func isIphone6() -> Bool {
        return height == 667 ? true : false
    }
    static func isIphone5() -> Bool {
        return height == 568 ? true : false
    }
    static func isIphone6Plus() -> Bool {
        return height == 736 ? true : false
    }
    static func isIphone4() -> Bool {
        return height == 480 ? true : false
    }
}

struct themeColor {
    static var dislikeColor : UIColor {
        return UIColor.init(red: 149.0/255.0, green: 149.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    }
    static var likeSelectedColor : UIColor {
        return UIColor.init(red: 251.0/255.0, green: 168.0/255.0, blue: 35.0/255.0, alpha: 1.0)
    }
    static var inforSelectedColor : UIColor {
        return UIColor.init(red: 101.0/255.0, green: 103.0/255.0, blue: 114.0/255.0, alpha: 1.0)
    }
    static var citySelectedColor : UIColor {
        return UIColor.init(red: 168.0/255.0, green: 168.0/255.0, blue: 168.0/255.0, alpha: 1.0)
    }
    static var headingSelectedColor : UIColor {
        return UIColor.init(red: 101.0/255.0, green: 103.0/255.0, blue: 114.0/255.0, alpha: 1.0)
    }
    static var appThemeYellowColor : UIColor {
        return UIColor.init(red: 251.0/255.0, green: 168.0/255.0, blue: 35.0/255.0, alpha: 1.0)
    }
    static var unreadbleText : UIColor {
        return UIColor.init(red: 101.0/255.0, green: 103.0/255.0, blue: 114.0/255.0, alpha: 1.0)
    }
}

internal let UILayoutPriorityNotificationPadding: Float = 999

/*internal struct Notification {
 static let titleFont = UIFont.boldSystemFont(ofSize: 14)
 static let subtitleFont = UIFont.systemFont(ofSize: 13)
 
 static let animationDuration: TimeInterval = 0.3 // second(s)
 static let exhibitionDuration: TimeInterval = 5.0 // second(s)
 
 }*/

internal struct NotificationLayout {
    static let height: CGFloat = 64.0
    static var width: CGFloat { return UIScreen.main.bounds.size.width }
    
    static var labelTitleHeight: CGFloat = 26
    static var labelMessageHeight: CGFloat = 35
    static var dragViewHeight: CGFloat = 3
    
    static let iconSize = CGSize(width: 22, height: 22)
    
    static let imageBorder: CGFloat = 15
    static let textBorder: CGFloat = 10
}


struct Messages {
    //static var appName  : String            = "Tender Tiger"
    //static var appFor   : String            = "Tender Tiger"
}

func getStringFromAnyObject(_ obj : AnyObject?) -> String {
    if let value = obj as? String {
        return value
    } else if let value = obj as? NSNumber {
        return String(describing: value)
    } else if (obj as? NSNull) == nil {
        return ""
    } else {
        return "\(String(describing: obj!))"
    }    
}

func getStringFromHTMLString(str : String) -> String {
    let newString = str.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    return newString
}

let UserDefault         = UserDefaults.standard
var appDelegate         = UIApplication.shared.delegate as! AppDelegate
