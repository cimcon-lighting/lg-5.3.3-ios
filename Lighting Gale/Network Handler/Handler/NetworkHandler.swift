//
//  NetworkHandler.swift
//  Carbon
//
//  Created by Akash on 30/06/16.
//  Copyright © 2016 Pankaj. All rights reserved.
//

import Foundation
import Alamofire
import Reachability
import MBProgressHUD

//CLIENT URL
//let baseUrlDev        = "http://199.199.50.114/WebAPI/"
//let baseUrlDev        = "http://199.199.50.216/WebAPI/"

//DARSHANBHAI URL
//let baseUrlDev        = "http://203.88.129.86/LGWebAPI/"

//LIVE URL
//let baseUrlDev        = "http://203.88.129.86/WebAPI/"


//let baseUrlDev        = "https://philly_lf.cimconlms.com/webAPI/"

//let baseUrlDev          = "https://demolg5.cimconlms.com/WebAPI/"


enum NetworkConnection {
    case available
    case notAvailable
}

class NetworkManager : NSObject {
    
    // MARK: - No Internet Connection
    func checkInternetConnection() -> NetworkConnection {
        if CheckNetwork.isNetworkAvailable() {
            return .available
        }
        return .notAvailable
    }
    
    
    
    // MARK: - POST KEY Method
    func POST_SECURITY (stringURL : String, parameters : [String : AnyObject], headerParams : [String : String], showLoader: Bool? = nil, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        switch checkInternetConnection() {
        case .available:
            if showLoader! {
                let loadingNotification = MBProgressHUD.showAdded(to:  UIApplication.shared.keyWindow!, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading".localized()
            }
            
            print("\n URl : \(stringURL) \n HEADER : \(headerParams) \n PARAMS : \(parameters)")
            
            Alamofire.request(stringURL,
                              method: .post,
                              parameters: parameters,
                              encoding: URLEncoding.default,
                              headers: headerParams).responseJSON
                {
                    (responseData) in
                    
                    if showLoader! {
                        MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                    }
                    
                    let statusCode = responseData.response?.statusCode
                    if statusCode == 200 {
//                        let responseDict = responseData.result.value as! [String : AnyObject]
                        if responseData.result.isSuccess {
                            let responseDict = responseData.result.value as! [String : AnyObject]
                            print(responseDict)
                            if responseDict[STATUS] as! String == "1" {
                                if responseDict != nil {
                                    completion(responseDict as! [String : AnyObject], responseData.result.isSuccess)
                                } else {
                                    completion([MSG:responseDict[MSG]!], true)
                                }
                            } else {
                                completion([:], false)
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    print("ERROR : \(responseDict[MSG]!)")
                                }
                            }
                        } else {
                            AlertView().showAlert(strMsg: (responseData.result.error?.localizedDescription)!, btntext: OK.localized()) { (str) in
                                print("ERROR MAP: \(responseData.result.error?.localizedDescription)")
                            }
                            completion([:], responseData.result.isSuccess)
                        }
                    } else if statusCode == 401 {
                        let responseDict = responseData.result.value as! [String : AnyObject]
                        if let status = responseDict[STATUS] as? AnyObject {
                            if status as? String == "-1" {
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    completion([:], false)
                                }
                            } else {
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                    UserDefaults().removeObject(forKey: "DYNAMIC")
                                    navigation.popToRootViewController(animated: true)
                                    completion([:], false)
                                }
                            }
                        } else {
                            AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                UserDefaults().removeObject(forKey: "DYNAMIC")
                                navigation.popToRootViewController(animated: true)
                                completion([:], false)
                            }
                        }
                    } else if statusCode == 500 {
                        AlertView().showAlert(strMsg: "Oops! Something went wrong. Try again later", btntext: OK.localized()) { (str) in
                            let navigation = appDelegate.window?.rootViewController as! UINavigationController
                            UserDefaults().removeObject(forKey: "DYNAMIC")
                            navigation.popToRootViewController(animated: true)
                        }
                    } else {
                        let responseDict : [String : AnyObject]? = responseData.result.value as? [String : AnyObject]
                        if responseDict != nil {
                            AlertView().showAlert(strMsg: (responseDict?[MSG] as? String), btntext: OK.localized()) { (str) in
                            }
                        } else {
                            AlertView().showAlert(strMsg: "Oops! Something went wrong. Try again later".localized(), btntext: OK.localized()) { (str) in
                            }
                        }
                        completion([:], false)
                    }
            }
        case .notAvailable:
            completion([:], false)
            if showLoader! {
                MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                AlertView().showAlert(strMsg: NETWRKERR, btntext: OK.localized()) { (str) in
                    print(NETWRKERR)
                }
            }
        }
    }
    
    
    
    // MARK: - POST KEY Method
    func POST (stringURL : String, parameters : [String : AnyObject], headerParams : [String : String], showLoader: Bool? = nil, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        switch checkInternetConnection() {
        case .available:
            if showLoader! {
                let loadingNotification = MBProgressHUD.showAdded(to:  UIApplication.shared.keyWindow!, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading".localized()
            }
            
            var newHeaderParams = headerParams
            newHeaderParams["IsSecured"] = "1"
            
            print("\n URl : \(appDelegate.baseUrlDev+stringURL) \n HEADER : \(newHeaderParams) \n PARAMS : \(parameters)")
            
            Alamofire.request(appDelegate.baseUrlDev+stringURL,
                              method: .post,
                              parameters: parameters,
                              encoding: JSONEncoding.default,
                              headers: newHeaderParams).responseJSON
                {
                    (responseData) in
                    
                    if showLoader! {
                        MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                    }
                    let statusCode = responseData.response?.statusCode
                    print("STATUS CODE : \(statusCode)")
                    if statusCode == 200 {
                        let responseDict = responseData.result.value as! [String : AnyObject]
                        if responseData.result.isSuccess {
                            let responseDict = responseData.result.value as! [String : AnyObject]
                            print(responseDict)
                            if responseDict[STATUS] as! String == "1" {
                                if let dict = responseDict[DATA] {
                                    if dict is Dictionary<AnyHashable,Any> {
                                        completion(dict as! [String : AnyObject], responseData.result.isSuccess)
                                    } else {
                                        var tmp : [String : AnyObject] = [:]
                                        tmp[DATA] = dict
                                        completion(tmp as! [String : AnyObject], responseData.result.isSuccess)
                                    }
                                } else {
                                    completion([MSG:responseDict[MSG]!], true)
                                }
                            } else {
                                completion([:], false)
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    print("ERROR : \(responseDict[MSG]!)")
                                }
                            }
                        } else {
                            AlertView().showAlert(strMsg: (responseData.result.error?.localizedDescription)!, btntext: OK.localized()) { (str) in
                                print("ERROR MAP: \(responseData.result.error?.localizedDescription)")
                            }
                            completion([:], responseData.result.isSuccess)
                        }
                    } else if statusCode == 401 {
                        let responseDict = responseData.result.value as! [String : AnyObject]
                        if let status = responseDict[STATUS] as? AnyObject {
                            if status as? String == "-1" {
                                let strMsg = (responseDict[MSG] as! String)
                            
                                let items = strMsg.components(separatedBy: "| ")
                                
                                AlertView().showAlert(strMsg: (items[0]).localized(), btntext: OK.localized()) { (str) in
                                    completion([:], false)
                                    if items.count > 1 {
                                        if let url = URL(string: items[1]) {
                                            UIApplication.shared.open(url)
                                        }
                                    }
                                }
                            } else {
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                    UserDefaults().removeObject(forKey: "DYNAMIC")
                                    navigation.popToRootViewController(animated: true)
                                    completion([:], false)
                                }
                            }
                        } else {
                            AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                UserDefaults().removeObject(forKey: "DYNAMIC")
                                navigation.popToRootViewController(animated: true)
                                completion([:], false)
                            }
                        }
                    } else if statusCode == 500 {
                        AlertView().showAlert(strMsg: "Oops! Something went wrong. Try again later".localized(), btntext: OK.localized()) { (str) in
                            let navigation = appDelegate.window?.rootViewController as! UINavigationController
                            UserDefaults().removeObject(forKey: "DYNAMIC")
                            navigation.popToRootViewController(animated: true)
                        }
                    } else {
                        let responseDict : [String : AnyObject]? = responseData.result.value as? [String : AnyObject]
                        if responseDict != nil,
                            let strMsg = responseDict?[MSG] {
                            AlertView().showAlert(strMsg: (strMsg as! String).localized(), btntext: OK.localized()) { (str) in
                                completion([:], false)
                            }
                        } else {
                            AlertView().showAlert(strMsg: "Oops! Something went wrong. Try again later".localized(), btntext: OK.localized()) { (str) in
                            }
                            completion([:], false)
                        }
                    }
            }
        case .notAvailable:
            completion([:], false)
            if showLoader! {
                MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                AlertView().showAlert(strMsg: NETWRKERR, btntext: OK.localized()) { (str) in
                    print(NETWRKERR)
                }
            }
        }
    }
    
    
    // MARK: - POST KEY Method
    func GET (stringURL : String, headerParams : [String : String], showLoader: Bool? = nil, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        switch checkInternetConnection() {
        case .available:
            if showLoader! {
                let loadingNotification = MBProgressHUD.showAdded(to:  UIApplication.shared.keyWindow!, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading".localized()
            }
            
            var newHeaderParams = headerParams
            newHeaderParams["IsSecured"] = "1"
            
            print("\n URl : \(appDelegate.baseUrlDev+stringURL) \n HEADER : \(newHeaderParams) \n )")
            
            Alamofire.request(appDelegate.baseUrlDev+stringURL,//appDelegate.baseUrlDev+stringURL,
                method: .get,
                encoding: JSONEncoding.default,
                headers: newHeaderParams).responseJSON
                {
                    (responseData) in
                    
                    if showLoader! {
                        MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                    }
                    let statusCode = responseData.response?.statusCode
                    if statusCode == 200 {
                        let responseDict = responseData.result.value as! [String : AnyObject]
                        if responseData.result.isSuccess {
                            print(responseDict)
                            if responseDict[STATUS] as! String == "1" {
                                if let dict = responseDict[DATA] {
                                    var dictResponseData : [String : AnyObject] = [:]
                                    dictResponseData[DATA] = dict
                                    completion(dictResponseData, responseData.result.isSuccess)
                                } else {
                                    completion([MSG:responseDict[MSG]!], true)
                                }
                            } else {
                                completion([:], false)
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    print("ERROR : \(responseDict[MSG]!)")
                                }
                            }
                        } else {
                            AlertView().showAlert(strMsg: (responseData.result.error?.localizedDescription)!, btntext: OK.localized()) { (str) in
                                print("ERROR MAP: \(responseData.result.error?.localizedDescription)")
                            }
                            completion([:], responseData.result.isSuccess)
                        }
                    } else if statusCode == 401  {
                        let responseDict = responseData.result.value as! [String : AnyObject]
                        if let status = responseDict[STATUS] as? AnyObject {
                            if status as? String == "-1" {
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    completion([:], false)
                                }
                            } else {
                                AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                    let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                    UserDefaults().removeObject(forKey: "DYNAMIC")
                                    navigation.popToRootViewController(animated: true)
                                    completion([:], false)
                                }
                            }
                        } else {
                            AlertView().showAlert(strMsg: (responseDict[MSG] as! String).localized(), btntext: OK.localized()) { (str) in
                                let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                UserDefaults().removeObject(forKey: "DYNAMIC")
                                navigation.popToRootViewController(animated: true)
                                completion([:], false)
                            }
                        }
                    } else if statusCode == 500 {
                        AlertView().showAlert(strMsg: "Oops! Something went wrong. Try again later".localized(), btntext: OK.localized()) { (str) in
                            let navigation = appDelegate.window?.rootViewController as! UINavigationController
                            UserDefaults().removeObject(forKey: "DYNAMIC")
                            navigation.popToRootViewController(animated: true)
                        }
                    } else {
                        let responseDict : [String : AnyObject]? = responseData.result.value as? [String : AnyObject]
                        if responseDict != nil,
                            let strMsg = responseDict?[MSG] {
                            AlertView().showAlert(strMsg: (strMsg as! String).localized(), btntext: OK.localized()) { (str) in
                                completion([:], false)
                            }
                        } else {
                            AlertView().showAlert(strMsg: "Oops! Something went wrong. Try again later".localized(), btntext: OK.localized()) { (str) in
                            }
                            completion([:], false)
                        }
                    }
            }
        case .notAvailable:
            completion([:], false)
            if showLoader! {
                MBProgressHUD.hide(for: appDelegate.window!, animated: true)
                AlertView().showAlert(strMsg: NETWRKERR, btntext: OK.localized()) { (str) in
                    print(NETWRKERR)
                }
            }
        }
    }
}
