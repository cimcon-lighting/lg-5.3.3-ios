//
//  ServiceHandler.swift
//  SONY
//
//  Created by admin on 21/02/17.
//  Copyright © 2017 e-Procurement Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class ServiceHandler: NSObject {
    
    static var instance :    ServiceHandler!
    
    // ModelCalss Object
    var objUserModel      :   UserModel!
    
    // SHARED INSTANCE
    class func sharedInstance() -> ServiceHandler {
        if self.instance == nil {
            self.instance = ServiceHandler()
        }
        return self.instance
    }
    
    override init() {
        objUserModel        = UserModel.init()
    }
}
