//
//  ProfileServices.swift
//  SONY
//
//  Created by admin on 07/03/17.
//  Copyright © 2017 e-Procurement Technologies Pvt Ltd. All rights reserved.
//

import Foundation

class ProfileServices {
    
    let kLogin                      = "General/API/get-token"
    let kSentCommandList            = "LightingGale/SendCommandlist"
    let kCommandTrackID             = "LightingGale/GetCommandTrackID"
    let kCommand                    = "LightingGale/GetCommand"
    let kCommandStatus              = "LightingGale/GetCommandStatus"
    let kCommandClientType          = "LightingGale/GetClientType"
    let kCommandGatwayList          = "api/v5/LightingGale/GetAllGatewayList"
    let kStatus_List                = "api/v5/LightingGale/GetSlcStatusList"
    let kMap_Data                   = "api/v5/LightingGale/GetMapsSLC"
    let kDashBoard                  = "api/v5/LightingGale/GetAllDashBoardCount"
    let kgetClientType              = "LightingGale/GetClientType"
    let kLampList                   = "api/v5/LightingGale/GetParameters"
    let kSLC_List                   = "api/v5/LightingGale/GetSLCSearch"
    let kSLC_Group                  = "/api/v5/LightingGale/GetSLCGroupSearch"
    let kSet_Mode                   = "LightingGale/SetMode"
    let kSwitch_ON_OFF              = "LightingGale/SwitchONOFFSLC"
    let kDIM                        = "LightingGale/DIMSLCs"
    let kForgotPassword             = "LightingGale/Forgotpassword"
    let kSAMLData                   = "api/v5/GetSamlDetails"
    let kTrackDetails               = "LightingGale/GetTrackDetailByTrackID"
    let kReadData                   = "LightingGale/SendReadDataCommand"
    let kResetData                  = "LightingGale/ResetSLCTrip"
    let kGet_Set_Mode               = "LightingGale/GetSetModeCommandList"
    let kGet_Mode                   = "LightingGale/GetMode"
    let kFaulty_Count               = "api/v5/LightingGale/GetFaultySLCCount"
    let kFaulty_List                = "api/v5/LightingGale/GetFaultySLCDetails"
    let kGateawayCount              = "api/v5/LightingGale/GatewayCount"
    let kGatewayList                = "api/v5/LightingGale/GetGatewayList"
    let kAssignedSLC                = "api/v5/LightingGale/AssignedSLC"
    let kBurnurList                 = "api/v5/LightingGale/ReportOutageDayBurner"
    let kHistoricalData             = "api/v5/LightingGale/HistoryDataReport"
    let kTotalData                  = "api/v5/LightingGale/SLCStatusTotal"
    let kTotalSLCSearch             = "api/v5/LightingGale/GetSLCSearchUnMapped"
    let kGetSAMLToken               = "api/v5/GetSAMLToken"
    
    
    let networkHandler              = NetworkManager()
    
    
    //MARK:- SECURITY CODE
    func security (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
//        clapptest.cimconlms.com
//        apps.cimconlighting.com
//        https://securitytest.cimconlms.com/LGWEbAPI
        networkHandler.POST_SECURITY(stringURL: "https://clapptest.cimconlms.com/LightingGale/api-bct/checkuniquecode", parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- UPGARDE CLIENT
    func upgradeClient (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST_SECURITY(stringURL: "https://clapptest.cimconlms.com/LightingGale/api-bct/UpgradeEmail", parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Login
    func login (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kLogin, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                ServiceHandler.sharedInstance().objUserModel.setUserData(responseData)
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- TOTAL SLC LIST
    func getTotalSLCSearch (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kTotalSLCSearch, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getGatewayCount
    func getGatewayCount (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kGateawayCount, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getGatewayList
    func getGatewayList (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kGatewayList, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getFaultyCount
    func getFaultyCount (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kFaulty_Count, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getFaultyList
    func getFaultyList (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kFaulty_List, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getDetailsTrackId
    func getDetailsFrmTrackID (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kTrackDetails, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    
    //MARK:- getSetMode
    func getSetMode (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kGet_Set_Mode, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    //MARK:- getSAMLToken
    func getSAMLToken (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kGetSAMLToken, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                ServiceHandler.sharedInstance().objUserModel.setUserData(responseData)
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getAssignedSLCs
    func getAssignedSLCs (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kAssignedSLC, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    
    //MARK:- getReadData
    func setResetData (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kResetData, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- getMode
    func getMode (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kGet_Mode, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    
    //MARK:- getReadData
    func getReadData (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kReadData, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- FORGOT PASSWORD
    func forgotPwd (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kForgotPassword, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- SAMLData
    func getSAMLData (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kSAMLData, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- SET Mode
    func setMode (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kSet_Mode, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Switch On Off
    func switchOnOff (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kSwitch_ON_OFF, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Switch Dim
    func switchDim (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kDIM, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    
    //MARK:- Sent Command
    func sentCommandList (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kSentCommandList, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- GET SLC LIST
    func getSLCList (headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kSLC_List, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    
    //MARK:- GET SLC LIST
    func getGroupSLC (headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kSLC_Group, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    //MARK:- Sent Command
    func commandTrackID (headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kCommandTrackID, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- DASHBOARD
    func getDashBoard (headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kDashBoard, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    //MARK:- getClientType
    func getClientType (headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kgetClientType, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- LAMP TYPE
    func getLampList (headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kLampList, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    
    //MARK:- Command
    func getCommand(headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kCommand, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- CommandStatus
    func getMapData (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kMap_Data, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- CommandStatus
    func getCommandStatus(headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kCommandStatus, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Command Client Type
    func getCommandClientType(headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kCommandClientType, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Command Gateway List
    func getCommandGatwayList(headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kCommandGatwayList, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Historical Data
    func getHistoricalData (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kHistoricalData, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Burnur List
    func getBurnerList (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kBurnurList, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    //MARK:- SLC STATUS LIST
    func getStatusList (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.POST(stringURL: kStatus_List, parameters: parameters, headerParams:headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
    
    
    //MARK:- Total List
    func getTotalData (parameters : [String : AnyObject], headerParams : [String : String], showLoader : Bool, completion: @escaping (_ result: [String : AnyObject], _ isSuccess : Bool) -> Void) {
        networkHandler.GET(stringURL: kTotalData, headerParams: headerParams, showLoader: showLoader) { (responseData, isSuccess) in
            if isSuccess {
                completion(responseData, isSuccess)
            } else {
                completion([:], isSuccess)
            }
        }
    }
}
